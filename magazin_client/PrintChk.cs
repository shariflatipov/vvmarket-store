﻿using System;
using System.Windows.Forms;
using magazin_client.peripheral;

namespace magazin_client {
    public class PrintChk {
        readonly private string _acceptedMoney;
        private readonly string _subTotal;
        readonly private string _changeBack;
        readonly private string _total, _discount;
        private readonly DataGridView _dg;
        private readonly IPrinter _printer = (IPrinter)Activator.CreateInstance(Type.GetType("magazin_client.peripheral." + MainForm.PrinterPackage));

        public PrintChk(string accept, string change, string total, string discount, DataGridView dgv, string subTotal) {
            try {
                _acceptedMoney = accept;
                _changeBack = change;
                _total = total;
                _discount = discount;
                _dg = dgv;
                _subTotal = subTotal;
                Init();
            } catch (Exception ex) {
                Logger.Log(ex, "PrintCheck", Level.Warining);
            }
        }

        private void SaveToFile(string str) {
            _printer.Print(str + "\r\n");
        }
        
        private void Init() {
            _printer.BoldOn();
            SaveToFile(MainForm.StoreName);
            SaveToFile("Адрес: " + MainForm.StoreAddress);
            SaveToFile("ИНН: " + MainForm.StoreINN);
            SaveToFile("Продавец: " + MainForm.Login);
            SaveToFile("Код: " + ProductCartToDb.Id);
            SaveToFile("---------------------------");
            _printer.BoldOff();
            for(int i = 0; i < _dg.Rows.Count; i++) {

                string prodName = _dg.Rows[i].Cells[(int)ProductColumns.Name].Value.ToString();
                if(prodName.Length > 27) {
                    prodName = prodName.Substring(0, 27);
                }
                prodName = (i + 1) + ". " + prodName;
                SaveToFile(prodName);

                string resultString = "  " + _dg.Rows[i].Cells[(int)ProductColumns.Quantity].Value + " x " + _dg.Rows[i].Cells[(int)ProductColumns.Price].Value;
                resultString = resultString.PadLeft(17);

                SaveToFile(resultString + "= " + _dg.Rows[i].Cells[(int)ProductColumns.Total].Value + "сом.");
            }
            _printer.BoldOn();
            SaveToFile("---------------------------");

            SaveToFile("Сумма: " + _subTotal);
            SaveToFile("Принято: " + _acceptedMoney);
            SaveToFile("Скидка:  " + _discount);
            SaveToFile("Сдача: " + _changeBack);
            SaveToFile("Итого: " + _total);
            SaveToFile("Дата:" + DateTime.Now);
            SaveToFile(MainForm.StoreSlogan);
            SaveToFile(MainForm.StorePhones);
            SaveToFile("   Спасибо за покупку!!!!   ");
            SaveToFile("");
            SaveToFile("");
            _printer.BoldOff();
            _printer.Cut();
        }
    }
}