﻿namespace magazin_client
{
    partial class ChangeBack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_1 = new System.Windows.Forms.Button();
            this.btn_2 = new System.Windows.Forms.Button();
            this.btn_5 = new System.Windows.Forms.Button();
            this.btn_6 = new System.Windows.Forms.Button();
            this.btn_3 = new System.Windows.Forms.Button();
            this.btn_9 = new System.Windows.Forms.Button();
            this.btn_8 = new System.Windows.Forms.Button();
            this.btn_4 = new System.Windows.Forms.Button();
            this.btn_7 = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.confirm = new System.Windows.Forms.Button();
            this.lblAcceptedMoney = new System.Windows.Forms.Label();
            this.btn_0 = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.lblDiscountTotal = new System.Windows.Forms.Label();
            this.lblChange = new System.Windows.Forms.Label();
            this.btn_dot = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblToPay = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDiscountPercent = new System.Windows.Forms.Button();
            this.btnDiscountFixed = new System.Windows.Forms.Button();
            this.lblDiscount = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblFinal = new System.Windows.Forms.Label();
            this.btnDiscount = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_1
            // 
            this.btn_1.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_1.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_1.Location = new System.Drawing.Point(628, 95);
            this.btn_1.Name = "btn_1";
            this.btn_1.Size = new System.Drawing.Size(105, 85);
            this.btn_1.TabIndex = 0;
            this.btn_1.Text = "1";
            this.btn_1.UseVisualStyleBackColor = false;
            this.btn_1.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_2
            // 
            this.btn_2.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_2.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_2.Location = new System.Drawing.Point(755, 95);
            this.btn_2.Name = "btn_2";
            this.btn_2.Size = new System.Drawing.Size(105, 85);
            this.btn_2.TabIndex = 1;
            this.btn_2.Text = "2";
            this.btn_2.UseVisualStyleBackColor = false;
            this.btn_2.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_5
            // 
            this.btn_5.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_5.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_5.Location = new System.Drawing.Point(755, 188);
            this.btn_5.Name = "btn_5";
            this.btn_5.Size = new System.Drawing.Size(105, 85);
            this.btn_5.TabIndex = 2;
            this.btn_5.Text = "5";
            this.btn_5.UseVisualStyleBackColor = false;
            this.btn_5.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_6
            // 
            this.btn_6.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_6.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_6.Location = new System.Drawing.Point(882, 188);
            this.btn_6.Name = "btn_6";
            this.btn_6.Size = new System.Drawing.Size(105, 85);
            this.btn_6.TabIndex = 3;
            this.btn_6.Text = "6";
            this.btn_6.UseVisualStyleBackColor = false;
            this.btn_6.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_3
            // 
            this.btn_3.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_3.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_3.Location = new System.Drawing.Point(882, 97);
            this.btn_3.Name = "btn_3";
            this.btn_3.Size = new System.Drawing.Size(105, 85);
            this.btn_3.TabIndex = 4;
            this.btn_3.Text = "3";
            this.btn_3.UseVisualStyleBackColor = false;
            this.btn_3.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_9
            // 
            this.btn_9.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_9.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_9.Location = new System.Drawing.Point(882, 281);
            this.btn_9.Name = "btn_9";
            this.btn_9.Size = new System.Drawing.Size(105, 85);
            this.btn_9.TabIndex = 5;
            this.btn_9.Text = "9";
            this.btn_9.UseVisualStyleBackColor = false;
            this.btn_9.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_8
            // 
            this.btn_8.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_8.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_8.Location = new System.Drawing.Point(755, 281);
            this.btn_8.Name = "btn_8";
            this.btn_8.Size = new System.Drawing.Size(105, 85);
            this.btn_8.TabIndex = 6;
            this.btn_8.Text = "8";
            this.btn_8.UseVisualStyleBackColor = false;
            this.btn_8.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_4
            // 
            this.btn_4.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_4.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_4.Location = new System.Drawing.Point(628, 188);
            this.btn_4.Name = "btn_4";
            this.btn_4.Size = new System.Drawing.Size(105, 85);
            this.btn_4.TabIndex = 7;
            this.btn_4.Text = "4";
            this.btn_4.UseVisualStyleBackColor = false;
            this.btn_4.Click += new System.EventHandler(this.BtnClick);
            // 
            // btn_7
            // 
            this.btn_7.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_7.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_7.Location = new System.Drawing.Point(628, 281);
            this.btn_7.Name = "btn_7";
            this.btn_7.Size = new System.Drawing.Size(105, 85);
            this.btn_7.TabIndex = 8;
            this.btn_7.Text = "7";
            this.btn_7.UseVisualStyleBackColor = false;
            this.btn_7.Click += new System.EventHandler(this.BtnClick);
            // 
            // cancel
            // 
            this.cancel.BackgroundImage = global::magazin_client.Properties.Resources.Close;
            this.cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cancel.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.cancel.ForeColor = System.Drawing.Color.Firebrick;
            this.cancel.Location = new System.Drawing.Point(882, 468);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(105, 85);
            this.cancel.TabIndex = 9;
            this.cancel.UseVisualStyleBackColor = false;
            this.cancel.Click += new System.EventHandler(this.CancelClick);
            // 
            // confirm
            // 
            this.confirm.BackgroundImage = global::magazin_client.Properties.Resources.camera_test;
            this.confirm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.confirm.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.confirm.ForeColor = System.Drawing.Color.Firebrick;
            this.confirm.Location = new System.Drawing.Point(628, 468);
            this.confirm.Name = "confirm";
            this.confirm.Size = new System.Drawing.Size(105, 85);
            this.confirm.TabIndex = 10;
            this.confirm.UseVisualStyleBackColor = false;
            this.confirm.Visible = false;
            this.confirm.Click += new System.EventHandler(this.ConfirmClick);
            // 
            // lblAcceptedMoney
            // 
            this.lblAcceptedMoney.BackColor = System.Drawing.Color.Transparent;
            this.lblAcceptedMoney.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcceptedMoney.ForeColor = System.Drawing.Color.Red;
            this.lblAcceptedMoney.Location = new System.Drawing.Point(359, 102);
            this.lblAcceptedMoney.Name = "lblAcceptedMoney";
            this.lblAcceptedMoney.Size = new System.Drawing.Size(263, 65);
            this.lblAcceptedMoney.TabIndex = 11;
            this.lblAcceptedMoney.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAcceptedMoney.TextChanged += new System.EventHandler(this.LblCountTextChanged);
            // 
            // btn_0
            // 
            this.btn_0.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_0.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_0.Location = new System.Drawing.Point(755, 374);
            this.btn_0.Name = "btn_0";
            this.btn_0.Size = new System.Drawing.Size(105, 85);
            this.btn_0.TabIndex = 12;
            this.btn_0.Text = "0";
            this.btn_0.UseVisualStyleBackColor = false;
            this.btn_0.Click += new System.EventHandler(this.BtnClick);
            // 
            // back
            // 
            this.back.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.back.ForeColor = System.Drawing.Color.Firebrick;
            this.back.Location = new System.Drawing.Point(628, 374);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(105, 85);
            this.back.TabIndex = 13;
            this.back.Text = "<<";
            this.back.UseVisualStyleBackColor = false;
            this.back.Click += new System.EventHandler(this.BackClick);
            // 
            // clear
            // 
            this.clear.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.clear.ForeColor = System.Drawing.Color.Firebrick;
            this.clear.Location = new System.Drawing.Point(882, 374);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(105, 85);
            this.clear.TabIndex = 14;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = false;
            this.clear.Click += new System.EventHandler(this.ClearClick);
            // 
            // lblDiscountTotal
            // 
            this.lblDiscountTotal.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscountTotal.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscountTotal.ForeColor = System.Drawing.Color.Red;
            this.lblDiscountTotal.Location = new System.Drawing.Point(359, 361);
            this.lblDiscountTotal.Name = "lblDiscountTotal";
            this.lblDiscountTotal.Size = new System.Drawing.Size(263, 65);
            this.lblDiscountTotal.TabIndex = 15;
            this.lblDiscountTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscountTotal.TextChanged += new System.EventHandler(this.lblDiscountTotal_TextChanged);
            // 
            // lblChange
            // 
            this.lblChange.BackColor = System.Drawing.Color.Transparent;
            this.lblChange.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChange.ForeColor = System.Drawing.Color.Red;
            this.lblChange.Location = new System.Drawing.Point(359, 530);
            this.lblChange.Name = "lblChange";
            this.lblChange.Size = new System.Drawing.Size(263, 65);
            this.lblChange.TabIndex = 16;
            this.lblChange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_dot
            // 
            this.btn_dot.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btn_dot.ForeColor = System.Drawing.Color.Firebrick;
            this.btn_dot.Location = new System.Drawing.Point(755, 468);
            this.btn_dot.Name = "btn_dot";
            this.btn_dot.Size = new System.Drawing.Size(105, 85);
            this.btn_dot.TabIndex = 17;
            this.btn_dot.Text = ",";
            this.btn_dot.UseVisualStyleBackColor = false;
            this.btn_dot.Click += new System.EventHandler(this.BtnDotClick);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(12, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(341, 65);
            this.label1.TabIndex = 18;
            this.label1.Text = "Получено";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(12, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(341, 65);
            this.label2.TabIndex = 19;
            this.label2.Text = "Сумма";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(12, 530);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(341, 65);
            this.label3.TabIndex = 20;
            this.label3.Text = "Сдача";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(12, 361);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(341, 65);
            this.label4.TabIndex = 25;
            this.label4.Text = "Сумма скид.";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblToPay
            // 
            this.lblToPay.BackColor = System.Drawing.Color.Transparent;
            this.lblToPay.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToPay.ForeColor = System.Drawing.Color.Red;
            this.lblToPay.Location = new System.Drawing.Point(359, 185);
            this.lblToPay.Name = "lblToPay";
            this.lblToPay.Size = new System.Drawing.Size(263, 65);
            this.lblToPay.TabIndex = 24;
            this.lblToPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(12, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(341, 65);
            this.label5.TabIndex = 27;
            this.label5.Text = "Скидка";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(1053, 65);
            this.label6.TabIndex = 28;
            this.label6.Text = "ОПЛАТА ПОКУПОК";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDiscountPercent
            // 
            this.btnDiscountPercent.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountPercent.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscountPercent.ForeColor = System.Drawing.Color.Firebrick;
            this.btnDiscountPercent.Location = new System.Drawing.Point(628, 568);
            this.btnDiscountPercent.Name = "btnDiscountPercent";
            this.btnDiscountPercent.Size = new System.Drawing.Size(105, 85);
            this.btnDiscountPercent.TabIndex = 29;
            this.btnDiscountPercent.Text = "П";
            this.btnDiscountPercent.UseVisualStyleBackColor = false;
            this.btnDiscountPercent.Visible = false;
            this.btnDiscountPercent.Click += new System.EventHandler(this.btnDiscountPercent_Click);
            // 
            // btnDiscountFixed
            // 
            this.btnDiscountFixed.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountFixed.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscountFixed.ForeColor = System.Drawing.Color.Firebrick;
            this.btnDiscountFixed.Location = new System.Drawing.Point(882, 568);
            this.btnDiscountFixed.Name = "btnDiscountFixed";
            this.btnDiscountFixed.Size = new System.Drawing.Size(105, 85);
            this.btnDiscountFixed.TabIndex = 30;
            this.btnDiscountFixed.Text = "Ф";
            this.btnDiscountFixed.UseVisualStyleBackColor = false;
            this.btnDiscountFixed.Visible = false;
            this.btnDiscountFixed.Click += new System.EventHandler(this.btnDiscountFixed_Click);
            // 
            // lblDiscount
            // 
            this.lblDiscount.BackColor = System.Drawing.Color.Transparent;
            this.lblDiscount.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiscount.ForeColor = System.Drawing.Color.Red;
            this.lblDiscount.Location = new System.Drawing.Point(359, 274);
            this.lblDiscount.Name = "lblDiscount";
            this.lblDiscount.Size = new System.Drawing.Size(263, 65);
            this.lblDiscount.TabIndex = 31;
            this.lblDiscount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscount.TextChanged += new System.EventHandler(this.lblDiscount_TextChanged);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(14, 449);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(341, 65);
            this.label7.TabIndex = 32;
            this.label7.Text = "К оплате";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFinal
            // 
            this.lblFinal.BackColor = System.Drawing.Color.Transparent;
            this.lblFinal.Font = new System.Drawing.Font("Times New Roman", 39.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinal.ForeColor = System.Drawing.Color.Red;
            this.lblFinal.Location = new System.Drawing.Point(359, 449);
            this.lblFinal.Name = "lblFinal";
            this.lblFinal.Size = new System.Drawing.Size(263, 65);
            this.lblFinal.TabIndex = 33;
            this.lblFinal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnDiscount
            // 
            this.btnDiscount.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscount.Font = new System.Drawing.Font("Mistral", 42F, System.Drawing.FontStyle.Bold);
            this.btnDiscount.ForeColor = System.Drawing.Color.Firebrick;
            this.btnDiscount.Location = new System.Drawing.Point(755, 568);
            this.btnDiscount.Name = "btnDiscount";
            this.btnDiscount.Size = new System.Drawing.Size(105, 85);
            this.btnDiscount.TabIndex = 34;
            this.btnDiscount.Text = "Д";
            this.btnDiscount.UseVisualStyleBackColor = false;
            this.btnDiscount.Click += new System.EventHandler(this.btnDiscount_Click);
            // 
            // ChangeBack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1019, 705);
            this.Controls.Add(this.btnDiscount);
            this.Controls.Add(this.lblFinal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblDiscount);
            this.Controls.Add(this.btnDiscountFixed);
            this.Controls.Add(this.btnDiscountPercent);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblToPay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_dot);
            this.Controls.Add(this.lblChange);
            this.Controls.Add(this.lblDiscountTotal);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.back);
            this.Controls.Add(this.btn_0);
            this.Controls.Add(this.lblAcceptedMoney);
            this.Controls.Add(this.confirm);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.btn_7);
            this.Controls.Add(this.btn_4);
            this.Controls.Add(this.btn_8);
            this.Controls.Add(this.btn_9);
            this.Controls.Add(this.btn_3);
            this.Controls.Add(this.btn_6);
            this.Controls.Add(this.btn_5);
            this.Controls.Add(this.btn_2);
            this.Controls.Add(this.btn_1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "ChangeBack";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nums";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeBack_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_1;
        private System.Windows.Forms.Button btn_2;
        private System.Windows.Forms.Button btn_5;
        private System.Windows.Forms.Button btn_6;
        private System.Windows.Forms.Button btn_3;
        private System.Windows.Forms.Button btn_9;
        private System.Windows.Forms.Button btn_8;
        private System.Windows.Forms.Button btn_4;
        private System.Windows.Forms.Button btn_7;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.Button confirm;
        private System.Windows.Forms.Label lblAcceptedMoney;
        private System.Windows.Forms.Button btn_0;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button clear;
        private System.Windows.Forms.Label lblDiscountTotal;
        private System.Windows.Forms.Label lblChange;
        private System.Windows.Forms.Button btn_dot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        //private System.Windows.Forms.Button btnDiscount_1;
        //private System.Windows.Forms.Button btnDiscount_2;
        //private System.Windows.Forms.Button btnDiscount_3;
        //private System.Windows.Forms.Button btnClearDiscount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblToPay;
        private System.Windows.Forms.Label label5;
        //private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnDiscountPercent;
        private System.Windows.Forms.Button btnDiscountFixed;
        private System.Windows.Forms.Label lblDiscount;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblFinal;
        private System.Windows.Forms.Button btnDiscount;
    }
}