﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;

namespace magazin_client {
    class ProductCartToDb {

        public static long Id;

        public static int ChangeProductQuantity(Product product) {
            try {
                CreateDB.cmd.CommandText = "update transactions set count = @quantity where id_trn in " +
                    "(select top(1) id_trn from transactions where product_id = @barcode and sold_id = @id and price = @price and @batch=batch)";
                CreateDB.cmd.Parameters.Add("@quantity", product.GetQuantity());
                CreateDB.cmd.Parameters.Add("@barcode", product.GetId());
                CreateDB.cmd.Parameters.Add("@price", product.GetPrice());
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.Parameters.Add("@batch", product.GetBatch());
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "changeProductQuantity");
                return -1;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return 1;
        }

        public int insertNewProduct(Product product) {
            try {
                CreateDB.cmd.CommandText = "insert into transactions(sold_id, product_id, count, price, discont, status, batch) " +
                                        "values(@id, @product, @count, @price, @discont, @status, @batch)";
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.Parameters.Add("@product", product.GetId());
                CreateDB.cmd.Parameters.Add("@count", product.GetQuantity());
                CreateDB.cmd.Parameters.Add("@price", product.GetPrice());
                CreateDB.cmd.Parameters.Add("@discont", product.GetDiscount());
                CreateDB.cmd.Parameters.Add("@status", "0");
                CreateDB.cmd.Parameters.Add("@batch", product.GetBatch());
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "changeProductQuantity");
                return -1;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return 1;
        }

        public int deleteProduct(string barcode, string price, string quantity, string batch) {
            try {
                CreateDB.cmd.CommandText = "delete from transactions where id_trn in " +
                                           "(select top(1) id_trn from transactions where product_id = @barcode " +
                                           "and sold_id = @id and count = @quantity and price = @price and @batch=batch)";
                CreateDB.cmd.Parameters.Add("@barcode", barcode);
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.Parameters.Add("@quantity", quantity);
                CreateDB.cmd.Parameters.Add("@price", price);
                CreateDB.cmd.Parameters.Add("@batch", batch);
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "changeProductQuantity");
                return -1;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return 1;
        }

        public int deleteAllProducts() {
            try {
                CreateDB.cmd.CommandText = "delete from transactions where sold_id = @id";
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "deleteAllProducts");
                return -1;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return 1;
        }

        public long storeBasket() {
            try {
                CreateDB.cmd.CommandText = "update sold_product set status = -1 where id_sold = @id";
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "deleteAllProducts");
                return -1;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return Id;
        }

        public IEnumerable<Product> restoreBasket(long trnId) {
            Id = trnId;
            var products = new List<Product>();

            try {
                CreateDB.cmd.CommandText = "update sold_product set status = 0 where id_sold = @id";
                CreateDB.cmd.Parameters.Add("@id", trnId);
                CreateDB.cmd.ExecuteNonQuery();

                CreateDB.cmd.CommandText = "SELECT p.product_id, p.product_name, p.product_description, p.unitID, t.price, t.count, t.batch, t.discount " +
                                        " FROM transactions AS t INNER JOIN product AS p ON t.product_id = p.product_id and t.batch=p.batch WHERE (t.sold_id = @id)";
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                while(reader.Read()) {
                    var product = new Product();
                    product.SetProductByFields(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(),
                        double.Parse(reader[4].ToString()), null, double.Parse(reader[5].ToString()), double.Parse(reader[7].ToString()), 0, reader[6].ToString());
                    products.Add(product);
                }

            } catch(Exception ex) {
                Logger.Log(ex, "deleteAllProducts");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return products;
        }

        public static void ChangeDiscount(Product product) {
            try {
                CreateDB.cmd.CommandText = "update transactions set discont = @discount where id_trn in " +
                    "(select top(1) id_trn from transactions where product_id = @barcode and sold_id = @id and price = @price and @batch=batch)";
                CreateDB.cmd.Parameters.Add("@discount", product.GetDiscount());
                CreateDB.cmd.Parameters.Add("@barcode", product.GetId());
                CreateDB.cmd.Parameters.Add("@price", product.GetPrice());
                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.Parameters.Add("@batch", product.GetBatch());
                CreateDB.cmd.ExecuteNonQuery();
            } catch(Exception ex) {
                Logger.Log(ex, "ProductCartToDb => ChangeDiscount");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
        }
    }
}
