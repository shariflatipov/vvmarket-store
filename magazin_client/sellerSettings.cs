﻿using System.Data.SqlServerCe;

namespace magazin_client
{
    class SellerSettings
    {
        readonly private SqlCeConnection conn = new SqlCeConnection();
        readonly private SqlCeCommand cmd = new SqlCeCommand();

        private const string strValidateUser = "SELECT * FROM seller where login = @login and pass = @pass";

        public string userPass;
        public string userLogin;
        public string userId;
        public string userFName;
        public string userLName;
        public string userPatron;

        public SellerSettings()
        {
            conn.ConnectionString = CreateDB.ConnString;
        }

        public short getUser(string login, string password)
        {
            if (conn.State != System.Data.ConnectionState.Open)
            {
                conn.Open();
            }

            cmd.Connection = conn;
            cmd.CommandText = strValidateUser;

            cmd.Parameters.Add("@login", login);
            cmd.Parameters.Add("@pass", password);

            SqlCeDataReader reader = cmd.ExecuteReader();

            bool flagRecordExists = false;

            while (reader.Read())
            {
                this.userId = reader[0].ToString();
                this.userLName = reader[1].ToString();
                this.userFName = reader[2].ToString();
                this.userPatron = reader[3].ToString();
                this.userLogin = reader[5].ToString();
                this.userPass = reader[6].ToString();
                flagRecordExists = true;
            }
            
            cmd.Parameters.Clear();
            conn.Close();
            
            if (flagRecordExists)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}