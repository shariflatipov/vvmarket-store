﻿using System;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace magazin_client {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException); 
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            if(!File.Exists("db.sdf")) {
                Application.Run(new CreateAdminPass());
            } else {
                CreateDB.conn.ConnectionString = CreateDB.ConnString;

                if(CreateDB.conn.State != System.Data.ConnectionState.Open) {
                    CreateDB.conn.Open();
                }
                try {
                    CreateDB.cmd.Connection = CreateDB.conn;
                    CreateDB.cmd.CommandText = "delete from sold_product where sum = 0";
                    CreateDB.cmd.ExecuteNonQuery();
                } catch(Exception ex) {
                    Logger.Log(ex, "Program.cs : function => Main()");
                }
                try {
                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.FileName = "kill.bat";
                    p.Start();

                    string output = p.StandardOutput.ReadToEnd();
                    p.WaitForExit();
                } catch {
                }
                Application.Run(new MainForm());
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
            Logger.Log((Exception)e.ExceptionObject, "Unhandled exception");
            MessageBox.Show(e.ToString() + sender);
        }

        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
            Logger.Log(e.Exception, "Unhandled thread exception");
            MessageBox.Show(e + sender.ToString());
        }
    }
}