﻿using System;
using System.Xml.Linq;
using System.Data.SqlServerCe;

namespace magazin_client {
    class CreateXml {
        private XElement _sold;
        private XElement _magazin;
        private readonly string _trnId;
        private readonly string _total;
        private readonly string _dateToSend;
        private readonly string _login;
        private readonly string _pass;
        private readonly string _discount;
        private readonly string _discountNum;
        private const int Act = 0;

        public CreateXml(string trnId, string total, string dateToSend, string login, string pass, string discount, string discountNum) {
            _trnId = trnId;
            _total = total;
            DateTime b = DateTime.Parse(dateToSend);
            _dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");
            _login = login;
            _pass = pass;
            _discount = discount;
            _discountNum = discountNum;
        }

        private void Init() {
            string password = MyCrypt.GetMd5Hash(_pass);
            string hashSum = MyCrypt.GetSha1(_dateToSend + "#" + _login + "#" + password);

            var seller = new XElement("seller");
            seller.Add(new XAttribute("login", _login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", Act));
            seller.Add(new XAttribute("date", _dateToSend));

            _magazin = new XElement("magazin");
            _magazin.Add(new XElement(seller));
        }

        public XElement FillProducts() {
            Init();
            _sold = new XElement("sold");
            _sold.Add(new XAttribute("id", _trnId));
            _sold.Add(new XAttribute("date", _dateToSend));
            _sold.Add(new XAttribute("discount_num", _discountNum));
            
            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    try {

                        double discount = 0;
                        double sum = 0;

                        cmd.CommandText = "select product_id, count, price, batch, discont from transactions where sold_id = @trn";
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.Parameters.Add("@trn", _trnId);
                        using (SqlCeDataReader reader = cmd.ExecuteReader()) {
                            while (reader.Read()) {

                                double lCount = 0;
                                double lDiscount = 0;
                                double lPrice = 0;

                                var product = new XElement("product");

                                product.Add(new XAttribute("id", reader[0].ToString()));
                                product.Add(new XElement("count", reader[1].ToString().Replace(",", ".")));
                                product.Add(new XElement("price", reader[2].ToString().Replace(",", ".")));
                                product.Add(new XElement("batch", reader[3].ToString()));
                                product.Add(new XElement("discount_percent", reader[4].ToString()));

                                double.TryParse(reader[1].ToString(), out lCount);
                                double.TryParse(reader[2].ToString(), out lPrice);
                                double.TryParse(reader[4].ToString(), out lDiscount);

                                sum += lPrice*lCount;
                                discount += lPrice*lCount*lDiscount/100;

                                _sold.Add(product);
                            }
                        }

                        _sold.Add(new XAttribute("sum", sum));
                        _sold.Add(new XAttribute("discount", (double.Parse(_discount.Replace(".", ",")) + discount)));
            
                        _magazin.Add(_sold);
                    } catch (Exception ex) {
                        Logger.Log(ex, "FillProducts()");
                    }
                }
            }
            return _magazin;
        }
    }
}