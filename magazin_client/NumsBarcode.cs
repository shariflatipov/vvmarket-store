﻿using System;
using System.Linq;

namespace magazin_client {
    class NumsBarcode :Nums {

        public NumsBarcode(ProductCart pc) : base(pc, 0) {
            lblInfo.Text = @"Штрихкод";
        }

        protected override void ConfirmClick(object sender, EventArgs e) {

            var products = Product.GetProducts(lblCount.Text);
            switch (products.Count) {
                case 0:
                    break;
                case 1:
                    pc.AddItem(products[0]);
                    break;
                default:
                    var chooseBatch = new ChooseBatch(products);
                    chooseBatch.Show();
                    if(chooseBatch.Batch == "") {
                        chooseBatch.Dispose();
                        return;
                    }
                    
                    foreach(var product in products.Where(product => product.GetBatch().Equals(chooseBatch.Batch))) {
                        pc.AddItem(product);
                    }

                    chooseBatch.Dispose();
                    break;
            }
            
            pc.HotRowsAdded();
            Dispose();
        }
    }
}
