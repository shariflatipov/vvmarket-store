﻿using System;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace magazin_client {
    public partial class AddUserForm : Form {

        public AddUserForm() {
            InitializeComponent();
        }

        private void ConfirmAddUser(object sender, EventArgs e) {
            if (txtConfirmPass.TextLength > 2 && txtFirstName.TextLength > 2 && txtLastName.TextLength > 2 && txtLogin.TextLength > 2) {
                if (txtPass.Text == txtConfirmPass.Text) {
                    try {
                        if (isLoginExists(txtLogin.Text) < 1) {
                            if (addUser(txtLastName.Text, txtFirstName.Text, txtPatronimyc.Text, txtPass.Text, 1, DateTime.Now) > 1) {
                                MessageBox.Show("Пользователь успешно добавлен", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            resetFields();
                        } else {
                            MessageBox.Show("Error: Данный логин существует!", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    } catch (Exception) {
                        MessageBox.Show("Error: Обратитесь к администратору!", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                } else {
                    MessageBox.Show("Пароли не совпадают!", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } else {
                MessageBox.Show("Заполните все поля!", "Incorrect password", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void resetFields() {
            txtConfirmPass.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtLogin.Text = "";
            txtPass.Text = "";
            txtPatronimyc.Text = "";
            dtpBirthdate.Value = DateTime.Now;
        }

        private int isLoginExists(String login) {
            int result = -1;

            using (SqlCeConnection conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (SqlCeCommand cmd = new SqlCeCommand()) {
                    cmd.Connection = conn;
                    cmd.CommandText = "select count(*) from seller where login = @login";
                    cmd.Parameters.Add("@login", login);

                    conn.Open();

                    result = (int)cmd.ExecuteScalar();
                }
            }
            return result;
        }

        private int addUser(string lastName, string firstName, string patronimyc, string pass, short status, DateTime birthDate) {
            int result = -1;
            using (SqlCeConnection conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (SqlCeCommand cmd = new SqlCeCommand()) {

                    cmd.CommandText = "insert into seller(last_name, first_name, patronymic, birthdate, login, pass, status)" +
                                                                         " values(@lastName, @firstName, @patronimyc, @birthdate, @login, @pass, @status)";

                    cmd.Parameters.Add("@lastName", lastName);
                    cmd.Parameters.Add("@firstName", firstName);
                    cmd.Parameters.Add("@patronimyc", patronimyc);
                    cmd.Parameters.Add("@pass", pass);
                    cmd.Parameters.Add("@status", status);
                    cmd.Parameters.Add("@birthdate", birthDate);

                    result = cmd.ExecuteNonQuery();
                }
            }
            return result;
        }

        private void BtnExitClick(object sender, EventArgs e) {
            Application.Exit();
        }

        private void BtnEnterClick(object sender, EventArgs e) {
            MainForm au = this.MdiParent as MainForm;
            au.Auth.MdiParent = this.MdiParent;
            au.Auth.Show();
            this.Hide();
        }
    }
}