﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.Generic;

namespace magazin_client {

    public enum ProductColumns : ushort {
        Id,
        Name,
        Description,
        Quantity,
        Unit,
        Batch,
        Price,
        Discount,
        Total,
        Delete
    }

    public class ProductCart {

        public delegate void RowAdded();

        private readonly DataGridView _dGrid;
        private Product _product;
        private readonly ProductCartToDb _productToDb = new ProductCartToDb();

        private readonly DataGridViewCellStyle _style = new DataGridViewCellStyle();
        private readonly DataGridViewCellStyle _defaultStyle = new DataGridViewCellStyle();
        public short PreviousHighlighted = -1;
        private long _savedPreviousBasketId;

        public Product GetCurrentProduct() {
            return _product;
        }

        public Product GetCurrentProduct(int index) {
            return Product.SetProductById(_dGrid.Rows[index].Cells[(int)ProductColumns.Id].Value.ToString(),
                _dGrid.Rows[index].Cells[(int) ProductColumns.Batch].Value.ToString(), 1);
        }

        public event RowAdded RowAdd;

        public void HotRowsAdded() {
            if (RowAdd != null) {
                RowAdd();
            }
        }

        public ProductCart(DataGridView dgvc) {
            _dGrid = dgvc;
            _style.BackColor = Color.Red;
            _style.BackColor = Color.Red;
            _defaultStyle.BackColor = Color.White;
            _defaultStyle.ForeColor = Color.Black;
        }

        public void AddItem(Product product) {
            int productIndex = CheckProductExistance(product.GetId(), product.GetBatch());

            if (productIndex >= 0) {

                product.AddQuantity(Convert.ToDouble(_dGrid.Rows[productIndex].Cells[(int)ProductColumns.Quantity].Value));

                ProductCartToDb.ChangeProductQuantity(product);

                _dGrid.Rows[productIndex].Cells[(int) ProductColumns.Quantity].Value = product.GetQuantity();

                CalculateSubTotal(productIndex);
                SetSelectedItem(productIndex);

            } else {
                product.SetQuantity(product.GetQuantity());
                _productToDb.insertNewProduct(product);

                AddProduct(product);

                if (_dGrid.Rows.Count > 0) {
                    SetSelectedItem(_dGrid.Rows.Count - 1);
                } else {
                    SetSelectedItem(0);
                }
            }
        }

        public void AddItemByPrice(double price) {
            _product = new Product();
            _product.SetProductByFields("2500000000001", "Без штрих кода", "Без штрих кода", "шт", price,
                "img/no_image.jpg", 1, 0, 0, "0");

            _productToDb.insertNewProduct(_product);

            AddProduct(_product);

            if (_dGrid.Rows.Count > 0) {
                SetSelectedItem(_dGrid.Rows.Count - 1);
            } else {
                SetSelectedItem(0);
            }
        }

        public void RemoveItem(int index) {
            try {
                if (index < PreviousHighlighted) {
                    PreviousHighlighted -= 1;
                } else if (index == PreviousHighlighted) {
                    PreviousHighlighted = 0;
                }
                _productToDb.deleteProduct(_dGrid.Rows[index].Cells[(int) ProductColumns.Id].Value.ToString(),
                    _dGrid.Rows[index].Cells[(int) ProductColumns.Price].Value.ToString(),
                    _dGrid.Rows[index].Cells[(int) ProductColumns.Quantity].Value.ToString(),
                    _dGrid.Rows[index].Cells[(int) ProductColumns.Batch].Value.ToString());
                _dGrid.Rows.RemoveAt(index);
            } catch (Exception ex) {
                Logger.Log(ex, "RemoveItem");
            }
        }

        public void Clear() {
            _dGrid.Rows.Clear();
        }

        public void Remove() {
            _dGrid.Rows.Clear();
            _productToDb.deleteAllProducts();
        }


        public void ChangeQuantity(int index, double quantity) {
            var product = new Product();
            product.SetProductByFields(
                _dGrid.Rows[index].Cells[(int)ProductColumns.Id].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Name].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Description].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Unit].Value.ToString(),
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Price].Value),
                null,
                quantity,
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Discount].Value),
                0,
                _dGrid.Rows[index].Cells[(int)ProductColumns.Batch].Value.ToString());

            ProductCartToDb.ChangeProductQuantity(product);

            _dGrid.Rows[index].Cells[(int) ProductColumns.Quantity].Value = quantity;

            CalculateSubTotal(index);
        }

        public void ChangeQuantityByPack(int index, double quantity, short count) {

            quantity = quantity*100/count/100;

            var product = new Product();
            product.SetProductByFields(
                _dGrid.Rows[index].Cells[(int) ProductColumns.Id].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Name].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Description].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Unit].Value.ToString(),
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Price].Value),
                null,
                quantity,
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Discount].Value),
                count,
                _dGrid.Rows[index].Cells[(int)ProductColumns.Batch].Value.ToString());
 
            ProductCartToDb.ChangeProductQuantity(product);

            _dGrid.Rows[index].Cells[(int) ProductColumns.Quantity].Value = quantity;

            CalculateSubTotal(index);
        }


        /// <summary>
        /// Highlights items in DataGridView
        /// </summary>
        /// <param name="itemIndex">Row index to highlight</param>
        private void SetSelectedItem(int itemIndex) {
            //dGrid.CurrentCell = dGrid[0, ItemIndex];
            try {
                _style.SelectionBackColor = Color.Red;
                //dGrid.Select(dGrid.SelectedRows);
                _dGrid.Rows[itemIndex].Cells[0].Style = _style;
                _dGrid.Rows[itemIndex].DefaultCellStyle.SelectionBackColor = Color.Black;
                _dGrid.Rows[itemIndex].Selected = true;

                _dGrid.Rows[itemIndex].Cells[0].Style = _style;
                _dGrid.Rows[itemIndex].Cells[1].Style = _style;
                _dGrid.Rows[itemIndex].Cells[2].Style = _style;
                _dGrid.Rows[itemIndex].Cells[3].Style = _style;
                _dGrid.Rows[itemIndex].Cells[4].Style = _style;
                _dGrid.Rows[itemIndex].Cells[5].Style = _style;
                _dGrid.Rows[itemIndex].Cells[6].Style = _style;
                _dGrid.Rows[itemIndex].Cells[7].Style = _style;
                _dGrid.Rows[itemIndex].Cells[8].Style = _style;

                if (itemIndex != PreviousHighlighted) {
                    ClearHighlighted();
                }

                PreviousHighlighted = (short) itemIndex;
                _dGrid.FirstDisplayedScrollingRowIndex = itemIndex;
            } catch (Exception ex) {
                Logger.Log(ex, "setSelectedItem", Level.Warining);
            }
        }

        private void ClearHighlighted() {
            try {
                if (PreviousHighlighted != -1) {
                    _dGrid.Rows[PreviousHighlighted].Cells[0].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[1].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[2].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[3].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[4].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[5].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[6].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[7].Style = _defaultStyle;
                    _dGrid.Rows[PreviousHighlighted].Cells[8].Style = _defaultStyle;
                }
            } catch (Exception ex) {
                Logger.Log(ex, "clearHighlighted", Level.Warining);
            }
        }

        /// <summary>
        /// Check if product already exists in datagridview
        /// </summary>
        /// <param name="productId">Product Bar Code</param>
        /// <param name="batch">Batch of the product</param>
        /// <returns>Index of Product in DataGrid</returns>
        public int CheckProductExistance(string productId, string batch) {
            foreach (DataGridViewRow row in _dGrid.Rows) {
                if (row.Cells[(int) ProductColumns.Id].Value.Equals(productId)
                    && row.Cells[(int) ProductColumns.Batch].Value.Equals(batch)) {
                    return row.Index;
                }
            }
            return -1;
        }

        public double CalculateTotal() {
            double total = 0;
            foreach (DataGridViewRow row in _dGrid.Rows) {
                var ttl = (Convert.ToDouble(row.Cells[(int) ProductColumns.Price].Value))
                          *(Convert.ToDouble(row.Cells[(int) ProductColumns.Quantity].Value));

                total += ttl - (ttl * (Convert.ToDouble(row.Cells[(int) ProductColumns.Discount].Value)) / 100);
            }
            return Math.Round(total, 2);
        }

        private void CalculateSubTotal(int index) {
            var ttl = (Convert.ToDouble(_dGrid.Rows[index].Cells[(int) ProductColumns.Price].Value))*
                      (Convert.ToDouble(_dGrid.Rows[index].Cells[(int) ProductColumns.Quantity].Value));
            ttl -= (ttl*(Convert.ToDouble(_dGrid.Rows[index].Cells[(int) ProductColumns.Discount].Value))/100);
            _dGrid.Rows[index].Cells[(int) ProductColumns.Total].Value =
                Math.Round(ttl, 2);
        }


        public bool SaveBasket() {
            _savedPreviousBasketId = _productToDb.storeBasket();
            if (_savedPreviousBasketId > 0) {
                _dGrid.Rows.Clear();
                return true;
            }

            return false;
        }

        public bool RestoreBasket() {
            if (_savedPreviousBasketId > 0) {
                _dGrid.Rows.Clear();
                IEnumerable<Product> products = _productToDb.restoreBasket(_savedPreviousBasketId);
                foreach (Product p in products) {
                    AddProduct(p);
                }
                ProductCartToDb.Id = _savedPreviousBasketId;
                return true;
            }
            return false;
        }

        private void AddProduct(Product product) {
            _dGrid.Rows.Add(product.GetId(), product.GetName(), product.GetDescription(), product.GetQuantity(),
                product.GetUnit(), product.GetBatch(), product.GetPrice(), product.GetDiscount(), product.GetTotal());
            MainWorkForm._product = product;
        }

        public void ChangeDiscount(int index, double discount) {
            var product = new Product();
            product.SetProductByFields(
                _dGrid.Rows[index].Cells[(int)ProductColumns.Id].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Name].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Description].Value.ToString(),
                _dGrid.Rows[index].Cells[(int)ProductColumns.Unit].Value.ToString(),
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Price].Value),
                null,
                Convert.ToDouble(_dGrid.Rows[index].Cells[(int)ProductColumns.Price].Value),
                discount,
                0,
                _dGrid.Rows[index].Cells[(int)ProductColumns.Batch].Value.ToString());

            ProductCartToDb.ChangeDiscount(product);

            _dGrid.Rows[index].Cells[(int)ProductColumns.Discount].Value = discount;

            CalculateSubTotal(index);
        }
    }
}