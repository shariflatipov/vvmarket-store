﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Gecko;

namespace magazin_client.ff {
    public partial class MainBrowser : Form {

        private readonly GeckoWebBrowser _browser = new GeckoWebBrowser {Dock = DockStyle.Fill};

        public MainBrowser() {
            InitializeComponent();
            Xpcom.Initialize(@"xul");
            GeckoPreferences.User["extensions.blocklist.enabled"] = false;

            _browser.DomClick += DomClick;
            _browser.DocumentCompleted += BrowserDocumentCompleted;

            Controls.Add(_browser);
            _browser.Navigate(MainForm.Ip);
        }


        void BrowserDocumentCompleted(object sender, EventArgs e) {
        }


        private void DomClick(object sender, DomEventArgs e) {
        }
    }
}
