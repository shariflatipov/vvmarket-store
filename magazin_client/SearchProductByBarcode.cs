﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace magazin_client {
    public partial class SearchProductByBarcode : Form {

        private DataTable dt = new DataTable();

        public SearchProductByBarcode() {
            InitializeComponent();
        }

        private void SearchProductByBarcode_Load(object sender, EventArgs e) {
            try {
                CreateDB.cmd.CommandText = "select top(10) product_id, product_name, product_image from product";
                ImageList im = new ImageList();
                using(SqlCeDataReader reader = CreateDB.cmd.ExecuteReader()) {
                    int i = 0;
                    im.ImageSize = new System.Drawing.Size(100, 100);

                    while(reader.Read()) {
                        try {
                            im.Images.Add(new Bitmap(reader[2].ToString()));
                        } catch(Exception) {
                            im.Images.Add(new Bitmap("img/no_image.jpg"));
                        }
                        listView1.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                        i++;
                    }
                }
            } catch(Exception ex) {
                Logger.Log(ex, "SearchProductByBarcode");
            } finally {

            }
        }
    }
}
