﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Media;
using magazin_client.discount;
using magazin_client.goodsreturn;
using magazin_client.peripheral;
using magazin_client.utils;

namespace magazin_client {

    public partial class MainWorkForm : Form {

        private readonly SoldProduct _soldProduct = new SoldProduct();
        private double _totalCost;
        private readonly int _screenHeight = Screen.PrimaryScreen.Bounds.Height;
        private readonly int _screenWidth = Screen.PrimaryScreen.Bounds.Width;
        private string _tempBarCode = "";
        private readonly ProductCart _productCart;
        private DependentList[] _depList;
        public static Product _product = new Product();


        public readonly PosiflexPD2600 Vfd = new PosiflexPD2600();

        public MainWorkForm() {
            InitializeComponent();
            KeyPreview = true;
            picUsrLogo.Image = Utils.GetImageFromPath("img/logo.png");
            _productCart = new ProductCart(dgProductCart);
            _productCart.RowAdd += SetProductSettings;
        }

        private void CalculateTotal() {
            _totalCost = _productCart.CalculateTotal();
            lblTotal.Text = _totalCost.ToString();
        }

        private void MainWorkForm_Load(object sender, EventArgs e) {
            // Geting Hot Plug Items into List
            DependentList.GetListContent(lstHotAccess, imLstPics, "MainList");
            _depList = new DependentList[lstHotAccess.Items.Count];
            lstHotAccess.LargeImageList = imLstPics;
            lstHotAccess.MouseClick += ShowHotPlug;
            _soldProduct.InsertProduct(MainForm.Id);
            ProductCartToDb.Id = _soldProduct.GetMaxId();
        }

        private void ShowHotPlug(object sender, EventArgs e) {
            var a = (ListView)sender;
            if (_depList[a.SelectedItems[0].Index] == null) {
                _depList[a.SelectedItems[0].Index] = new DependentList(_productCart, a.SelectedItems[0].Name) {
                    MdiParent = MdiParent,
                    Dock = DockStyle.Fill
                };
            }
            _depList[a.SelectedItems[0].Index].Show();
        }

        private void BtnConfirmBuyClick(object sender, EventArgs e) {
            if (dgProductCart.Rows.Count > 0) {
                _productCart.PreviousHighlighted = -1;
                var chb = new ChangeBack(_totalCost, dgProductCart, Vfd) {
                    MdiParent = MdiParent,
                    Dock = DockStyle.Fill
                };
                chb.Show();
            }
        }

        private void DgProductCartCellClick(object sender, DataGridViewCellEventArgs e) {
            if(e.ColumnIndex >= 0 && e.RowIndex >= 0) {
                if(e.ColumnIndex == (int)ProductColumns.Delete) { // Button delete goods
                    _productCart.RemoveItem(e.RowIndex);
                    ClearFormLbl();
                    CalculateTotal();
                } else if(e.ColumnIndex == (int)ProductColumns.Quantity) {// Change quantity of goods and calculate total
                    Nums nums = new Nums(_productCart, e.RowIndex);
                    nums.ShowDialog();
                } else if (e.ColumnIndex == (int) ProductColumns.Unit) {
                    // Set pack count
                    Product pr = _productCart.GetCurrentProduct(e.RowIndex);
                    if (pr.GetPackCount() <= 0) {
                        MessageBox.Show(@"Операция запрещена");
                    } else {
                        var chp = new ChangePack(_productCart, e.RowIndex, pr.GetPackCount());
                        chp.ShowDialog();
                    }
                } else if (e.ColumnIndex == (int)ProductColumns.Discount) {
                    var discount = new NumsChangeDiscount(_productCart, e.RowIndex);
                    discount.ShowDialog();
                }
            }
        }

        // Logout event
        private void PicUsrLogoClick(object sender, EventArgs e) {
            ClearDgProduct();
            var au = MdiParent as MainForm;
            if (au != null) {
                au.Auth.MdiParent = MdiParent;
                au.Auth.Show();
            }
            Hide();
        }

        // Clear all labels in form
        private void ClearFormLbl() {
            lblProductCost.Text = "";
            lblProductDescription.Text = "";
            lblProductName.Text = "";
            picProduct.Image = null;
            lblTotal.Text = "";
            lblCurProductName.Text = "";
            lblCurProductPrice.Text = "";
            lblCurProductQuantity.Text = "";
        }


        public void ClearDgProduct() {
            _productCart.Clear();
            ClearFormLbl();
        }

        private void MainWorkFormKeyDown(object sender, KeyEventArgs e) {
            if (e.KeyValue >= 48 && e.KeyValue <= 58) {           // Только цифры от 0 до 9 и Enter
                _tempBarCode += e.KeyCode.ToString();
            } else if (e.KeyCode == Keys.Enter) {                 // Штрих код полностью прочитан
                //long tics = DateTime.Now.Ticks;
                //Console.WriteLine("------ START --------   {0} ", tics);
                _tempBarCode = _tempBarCode.Replace("D", "");
                string prCode = _tempBarCode;
                double weight = 1;

                try {
                    if (_tempBarCode.Substring(0, 2) == "20") {
                        // Bar code generated by scale 
                        if (_tempBarCode.Length == 12) {
                            prCode = Convert.ToInt32(_tempBarCode.Substring(2, 4))
                                .ToString();
                            weight = Math.Round((Convert.ToSingle(_tempBarCode.Substring(6, 5))/1000), 3);
                        } else if (_tempBarCode.Length == 13) {
                            prCode = Convert.ToInt32(_tempBarCode.Substring(2, 5))
                                .ToString();
                            weight = Math.Round((Convert.ToSingle(_tempBarCode.Substring(7, 5))/1000), 3);
                        }
                    }

                    var products = Product.GetProducts(prCode);

                    switch (products.Count) {
                        case 0:
                            _product.SetProductByFields("", "Товар не зарегистрирован", "", "", 0, "img/no_goods.jpg", 0, 0, 0, "0");
                            break;
                        case 1:
                            _product = products[0];
                            break;
                        default:
                            var chooseBatch = new ChooseBatch(products);
                            chooseBatch.ShowDialog();
                            if (chooseBatch.Batch == "") {
                                chooseBatch.Dispose();
                                return;
                            }

                            foreach (var product in products.Where(product => product.GetBatch().Equals(chooseBatch.Batch))) {
                                _product = product;
                            }

                            chooseBatch.Dispose();
                            break;
                    }
                    if (_product.GetId() != "") {
                        _product.SetQuantity(weight);
                        _productCart.AddItem(_product);
                    }
                    SetProductSettings();
                } catch (Exception ex) {
                    SystemSounds.Hand.Play();
                    Logger.Log(ex, "MainWorkForm.cs : function => MainWorkFormKeyDown");
                } // Gets here if tempBarCode length equals 1
                finally {
                    _tempBarCode = "";
                }
                //long diff = DateTime.Now.Ticks;
                //Console.WriteLine("------ END ------  {0} ", diff);
                //Console.WriteLine("----- DIFF -----   {0} ", diff - tics);
            } else if (e.KeyCode == Keys.F3) {                    // Добавить товар без штрих кода
                picProductCost_Click(sender, e);
            } else if (e.KeyCode == Keys.F1) {                    // Оформить покупку 
                BtnConfirmBuyClick(sender, e);
            } else if (e.KeyCode == Keys.F2) {                    // Очистить все товары
                ClearDataClick(sender, e);
            } else if (e.KeyCode == Keys.F9) {                    // Discount
                var df = new DiscountListForm();
                df.ShowDialog();
            } else if (e.KeyCode == Keys.F10) {                   // Return
                var retMain = new ReturnMain();
                retMain.ShowDialog();
            }
        }

        private void SetProductSettings() {
            try {
                lblCurProductName.Text = _product.GetName();
                lblCurProductPrice.Text = _product.GetPrice().ToString();
                lblCurProductQuantity.Text = _product.GetQuantity().ToString();
                lblProductCost.Text = _product.GetPrice().ToString();
                lblProductDescription.Text = _product.GetDescription();
                lblProductName.Text = _product.GetName();
                picProduct.Image = _product.GetImage();

                CalculateTotal();

                if (!Vfd.IsNumeric) {
                    string line1;
                    string l1;

                    if (_product.GetName().Length > 12) {
                        line1 = _product.GetName().Substring(0, 12);
                        l1 = _product.GetPrice().ToString().PadLeft(8);
                    } else {
                        line1 = _product.GetName().PadLeft(12);
                        l1 = _product.GetPrice().ToString().PadLeft(8);
                    }

                    Vfd.WriteFull(line1 + l1, lblTotal.Text.PadLeft(13));
                    //Vfd.Write("Итого :" + lblTotal.Text.PadLeft(13));
                } else {
                    Vfd.Write(_product.GetPrice().ToString());
                }
            } catch (Exception ex) {
                Logger.Log(ex, "MainWorkForm.cs : function => SetProductSettings");
            }
        }

        // Delete all goods image
        private void ClearDataClick(object sender, EventArgs e) {
            if (dgProductCart.Rows.Count <= 0) return;
            _product = new Product();
            _productCart.Remove();
            _productCart.PreviousHighlighted = -1;
            ClearFormLbl();
        }

        // Save cart image
        private void SaveDataClick(object sender, EventArgs e) {
            _productCart.SaveBasket();
            ClearFormLbl();
            _soldProduct.InsertProduct(MainForm.Id);
            ProductCartToDb.Id = _soldProduct.GetMaxId();
        }

        // Restore cart image
        private void RestoreDataClick(object sender, EventArgs e) {
            _productCart.RestoreBasket();
            ClearFormLbl();
            CalculateTotal();
        }

        // Good without barcode image
        private void picProductCost_Click(object sender, EventArgs e) {
            var frmNum = new NumsByPrice(_productCart);
            frmNum.ShowDialog();
        }

        // Create new form with several settings 
        private void PicCart_Click(object sender, EventArgs e) {
            var sf = new SettingsForm();
            sf.ShowDialog();
        }

     }
}