﻿using System;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace magazin_client {
    public partial class AuthForm : Form {

        private string _msisdn = "";

        public AuthForm() {
            InitializeComponent();
            KeyPreview = true;

            GetUsers();
        }

        private void GetUsers() {
            using (var conn = new SqlCeConnection(CreateDB.ConnString)) {
                using (var cmd = new SqlCeCommand("SELECT login FROM seller where login not like '%admin%'", conn)) {
                    conn.Open();
                    using (SqlCeDataReader reader = cmd.ExecuteReader()) {
                        while (reader.Read()) {
                            Login.Items.Add(reader[0].ToString());
                        }
                    }
                }
            }
        }

        private void UserAuth(object sender, EventArgs e) {
            var seller = new SellerSettings();
            if (seller.getUser(Login.Text, Pass.Text) == 1) {
                if (Login.Text == "administrator") {
                    AddUserForm auf = new AddUserForm();
                    auf.MdiParent = this.MdiParent;
                    auf.Show();
                } else {
                    MainForm main = this.MdiParent as MainForm;
                    MainForm.Id = seller.userId;
                    MainForm.Login = seller.userLogin;
                    MainForm.Pass = seller.userPass;

                    main.Dock = DockStyle.Fill;

                    main.MWork.MdiParent = this.MdiParent;
                    main.MWork.Show();
                    main.MWork.Dock = DockStyle.Fill;
                }

                Login.Text = "";
                Pass.Text = "";
                Hide();
            } else {
                MessageBox.Show(this, "Error login pass", "Login/Pass", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnExitClick(object sender, EventArgs e) {
            var prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = "explorer";
            try {
                prc.Start();
            } catch (Exception excep) {
                MessageBox.Show(excep.ToString());
            }
            Application.Exit();
        }

        private void AuthFormLoad(object sender, EventArgs e) {
            if (System.Configuration.ConfigurationManager.AppSettings["isBilyard"] == "True") {
                btnExit.Visible = false;
            }
        }

        private void AuthForm_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                UserAuth(sender, e);
            }
        }

        private void NumValidation(object sender, EventArgs e) {
            Button btn = (Button)sender;
            string tmpText = btn.Text.ToLower();

            if (tmpText == "backspace") {
                if (_msisdn.Length > 0)
                    _msisdn = _msisdn.Substring(0, _msisdn.Length - 1);
            } else if (tmpText == "clear") {
                _msisdn = "";
            } else {
                _msisdn += tmpText;
            }

            Pass.Text = _msisdn;
        }
    }
}