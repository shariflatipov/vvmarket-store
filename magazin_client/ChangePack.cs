﻿using System;

namespace magazin_client {
    public partial class ChangePack : Nums {
        private readonly short _count;

        public ChangePack(ProductCart pc, int index, short count) : base(pc, index) {
            _count = count;
            lblInfo.Text = "Количество в пачке" + "\r\n" + count;
        }


        protected override void ConfirmClick(object sender, EventArgs e) {
            double count = 0;
            try {
                count = Convert.ToDouble(lblCount.Text);
            }
            catch (Exception ex) {
                Logger.Log(ex, "ChangePack.cs : function => ConfirmClick()");
            }

            if (count > 0) {
                pc.ChangeQuantityByPack(rowIndex, count, _count);
                pc.HotRowsAdded();
            }
            Dispose();
        }
    }
}
