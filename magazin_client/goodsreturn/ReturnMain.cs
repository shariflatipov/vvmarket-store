﻿using System;
using System.Windows.Forms;

namespace magazin_client.goodsreturn {
    public partial class ReturnMain : Form {
        
        private ReturnGood _returnGood = new ReturnGood();

        public ReturnMain() {
            InitializeComponent();
        }

        
        private void TxtBarcodeTextChanged(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                if (dgReturn.Rows.Count > 0) {
                    dgReturn.Rows.Clear();
                }

                _returnGood = ReturnGood.GetById(txtBarcode.Text);

                foreach (var item in _returnGood.ReturnProductList) {
                    dgReturn.Rows.Add(item.TrnId, item.ProductName, item.Count, item.Price, item.Batch, item.Discont);
                }
            }
        }

        private void DgReturnCellClick(object sender, DataGridViewCellEventArgs e) {
            if (e.ColumnIndex >= 0 && e.RowIndex >= 0) {
                if (e.ColumnIndex == 6) { // Button delete goods
                    _returnGood.RemoveItem(e.RowIndex);
                    dgReturn.Rows.RemoveAt(e.RowIndex);
                } else if (e.ColumnIndex == 2) {// Change quantity of goods and calculate total
                    NumsReturn nums = new NumsReturn(double.Parse(dgReturn.Rows[e.RowIndex].Cells[2].Value.ToString()));
                    nums.ShowDialog();
                    double quantity = nums.Quantity;
                    dgReturn.Rows[e.RowIndex].Cells[2].Value = quantity;
                    _returnGood.ReturnProductList[e.RowIndex].Count = quantity;
                }
            }
        }

        private void BtnConfirmClick(object sender, EventArgs e) {
            _returnGood.Save();
            Dispose();
        }
    }
}
