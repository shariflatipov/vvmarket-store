﻿using System;

namespace magazin_client.goodsreturn {
    class NumsReturn : Nums {

        public double Quantity { get; private set; }
       
        public NumsReturn(double quantity) : base (null, 0) {
            Quantity = quantity;
        }

        protected override void ConfirmClick(object sender, EventArgs e) {
            double lQuantity;
            if (double.TryParse(lblCount.Text, out lQuantity)) {
                if (lQuantity > 0 && lQuantity <= Quantity) {
                    Quantity = lQuantity;
                }
            }
            Hide();
        }
    }
}
