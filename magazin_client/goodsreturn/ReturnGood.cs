﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Xml.Linq;

namespace magazin_client.goodsreturn {
    public class ReturnProductCollection {
        public Int64 TrnId;
        public string ProductName;
        public double Count;
        public double Price;
        public string Batch;
        public string Product;
        public string Discont;
    }


    public class ReturnGood {

        public readonly List<ReturnProductCollection> ReturnProductList = new List<ReturnProductCollection>();
        private String _soldId;


        public static ReturnGood GetById(string tmpBarcode) {
            ReturnGood result = new ReturnGood {_soldId = tmpBarcode};


            try {

                CreateDB.cmd.CommandText = "SELECT t.id_trn, p.product_name, t.count, t.price, t.batch, p.product_id, t.discont " +
                                           "FROM transactions t, sold_product sp, product p  " +
                                           "WHERE t.sold_id = @sold_id AND sp.status > 1 " +
                                           "AND t.sold_id = sp.id_sold AND p.product_id = t.product_id";

                CreateDB.cmd.Parameters.Add("@sold_id", tmpBarcode);
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                while (reader.Read()) {
                    var rpc = new ReturnProductCollection {
                                                                TrnId =
                                                                    Int64.Parse(reader[0].ToString()),
                                                                ProductName = reader[1].ToString(),
                                                                Count =
                                                                    double.Parse(reader[2].ToString()),
                                                                Price =
                                                                    double.Parse(reader[3].ToString()),
                                                                Batch = reader[4].ToString(),
                                                                Product = reader[5].ToString(),
                                                                Discont = reader[6].ToString()
                                                            };

                    result.ReturnProductList.Add(rpc);
                }
            } catch (Exception e) {
                Logger.Log(e, "ReturnGood.cs=>GetById");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return result;
        }


        public void RemoveItem(int index) {
            ReturnProductList.RemoveAt(index);
        }


        public void Save() {

            using (SqlCeConnection connection = new SqlCeConnection(CreateDB.ConnString)) {
                using (SqlCeCommand command = new SqlCeCommand()) {
                    connection.Open();

                    SqlCeTransaction transaction = connection.BeginTransaction();

                    try {
                        command.Connection = connection;
                        command.Transaction = transaction;
                        command.CommandText = "INSERT INTO retProd(sold_id, seller_id, date) " +
                                              "VALUES (@soldID, @sellerId, @dt)";

                        command.Parameters.Add("@soldID", _soldId);
                        command.Parameters.Add("@sellerId", MainForm.Id);
                        command.Parameters.Add("@dt", DateTime.Now);

                        command.ExecuteNonQuery();
                        command.Parameters.Clear();

                        foreach (var item in ReturnProductList) {
                            command.CommandText =
                                "INSERT INTO return_transactions(sold_id, trn_id, count, sum, batch, product_id, discount) " +
                                "VALUES(@soldId, @trnID, @count, @sum, @batch, @product_id, @discount)";

                            command.Parameters.Add("@soldId", _soldId);
                            command.Parameters.Add("@trnID", item.TrnId);
                            command.Parameters.Add("@count", item.Count);
                            command.Parameters.Add("@sum", item.Price);
                            command.Parameters.Add("@batch", item.Batch);
                            command.Parameters.Add("@product_id", item.Product);
                            command.Parameters.Add("@discount", item.Discont);

                            command.ExecuteNonQuery();

                            command.Parameters.Clear();
                        }
                        transaction.Commit();
                    } catch (Exception e) {
                        Logger.Log(e, "ReturnGood=>Save");
                        transaction.Rollback();
                    } finally {
                        command.Parameters.Clear();
                    }
                }
            }
        }


        public static XElement GetUnfinished() {
            XElement result = new XElement("magazin");
            string pass = "";
            string login = "";
            string dateToSend = "";
            string soldId = "";
            bool flag = false;
            try {
                CreateDB.cmd.CommandText = "SELECT top(1) rp.*, sl.login, sl.pass " +
                                           "from retProd rp, seller sl where state=0 and rp.seller_id = sl.id_seller";

                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                while (reader.Read()) {
                    flag = true;
                    soldId = reader[1].ToString();
                    dateToSend = DateTime.Parse(reader[3].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                    login = reader[5].ToString();
                    pass = reader[6].ToString();
                }

                if (!flag) { // Нет ни одной записи
                    return null;
                }

                string password = MyCrypt.GetMd5Hash(pass);
                string hashSum = MyCrypt.GetSha1(dateToSend + "#" + login + "#" + password);

                var seller = new XElement("seller");
                seller.Add(new XAttribute("login", login));
                seller.Add(new XAttribute("checksum", hashSum));
                seller.Add(new XAttribute("stock", MainForm.Stock));
                seller.Add(new XAttribute("act", 5));
                seller.Add(new XAttribute("date", dateToSend));
                result.Add(seller);

                XElement sold = new XElement("sold");
                sold.Add(new XAttribute("id", soldId));
                sold.Add(new XAttribute("date", dateToSend));

                CreateDB.cmd.CommandText =
                    "SELECT product_id, count, sum, batch, count*sum as total, discount, (sum*discount/100) FROM return_transactions where sold_id = @soldId";
                CreateDB.cmd.Parameters.Add("@soldId", soldId);

                reader = CreateDB.cmd.ExecuteReader();

                double total = 0;
                double totalDiscount = 0;

                while (reader.Read()) {
                    double lTotal;
                    double dTotal;
                    XElement product = new XElement("product");

                    product.Add(new XAttribute("id", reader[0].ToString()));
                    product.Add(new XElement("count", reader[1].ToString().Replace(",", ".")));
                    product.Add(new XElement("price", reader[2].ToString().Replace(",", ".")));
                    product.Add(new XElement("batch", reader[3].ToString()));
                    product.Add(new XElement("discount_percent", reader[5].ToString()));

                    double.TryParse(reader[4].ToString(), out lTotal);
                    total += lTotal;

                    double.TryParse(reader[6].ToString(), out dTotal);
                    totalDiscount += dTotal;

                    sold.Add(product);
                }

                CreateDB.cmd.CommandText = "select discount/sum from sold_product where id_sold=@soldId";
                double koef = (double) CreateDB.cmd.ExecuteScalar();

                sold.Add(new XAttribute("sum", total));
                sold.Add(new XAttribute("discount", total*koef + totalDiscount));

                result.Add(sold);

            } catch (Exception e) {
                Logger.Log(e, "ReturnGood.cs=>GetById");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return result;
        }

        public static void Finish(XElement response) {

            try {
                if (response.Elements().Any()) {
                    Int64 reqTrn = Convert.ToInt64(response.Element("trn").Value);
                    if (response.Element("status").Value.Equals("2")) {
                        using (SqlCeConnection conn = new SqlCeConnection()) {
                            conn.ConnectionString = CreateDB.ConnString;
                            using (SqlCeCommand cmd = new SqlCeCommand()) {
                                conn.Open();
                                try {
                                    cmd.Connection = conn;
                                    cmd.CommandText =
                                        "UPDATE retProd set state = 2 where sold_id = @sold_id";
                                    cmd.Parameters.Add("@sold_id", reqTrn);
                                    cmd.ExecuteNonQuery();
                                } catch (Exception ex) {
                                    Logger.Log(ex, "FinishReturn cannot update ");
                                } finally {
                                    cmd.Parameters.Clear();
                                }
                            }
                        }
                    }
                }
            } catch (Exception excep) {
                Logger.Log(excep, "ReturnGood=>Finish", Level.Warining);
            }
        }
    }
}
