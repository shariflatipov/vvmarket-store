﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Management;
using System.Xml.Linq;
using System.Data.SqlServerCe;


namespace magazin_client {
    public partial class DBConnection : Form {
        private string HDD = "hdd";
        private string baseBoard = "bBoard";
        //private string Date = "date";
        private string isActivate = "isActivate";
        private string ipAddress = "ipAddress";
        private string macAddress = "macAddress";
        private string netPlata = "netPlata";
        public bool active = true;

        private XElement check = new XElement("check");

        public DBConnection() {
            InitializeComponent();

            //GetMACAddress();
            //GetHDDSerial();
            //GetMotherboardSerial();

            string serial = GetMotherboardSerial();

            if (serial == GetDBValues(baseBoard)) {
                if (GetDBValues(isActivate) != "true") {
                    //if (HttpCommunication.HttpCommun(check) == "ok") {
                    //    UpdateDBValues(isActivate, "true");
                    //} else {
                    //    active = false;
                    //    //this.Show();
                    //}
                }
            } else {
                //if (HttpCommunication.HttpCommun(check) == "ok") {
                //    UpdateDBValues(isActivate, "true");
                //    UpdateDBValues(baseBoard, serial);

                //} else {
                //    active = false;
                //    UpdateDBValues(isActivate, "false");
                //    UpdateDBValues(baseBoard, serial);
                //}
            }
        }

        private void DBConnection_Load(object sender, EventArgs e) {

        }

        public void GetMACAddress() {

            ManagementObjectSearcher query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = 'TRUE'");
            ManagementObjectCollection queryCollection = query.Get();

            foreach (ManagementObject mo in queryCollection) {
                check.Add(new XElement("NetworkCard", mo["Description"]));
                check.Add(new XElement("MACAddress", mo["MACAddress"]));

                insertToDB(this.netPlata, mo["Description"].ToString());
                insertToDB(this.macAddress, mo["MACAddress"].ToString());


                XElement ips = new XElement("ipAdresses");
                string[] addresses = (string[])mo["IPAddress"];

                foreach (string ipaddress in addresses) {
                    ips.Add(new XElement("IPAddress", ipaddress));
                    insertToDB(this.ipAddress, ipaddress);
                }
                check.Add(ips);
            }
        }

        public string GetHDDSerial() {
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

            string result = "";
            foreach (var search in searcher.Get()) {
                if (search["SerialNumber"].ToString().Length > 1) {
                    result = search["SerialNumber"].ToString();
                    XElement hdd = new XElement("hdd", result);
                    check.Add(hdd);
                    insertToDB(HDD, result);
                }
            }

            return result;
        }

        public string GetMotherboardSerial() {
            ManagementObjectSearcher searcher =
                new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");

            string result = "";

            foreach (var search in searcher.Get()) {
                if (search["SerialNumber"].ToString().Length > 1) {
                    result = search["SerialNumber"].ToString();
                    XElement bBoard = new XElement("bBoard", result);
                    check.Add(bBoard);
                    insertToDB(this.baseBoard, result);
                }
            }

            return result;
        }

        public void insertToDB(string name, string status) {
            SqlCeConnection conn = new SqlCeConnection(CreateDB.ConnString);
            SqlCeCommand cmd = new SqlCeCommand();

            if (conn.State != ConnectionState.Open) {
                conn.Open();
            }
            try {
                cmd.Connection = conn;
                cmd.Parameters.Add("@name", name);
                cmd.Parameters.Add("@status", status);

                cmd.CommandText = "select status from unit where name_unit = @name";

                string result = (string)cmd.ExecuteScalar();

                if (result == null) {
                    cmd.CommandText = "insert into unit  (name_unit, status) values (@name, @status)";
                    cmd.ExecuteNonQuery();
                }
            } catch (Exception ex) {
                Logger.Log(ex, "DBConnection.cs : function => InsertToDB() => exception");
            } finally {
                cmd.Parameters.Clear();
                conn.Close();
            }
        }

        public string GetDBValues(string param) {
            SqlCeConnection subconn = new SqlCeConnection(CreateDB.ConnString);
            SqlCeCommand subcmd = new SqlCeCommand();
            string result = "";

            if (subconn.State != ConnectionState.Open) {
                subconn.Open();
            }
            subcmd.Connection = subconn;
            subcmd.CommandText = "select status from unit where name_unit = @name";
            subcmd.Parameters.Add("@name", param);
            try {
                result = (string)subcmd.ExecuteScalar();
            } catch (Exception ex) {
                Logger.Log(ex, "DBConnection.cs : function => GetDBValues()");
            } finally {
                subcmd.Parameters.Clear();
                subconn.Close();
            }
            return result;
        }

        public void UpdateDBValues(string param, string value) {
            SqlCeConnection subconn = new SqlCeConnection(CreateDB.ConnString);
            SqlCeCommand subcmd = new SqlCeCommand();

            if (subconn.State != ConnectionState.Open) {
                subconn.Open();
            }
            subcmd.Connection = subconn;
            subcmd.CommandText = "update unit set status = @status where name_unit = @name";
            subcmd.Parameters.Add("@name", param);
            subcmd.Parameters.Add("@status", value);
            try {
                subcmd.ExecuteScalar();
            } catch (Exception ex) {
                Logger.Log(ex, "DBConnection.cs : function => UpdateDBValues()");
            } finally {
                subcmd.Parameters.Clear();
                subconn.Close();
            }
        }
    }
}