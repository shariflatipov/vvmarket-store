﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Xml.Linq;
using System.Linq;

namespace magazin_client.discount {

    public enum AllowedPercents {
        P5 = 5,
        P10 = 10,
        P15 = 15,
        P20 = 20
    }


    public class Discount {
        public string Id { get; set; }  
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Phone { get; set; }
        public string DiscountCode { get; set; }
        public AllowedPercents Percents { get; set; }
        public double Capital { get; set; }
        //private const int State = 0;


        public bool Save() {
            try {
                CreateDB.cmd.CommandText = "insert into discount " +
                                           "(name, surname, patronymic, phone, discount_code, [percent], state, summa) " +
                                           "values(@name, @surname, @patronymic, @phone, @discount_code, @percent, 0, @summa)";
                CreateDB.cmd.Parameters.Add("@name", Name);
                CreateDB.cmd.Parameters.Add("@surname", Surname);
                CreateDB.cmd.Parameters.Add("@patronymic", Patronymic);
                CreateDB.cmd.Parameters.Add("@phone", Phone);
                CreateDB.cmd.Parameters.Add("@discount_code", DiscountCode);
                CreateDB.cmd.Parameters.Add("@percent", Percents);
                CreateDB.cmd.Parameters.Add("@summa", Capital);
                CreateDB.cmd.ExecuteNonQuery();

            } catch (Exception) {
                return false;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return true;
        }


        public static Discount Get(string discountCode) {
            Discount discount = new Discount();
            try {
                
                CreateDB.cmd.CommandText = "select * from discount where discount_code = @dc";
                CreateDB.cmd.Parameters.Add("@dc", discountCode);
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();
                
                while (reader.Read()) {
                    discount.Id = reader[0].ToString();
                    discount.Name = reader[1].ToString();
                    discount.Surname = reader[2].ToString();
                    discount.Patronymic = reader[3].ToString();
                    discount.Phone = reader[4].ToString();
                    discount.DiscountCode = reader[5].ToString();
                    discount.Percents = (AllowedPercents)int.Parse(reader[6].ToString());
                    discount.Capital = double.Parse(reader[8].ToString());
                }
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return discount;
        }


        public static List<Discount> GetList() {
            var results = new List<Discount>();
            try {

                CreateDB.cmd.CommandText = "select * from discount";
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                while(reader.Read()) {

                    var discount = new Discount {
                        Id = reader[0].ToString(),
                        Name = reader[1].ToString(),
                        Surname = reader[2].ToString(),
                        Patronymic = reader[3].ToString(),
                        Phone = reader[4].ToString(),
                        DiscountCode = reader[5].ToString(),
                        Percents = (AllowedPercents) int.Parse(reader[6].ToString())
                    };

                    results.Add(discount);
                }
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return results;
        }


        public bool Update() {
            try {
                CreateDB.cmd.CommandText = "update discount set " +
                                               "name = @name, " +
                                               "surname=@surname, " +
                                               "patronymic=@patronymic, " +
                                               "phone=@phone, " +
                                               "discount_code=@discount_code, " +
                                               "[percent]=@percent, " +
                                               "state=0," +
                                               "summa=@summa " +
                                           "where id=@id";

                CreateDB.cmd.Parameters.Add("@id", Id);
                CreateDB.cmd.Parameters.Add("@name", Name);
                CreateDB.cmd.Parameters.Add("@surname", Surname);
                CreateDB.cmd.Parameters.Add("@patronymic", Patronymic);
                CreateDB.cmd.Parameters.Add("@phone", Phone);
                CreateDB.cmd.Parameters.Add("@discount_code", DiscountCode);
                CreateDB.cmd.Parameters.Add("@percent", Percents);
                CreateDB.cmd.Parameters.Add("@summa", Capital);
                CreateDB.cmd.ExecuteNonQuery();

            } catch(Exception) {
                return false;
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return true;
        }


        public static List<Discount> GetUnfinishedStates() {
            var results = new List<Discount>();
            try {

                CreateDB.cmd.CommandText = "select * from discount where state = 0";
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                while(reader.Read()) {

                    var discount = new Discount {
                        Id = reader[0].ToString(),
                        Name = reader[1].ToString(),
                        Surname = reader[2].ToString(),
                        Patronymic = reader[3].ToString(),
                        Phone = reader[4].ToString(),
                        DiscountCode = reader[5].ToString(),
                        Percents = (AllowedPercents)int.Parse(reader[6].ToString()),
                        Capital = double.Parse(reader[8].ToString())
                    };

                    results.Add(discount);
                }
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return results;
       
        }


        public static bool FinishState(string id, XElement response) {
            try {
                if(response.Elements().Any()) {
                    var xElement = response.Element("status");
                    if (xElement != null && xElement.Value.Equals("2")) {
                        try {
                            CreateDB.cmd.CommandText = "update discount set state=1 where id=@id";
                            CreateDB.cmd.Parameters.Add("@id", id);
                            CreateDB.cmd.ExecuteNonQuery();

                        } catch (Exception) {
                            return false;
                        } finally {
                            CreateDB.cmd.Parameters.Clear();
                        }
                    } else {
                        return false;
                    }
                }
            } catch(Exception excep) {
                Logger.Log(excep, "CreateXML.cs : function => HandleResponseFromServer()");
                return false;
            }
            return true;
        }



        public static XElement Serialize(Discount discount) {
            // <magazin>
                // <seller login="sm1kassa2" stock="kassa2" 
                      //date="2013-07-03 15:02:25" checksum="93ddde78c632af5b550f45dd1be4e1d35192" act="4"></seller>
                // <discount  first_name="Бахтиёр" 
                      //last_name="Муротов"  patronymic="Маруфович" phone="9277535" discount_code="00002354" percent="20"/> 
            //</magazin>

            DateTime b = DateTime.Now;

            string dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");

            string password = MyCrypt.GetMd5Hash(MainForm.Pass);
            string hashSum = MyCrypt.GetSha1(dateToSend + "#" + MainForm.Login + "#" + password);

            XElement seller = new XElement("seller");
            seller.Add(new XAttribute("login", MainForm.Login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", 4));
            seller.Add(new XAttribute("date", dateToSend));

            XElement element = new XElement("discount");
            element.Add(new XAttribute("first_name", discount.Name));
            element.Add(new XAttribute("last_name", discount.Surname));
            element.Add(new XAttribute("patronymic", discount.Patronymic));
            element.Add(new XAttribute("phone", discount.Phone));
            element.Add(new XAttribute("discount_code", discount.DiscountCode));
            element.Add(new XAttribute("percent", (int)discount.Percents));
            element.Add(new XAttribute("capital", discount.Capital));


            XElement magazin = new XElement("magazin");
            magazin.Add(new XElement(seller));
            magazin.Add(element);
            return magazin;
        }


        public static bool ChangeSum(double sum, Discount discount) {
            try {

                discount.Capital += sum;

                if(discount.Capital >= 2000 && discount.Capital <= 2999) {
                    discount.Percents = AllowedPercents.P5;
                } else if(discount.Capital >= 3000 && discount.Capital <= 3999) {
                    discount.Percents = AllowedPercents.P10;
                } else if(discount.Capital >= 4000 && discount.Capital <= 5999) {
                    discount.Percents = AllowedPercents.P15;
                } else if(discount.Capital > 5999) {
                    discount.Percents = AllowedPercents.P20;
                }

                discount.Update();

            } catch(Exception) {
                return false;
            } 

            return true;
        }
    }
}
