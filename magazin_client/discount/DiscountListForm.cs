﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace magazin_client.discount {
    public partial class DiscountListForm : Form {
        public DiscountListForm() {
            InitializeComponent();
        }


        private void DiscountListForm_Load(object sender, EventArgs e) {
            List<Discount> discounts = Discount.GetList();

            foreach (var discount in discounts) {
                dataGridView1.Rows.Add(discount.Id, discount.Name, discount.Surname, discount.Patronymic, discount.Phone,
                    discount.DiscountCode, discount.Percents);
            }
        }


        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e) {
            var dForm = new DiscountAddEditForm(dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString());
            dForm.ShowDialog();
        }


        private void btnCreate_Click(object sender, EventArgs e) {
            var dForm = new DiscountAddEditForm();
            dForm.ShowDialog();
        }


        private void btnExit_Click(object sender, EventArgs e) {
            Dispose();
        }
    }
}
