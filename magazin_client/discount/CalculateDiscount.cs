﻿using System;
using System.Windows.Forms;

namespace magazin_client.discount {
    public partial class CalculateDiscount : Form {
        
        public Discount discount { get; set; }
        private string _tempBarCode ;

        public CalculateDiscount() {
            InitializeComponent();
        }


        private void CalculateDiscount_KeyDown(object sender, KeyEventArgs e) {
            if(e.KeyValue >= 48 && e.KeyValue <= 58) {           // Только цифры от 0 до 9 и Enter
                _tempBarCode += e.KeyCode.ToString();
            } else if(e.KeyCode == Keys.Enter && _tempBarCode.Length > 3) {                 // Штрих код полностью прочитан
                
                _tempBarCode = _tempBarCode.Replace("D", "");

                try {
                    discount = Discount.Get(_tempBarCode);
                    if (discount.Capital <= 0) {
                        MessageBox.Show("Карта не является действительной");
                    }
                } catch (Exception exception) {
                    Logger.Log(exception, "CalculateDiscount");
                }finally {
                    _tempBarCode = "";
                }
                Dispose();
            } 
        }
    }
}
