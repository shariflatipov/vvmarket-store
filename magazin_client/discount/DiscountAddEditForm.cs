﻿using System;
using System.Windows.Forms;

namespace magazin_client.discount {
    public partial class DiscountAddEditForm : Form {

        private bool _flagEdit = false;
        private Discount _discount = null;

        public DiscountAddEditForm() {
            InitializeComponent();
        }

        public DiscountAddEditForm(string  discountCode) {
            _flagEdit = true;
            InitializeComponent();

            _discount = Discount.Get(discountCode);

            txtName.Text = _discount.Name;
            txtDiscountCode.Text = _discount.DiscountCode;
            txtPatronymic.Text = _discount.Patronymic;
            txtPhone.Text = _discount.Phone;
            txtSurname.Text = _discount.Surname;

            int selectedIndex = 0;

            switch (_discount.Percents) {
                case AllowedPercents.P5:
                    selectedIndex = 0;
                    break;
                case AllowedPercents.P10:
                    selectedIndex = 1;
                    break;
                case AllowedPercents.P15:
                    selectedIndex = 2;
                    break;
                case AllowedPercents.P20:
                    selectedIndex = 3;
                    break;
            }

            cmbPercent.SelectedIndex = selectedIndex;
        }


        private void button1_Click(object sender, EventArgs e) {
            if (_flagEdit) {
                _discount.Name = txtName.Text;
                _discount.DiscountCode = txtDiscountCode.Text;
                _discount.Patronymic = txtPatronymic.Text;
                _discount.Phone = txtPhone.Text;
                _discount.Surname = txtSurname.Text;
                _discount.Percents = (AllowedPercents) int.Parse(cmbPercent.SelectedItem.ToString());
                if (IsValid()) {
                    if (_discount.Update()) {
                        Dispose();
                    } else {
                        MessageBox.Show("Не возможно обновить запись");
                    }

                } else {
                    MessageBox.Show("Заполните все поля!");
                }
            } else {
                if (IsValid()) {

                    var discount = new Discount {
                        Name = txtName.Text,
                        DiscountCode = txtDiscountCode.Text,
                        Patronymic = txtPatronymic.Text,
                        Phone = txtPhone.Text,
                        Surname = txtSurname.Text,
                        Percents = (AllowedPercents) int.Parse(cmbPercent.SelectedItem.ToString())
                    };

                    if (discount.Save()) {
                        Dispose();
                    } else {
                        MessageBox.Show("Данный пользователь существует");
                    }
                } else {
                    MessageBox.Show("Заполните все поля!");
                }
            }
        }

        private bool IsValid() {
            return !String.IsNullOrWhiteSpace(txtDiscountCode.Text) && !String.IsNullOrWhiteSpace(txtName.Text) && !String.IsNullOrWhiteSpace(cmbPercent.Text);
        }

        private void button2_Click(object sender, EventArgs e) {
            Dispose();
        }
    }
}
