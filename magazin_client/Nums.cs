﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace magazin_client {
    public partial class Nums : Form {

        protected int rowIndex;
        protected ProductCart pc;


        public Nums(ProductCart pc, int index) {
            rowIndex = index;
            this.pc = pc;
            InitializeComponent();
            
            lblInfo.Text = "Введите количество";
        }

        private void CancelClick(object sender, EventArgs e) {
            Dispose();
        }

        protected virtual void ConfirmClick(object sender, EventArgs e) {
            double count = 0;
            try {
                count = Convert.ToDouble(lblCount.Text);
            } catch (Exception ex) {
                Logger.Log(ex, "Nums.cs : function => ConfirmClick()");
            }

            if (count > 0) {
                pc.ChangeQuantity(rowIndex, count);
                pc.HotRowsAdded();
            }
            Dispose();
        }

        private void BtnClick(object sender, EventArgs e) {
            var num = (Button)sender;
            lblCount.Text += num.Name.Substring(4);
        }

        private void ClearClick(object sender, EventArgs e) {
            lblCount.Text = "";
        }

        private void BackClick(object sender, EventArgs e) {
            if (!lblCount.Text.Contains(',')) {
                lblCount.Text += ",";
            }
        }

        private void DeactivateDispose(object sender, EventArgs e) {
            Dispose();
        }

        private void NumsKeyDown(object sender, KeyEventArgs e) {
            string key = "";
            key = utils.Utils.KeyToChar(e.KeyCode.ToString());

            if (key.Equals("dec")) { // ,
                BackClick(null, null);
            } else if (key.Equals("esc")) {
                CancelClick(null, null);
            } else if (key.Equals("clr")) {
                ClearClick(null, null);
            } else {
                lblCount.Text += key;
            }
        }
    }
}