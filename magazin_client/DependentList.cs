﻿using System;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Drawing;
using magazin_client.utils;

namespace magazin_client {
    public partial class DependentList : Form {

        private string productId;
        private ProductCart productCart;
        private int SCREEN_HEIGHT = Screen.PrimaryScreen.Bounds.Height;

        public DependentList(ProductCart pc, string id) {

            InitializeComponent();
            productId = id;
            productCart = pc;
        }

        private void DependentList_Load(object sender, EventArgs e) {

            GetListContent(depImgList, "DepList", productId);

            depList.Height = SCREEN_HEIGHT - 100;
            depList.LargeImageList = depImgList;
            depList.MouseClick += depList_MouseClick;
        }

        private void depList_MouseClick(object sender, MouseEventArgs e) {
            var a = (ListView)sender;

            var nByBarCode = new NumsByBarCode(productCart, a.SelectedItems[0].Name, a.SelectedItems[0].Text);
            nByBarCode.ShowDialog();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e) {
            Hide();
        }

        private void button2_Click(object sender, EventArgs e) {
            depList.Clear();
            depImgList.Images.Clear();
            GetListContent(depImgList, "DepList", productId);
        }

        private void button4_Click(object sender, EventArgs e) {
            if(depList.Items.Count > 0) {
                depList.EnsureVisible(depList.Items.Count - 1);
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            if(depList.Items.Count > 0) {
                depList.EnsureVisible(0);
            }
        }

        private void GetListContent(ImageList im, string commandType, string depId = "") {
            switch (commandType) {
                case "MainList":
                    CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_image FROM product where is_list = 1";
                    break;
                case "DepList":
                    CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_image, batch FROM  product where dependent = @dep order by product_name";
                    CreateDB.cmd.Parameters.Add("@dep", depId);
                    break;
            }
            try {
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();
                int i = 0;
                im.ImageSize = new Size(100, 100);

                while(reader.Read()) {
                    im.Images.Add(Utils.GetImageFromPath(reader[2].ToString()));
                    if (commandType.Equals("MainList")) {
                        depList.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                    } else if(commandType.Equals("DepList")) {
                        depList.Items.Add(reader[0].ToString(), reader[1] + "#" + reader[3], i);
                    }
                    i++;
                }
            } catch(Exception ex) {
                Logger.Log(ex, "SQLCommunication.cs : function => GetListContent()");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
        }

        public static void GetListContent(ListView depList, ImageList im, string commandType, string depId = "") {
            switch (commandType) {
                case "MainList":
                    CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_image FROM product where is_list = 1";
                    break;
                case "DepList":
                    CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_image FROM  product where dependent = @dep";
                    CreateDB.cmd.Parameters.Add("@dep", depId);
                    break;
            }
            try {
                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();
                int i = 0;
                im.ImageSize = new Size(100, 100);

                while(reader.Read()) {
                    im.Images.Add(Utils.GetImageFromPath(reader[2].ToString()));
                    depList.Items.Add(reader[0].ToString(), reader[1].ToString(), i);
                    i++;
                }
            } catch(Exception ex) {
                Logger.Log(ex, "SQLCommunication.cs : function => GetListContent()");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
        }
    }
}