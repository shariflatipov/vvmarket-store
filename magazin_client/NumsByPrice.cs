﻿using System;

namespace magazin_client {
    internal class NumsByPrice : Nums {

        public NumsByPrice(ProductCart pc)
            : base(pc, 0) {
            lblInfo.Text = "Товар без штрихкода";
        }

        protected override void ConfirmClick(object sender, EventArgs e) {
            double price = 1;
            try {
                price = Convert.ToDouble(lblCount.Text);
            }
            catch (Exception ex) {
                Logger.Log(ex, "NumsByPrice.cs : function => confirm_click()");
            }

            if (price > 0) {
                pc.AddItemByPrice(price);
                pc.HotRowsAdded();
                Dispose();
            }
        }
    }
}