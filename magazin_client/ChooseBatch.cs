﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace magazin_client {
    public partial class ChooseBatch : Form {

        public string Batch;


        public ChooseBatch(List<Product> products) {
            InitializeComponent();
            int i = 0;
            //var labels = new Label[products.Count];
            //var buttons = new Button[products.Count];
            lblProductName.Text = products[0].GetName();
            foreach (var product in products) {
                var lbl1 = new Label {
                    Text = product.GetBatch().ToString(),
                    Font = new Font("Microsoft Sans Serif", 18F, FontStyle.Regular, GraphicsUnit.Point, (204)),
                    Name = "labels" + i,
                    Size = new Size(150, 60),
                    TextAlign = ContentAlignment.MiddleCenter,
                    Location = new Point(0, (70 * i) + 150)
                };

                var lbl2 = new Label {
                    Text = product.GetPrice().ToString(),
                    Font = new Font("Microsoft Sans Serif", 18F, FontStyle.Regular, GraphicsUnit.Point, (204)),
                    Name = "labels" + i,
                    Size = new Size(150, 60),
                    Location = new Point(150, (70 * i) + 150),
                    TextAlign = ContentAlignment.MiddleCenter
                };


                var btn = new Button {
                    Name = product.GetBatch().ToString(),
                    Text = @"Выбрать",
                    Size = new Size(150, 60),
                    Location = new Point(300, (70 * i) + 150)
                };

                btn.Click += btn_Click;

                Controls.Add(lbl1);
                Controls.Add(lbl2);
                Controls.Add(btn);
                i++;
            }
        }

        void btn_Click(object sender, EventArgs e) {
            Batch = ((Button)sender).Name;
            Hide();
        }

        private void button1_Click(object sender, EventArgs e) {
            Batch = "";
            Hide();
        }
    }
}
