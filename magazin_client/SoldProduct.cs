﻿using System;
using System.Data.SqlServerCe;

namespace magazin_client {
    class SoldProduct {
       
        /// <summary>
        /// 
        /// </summary>
        /// <returns>Max sold id</returns>
        public Int64 GetMaxId() {
            Int64 res;

            CreateDB.cmd.CommandText = "SELECT max(id_sold) FROM sold_product";

            try {
                res = Convert.ToInt64(CreateDB.cmd.ExecuteScalar());
            } catch (InvalidCastException ex) {
                Logger.Log(ex, "SQLCommunication.cs : function => GetMaxId()");
                res = 1;
            } 
            return res;
        }

        /// <summary>
        /// Inserts new sold into sold_product table
        /// </summary>
        /// <param name="sellerId"></param>
        public void InsertProduct(string sellerId) {
            short status = 0;
            double sum = 0;

            try {
                CreateDB.cmd.CommandText = "INSERT INTO sold_product (description, status, end_date, start_date, sum, seller_id)" +
                                                    "VALUES (@desc, @status, @en_dt, @st_dt, @sum, @seller)";

                CreateDB.cmd.Parameters.Add("@desc", "Sold started");
                CreateDB.cmd.Parameters.Add("@status", status);
                CreateDB.cmd.Parameters.Add("@en_dt", DateTime.Now);
                CreateDB.cmd.Parameters.Add("@st_dt", DateTime.Now);
                CreateDB.cmd.Parameters.Add("@sum", sum);
                CreateDB.cmd.Parameters.Add("@seller", sellerId);
                CreateDB.cmd.ExecuteNonQuery();
            } catch (Exception ex) {
                Logger.Log(ex, "SQLCommunication.cs : function => InsertProduct()");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
        }

        /// <summary>
        /// Prepare product for further work
        /// </summary>
        /// <param name="total">transaction total</param>
        /// <param name="trnId">transaction id</param>
        /// <param name="discount">discount in money(not percent)</param>
        /// <param name="discountNum">number of discount number that was used</param>
        public void UpdateProduct(double total, Int64 trnId, double discount, string discountNum) {
            short status = 1;

            try {
                CreateDB.cmd.CommandText = "UPDATE sold_product set end_date = @en_dt, sum = @sum, discount = @discount, status = @status," +
                                                    " description = @desc, seller_id=@seller, discount_num=@discountNum WHERE id_sold = @sold_id";
                CreateDB.cmd.Parameters.Add("@desc", "Sold processing");
                CreateDB.cmd.Parameters.Add("@status", status);
                CreateDB.cmd.Parameters.Add("@en_dt", DateTime.Now);
                CreateDB.cmd.Parameters.Add("@sum", total);
                CreateDB.cmd.Parameters.Add("@discount", discount);
                CreateDB.cmd.Parameters.Add("@discountNum", discountNum);
                CreateDB.cmd.Parameters.Add("@sold_id", trnId);
                CreateDB.cmd.Parameters.Add("@seller", MainForm.Id);
                CreateDB.cmd.ExecuteNonQuery();
            } catch (Exception ex) {
                Logger.Log(ex, "SQLCommunication.cs : function => UpdateProduct()");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
        }


        /// <summary>
        /// deletes from sold_product table record with trnId
        /// </summary>
        /// <param name="trnId">transaction id</param>
        /// <returns>true on success and false on failure</returns>
        public static bool FinishProduct(Int64 trnId) {
            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    try{
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandText = "DELETE FROM transactions WHERE sold_id = @sold_id";
                        cmd.Parameters.Add("@sold_id", trnId);
                        cmd.ExecuteNonQuery();
                        cmd.CommandText = "DELETE FROM sold_product WHERE id_sold = @sold_id";
                        cmd.ExecuteNonQuery();
                    } catch (Exception ex) {
                        Logger.Log(ex, "SQLCommunication.cs : function => FinishProduct()");
                        return false;
                    } finally {
                        cmd.Parameters.Clear();
                    }
                    return true;
                }
            }
        }


        public static bool FinishProduct(Int64 trnId, string description, short status) {
            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    conn.Open();
                    try {
                        cmd.Connection = conn;
                        cmd.CommandText = "UPDATE sold_product set end_date = @en_dt, status = @status, description = @desc WHERE id_sold = @sold_id";
                        cmd.Parameters.Add("@desc", description);
                        cmd.Parameters.Add("@status", status);
                        cmd.Parameters.Add("@en_dt", DateTime.Now);
                        cmd.Parameters.Add("@sold_id", trnId);
                        cmd.ExecuteNonQuery();
                    } catch (Exception ex) {
                        Logger.Log(ex, "SQLCommunication.cs : function => FinishProduct(Int64 trnId, string description, short status)");
                        return false;
                    } finally {
                        cmd.Parameters.Clear();
                    }
                    return true;
                }
            }
        }
    }
}