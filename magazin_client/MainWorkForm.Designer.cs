﻿namespace magazin_client
{
    partial class MainWorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lstHotAccess = new System.Windows.Forms.ListView();
            this.imLstPics = new System.Windows.Forms.ImageList(this.components);
            this.dgProductCart = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductBatch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPriceDisc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnRemove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.lblProductName = new System.Windows.Forms.Label();
            this.lblProductCost = new System.Windows.Forms.Label();
            this.lblProductDescription = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.picProductCost = new System.Windows.Forms.PictureBox();
            this.picUsrLogo = new System.Windows.Forms.PictureBox();
            this.picCart = new System.Windows.Forms.PictureBox();
            this.picProduct = new System.Windows.Forms.PictureBox();
            this.bigData = new System.Windows.Forms.Panel();
            this.lblCurProductPrice = new System.Windows.Forms.Label();
            this.lblCurProductName = new System.Windows.Forms.Label();
            this.lblCurProductQuantity = new System.Windows.Forms.Label();
            this.btnRestoreData = new magazin_client.RoundButton();
            this.btnSaveData = new magazin_client.RoundButton();
            this.btnClearData = new magazin_client.RoundButton();
            this.btnConfirmBuy = new magazin_client.RoundButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUsrLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProduct)).BeginInit();
            this.bigData.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstHotAccess
            // 
            this.lstHotAccess.Dock = System.Windows.Forms.DockStyle.Top;
            this.lstHotAccess.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstHotAccess.Location = new System.Drawing.Point(0, 0);
            this.lstHotAccess.Margin = new System.Windows.Forms.Padding(0);
            this.lstHotAccess.MultiSelect = false;
            this.lstHotAccess.Name = "lstHotAccess";
            this.lstHotAccess.Scrollable = false;
            this.lstHotAccess.Size = new System.Drawing.Size(1036, 147);
            this.lstHotAccess.TabIndex = 0;
            this.lstHotAccess.UseCompatibleStateImageBehavior = false;
            
            this.lstHotAccess.Visible = false;
            
            // 
            // imLstPics
            // 
            this.imLstPics.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
            this.imLstPics.ImageSize = new System.Drawing.Size(32, 32);
            this.imLstPics.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // dgProductCart
            // 
            this.dgProductCart.AllowUserToAddRows = false;
            this.dgProductCart.AllowUserToDeleteRows = false;
            this.dgProductCart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgProductCart.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgProductCart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgProductCart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductCart.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.Product_Name,
            this.ProductDescription,
            this.ProductCount,
            this.ProductUnit,
            this.ProductBatch,
            this.ProductPrice,
            this.ProductPriceDisc,
            this.ProductTotal,
            this.btnRemove});
            this.dgProductCart.GridColor = System.Drawing.Color.LimeGreen;
            this.dgProductCart.Location = new System.Drawing.Point(_screenWidth - 770 + 60, 225 - 225);
            this.dgProductCart.Margin = new System.Windows.Forms.Padding(0);
            this.dgProductCart.MultiSelect = false;
            this.dgProductCart.Name = "dgProductCart";
            this.dgProductCart.ReadOnly = true;
            this.dgProductCart.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dgProductCart.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProductCart.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgProductCart.Size = new System.Drawing.Size(770, _screenHeight - 225 + 210);
            this.dgProductCart.TabIndex = 1;
            this.dgProductCart.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;            
            this.dgProductCart.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgProductCartCellClick);
            // 
            // id
            // 
            this.id.HeaderText = "Column1";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 5;
            // 
            // Product_Name
            // 
            this.Product_Name.HeaderText = "Наименование продукта";
            this.Product_Name.Name = "Product_Name";
            this.Product_Name.ReadOnly = true;
            this.Product_Name.Width = 260;
            // 
            // ProductDescription
            // 
            this.ProductDescription.HeaderText = "Описание продукта";
            this.ProductDescription.Name = "ProductDescription";
            this.ProductDescription.ReadOnly = true;
            this.ProductDescription.Visible = false;
            this.ProductDescription.Width = 120;
            // 
            // ProductCount
            // 
            this.ProductCount.HeaderText = "Кол-во";
            this.ProductCount.Name = "ProductCount";
            this.ProductCount.ReadOnly = true;
            this.ProductCount.Width = 65;
            // 
            // ProductUnit
            // 
            this.ProductUnit.HeaderText = "Ед. изм";
            this.ProductUnit.Name = "ProductUnit";
            this.ProductUnit.ReadOnly = true;
            this.ProductUnit.Visible = false;
            this.ProductUnit.Width = 60;
            //
            // ProductBatch
            //
            this.ProductBatch.HeaderText = "Серия";
            this.ProductBatch.Name = "batch";
            this.ProductBatch.ReadOnly = true;
            this.ProductBatch.Width = 60;
            // 
            // ProductPrice
            // 
            this.ProductPrice.HeaderText = "Цена";
            this.ProductPrice.Name = "ProductPrice";
            this.ProductPrice.ReadOnly = true;
            // 
            // ProductPriceDisc
            // 
            this.ProductPriceDisc.HeaderText = "Скидка";
            this.ProductPriceDisc.Name = "ProductPriceDisc";
            this.ProductPriceDisc.ReadOnly = true;
            this.ProductPriceDisc.Visible = true;
            this.ProductPriceDisc.Width = 60;
            // 
            // ProductTotal
            // 
            this.ProductTotal.HeaderText = "Итого";
            this.ProductTotal.Name = "ProductTotal";
            this.ProductTotal.ReadOnly = true;
            // 
            // btnRemove
            // 
            this.btnRemove.HeaderText = "Удалить";
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.ReadOnly = true;
            this.btnRemove.Text = "Remove";
            this.btnRemove.ToolTipText = "Удалить продукт из корзины";
            this.btnRemove.Width = 60;
            // 
            // lblProductName
            // 
            this.lblProductName.BackColor = System.Drawing.Color.White;
            this.lblProductName.Font = new System.Drawing.Font("Monotype Corsiva", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProductName.Location = new System.Drawing.Point(xPos, _screenHeight - 540 - 150);
            this.lblProductName.Name = "lblProductName";
            this.lblProductName.Size = new System.Drawing.Size(285, 64);
            this.lblProductName.TabIndex = 4;
            this.lblProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProductCost
            // 
            this.lblProductCost.BackColor = System.Drawing.Color.White;
            this.lblProductCost.Font = new System.Drawing.Font("Monotype Corsiva", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblProductCost.Location = new System.Drawing.Point(120, _screenHeight - 260);
            this.lblProductCost.Name = "lblProductCost";
            this.lblProductCost.Size = new System.Drawing.Size(207, 64);
            this.lblProductCost.TabIndex = 5;
            this.lblProductCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProductDescription
            // 
            this.lblProductDescription.BackColor = System.Drawing.Color.White;
            this.lblProductDescription.Font = new System.Drawing.Font("Monotype Corsiva", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductDescription.Location = new System.Drawing.Point(12, 281);
            this.lblProductDescription.Name = "lblProductDescription";
            this.lblProductDescription.Size = new System.Drawing.Size(154, 64);
            this.lblProductDescription.TabIndex = 6;
            this.lblProductDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblProductDescription.Visible = false;
            // 
            // lblTotal
            // 
            this.lblTotal.BackColor = System.Drawing.Color.White;
            this.lblTotal.Font = new System.Drawing.Font("Monotype Corsiva", 36F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTotal.Location = new System.Drawing.Point(120, _screenHeight - 190);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(218, 64);
            this.lblTotal.TabIndex = 7;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // picProductCost
            // 
            this.picProductCost.Image = global::magazin_client.Properties.Resources.money;
            this.picProductCost.Location = new System.Drawing.Point(xPos, _screenHeight - 260);
            this.picProductCost.Name = "picProductCost";
            this.picProductCost.Size = new System.Drawing.Size(64, 64);
            this.picProductCost.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProductCost.TabIndex = 13;
            this.picProductCost.TabStop = false;
            this.picProductCost.Click += new System.EventHandler(this.picProductCost_Click);
            // 
            // picUsrLogo
            // 
            this.picUsrLogo.Location = new System.Drawing.Point(xPos, 150 - 150);
            this.picUsrLogo.Name = "picUsrLogo";
            this.picUsrLogo.Size = new System.Drawing.Size(275, 64);
            this.picUsrLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picUsrLogo.TabIndex = 12;
            this.picUsrLogo.TabStop = false;
            this.picUsrLogo.Click += new System.EventHandler(this.PicUsrLogoClick);
            // 
            // picCart
            // 
            this.picCart.Image = global::magazin_client.Properties.Resources.cart;
            this.picCart.Location = new System.Drawing.Point(xPos, _screenHeight - 190);
            this.picCart.Name = "picCart";
            this.picCart.Size = new System.Drawing.Size(64, 64);
            this.picCart.TabIndex = 11;
            this.picCart.TabStop = false;
            this.picCart.Click += new System.EventHandler(this.PicCart_Click);
            // 
            // picProduct
            // 
            this.picProduct.BackColor = System.Drawing.Color.White;
            this.picProduct.Location = new System.Drawing.Point(35, _screenHeight - 460 - 150);
            this.picProduct.Name = "picProduct";
            this.picProduct.Size = new System.Drawing.Size(238, 181);
            this.picProduct.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picProduct.TabIndex = 3;
            this.picProduct.TabStop = false;
            // 
            // bigData
            // 
            this.bigData.Controls.Add(this.lblCurProductPrice);
            this.bigData.Controls.Add(this.lblCurProductName);
            this.bigData.Controls.Add(this.lblCurProductQuantity);
            this.bigData.Location = new System.Drawing.Point(_screenWidth - 710, 147);
            this.bigData.Name = "bigData";
            this.bigData.Size = new System.Drawing.Size(710, 78);
            this.bigData.TabIndex = 22;
            this.bigData.Visible = false;
            // 
            // lblCurProductPrice
            // 
            this.lblCurProductPrice.BackColor = System.Drawing.Color.RoyalBlue;
            this.lblCurProductPrice.Font = new System.Drawing.Font("Calibri", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCurProductPrice.Location = new System.Drawing.Point(526, 0);
            this.lblCurProductPrice.Name = "lblCurProductPrice";
            this.lblCurProductPrice.Size = new System.Drawing.Size(184, 78);
            this.lblCurProductPrice.TabIndex = 20;
            // 
            // lblCurProductName
            // 
            this.lblCurProductName.BackColor = System.Drawing.Color.Red;
            this.lblCurProductName.Font = new System.Drawing.Font("Calibri", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurProductName.Location = new System.Drawing.Point(0, 0);
            this.lblCurProductName.Name = "lblCurProductName";
            this.lblCurProductName.Size = new System.Drawing.Size(366, 78);
            this.lblCurProductName.TabIndex = 17;
            this.lblCurProductName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCurProductQuantity
            // 
            this.lblCurProductQuantity.BackColor = System.Drawing.Color.YellowGreen;
            this.lblCurProductQuantity.Font = new System.Drawing.Font("Calibri", 42F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCurProductQuantity.Location = new System.Drawing.Point(366, 0);
            this.lblCurProductQuantity.Name = "lblCurProductQuantity";
            this.lblCurProductQuantity.Size = new System.Drawing.Size(160, 78);
            this.lblCurProductQuantity.TabIndex = 18;
            // 
            // btnRestoreData
            // 
            this.btnRestoreData.BackColor = System.Drawing.Color.Transparent;
            this.btnRestoreData.BaseColor = System.Drawing.Color.White;
            this.btnRestoreData.ButtonColor = System.Drawing.Color.DarkRed;
            this.btnRestoreData.ButtonText = null;
            this.btnRestoreData.CornerRadius = 5;
            this.btnRestoreData.ForeColor = System.Drawing.Color.Black;
            this.btnRestoreData.HighlightColor = System.Drawing.Color.Red;
            this.btnRestoreData.Image = global::magazin_client.Properties.Resources.view_refresh;
            this.btnRestoreData.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRestoreData.ImageSize = new System.Drawing.Size(70, 70);
            this.btnRestoreData.Location = new System.Drawing.Point(220, _screenHeight - 90);
            this.btnRestoreData.Name = "btnRestoreData";
            this.btnRestoreData.Size = new System.Drawing.Size(65, 65);
            this.btnRestoreData.TabIndex = 16;
            this.btnRestoreData.Click += new System.EventHandler(this.RestoreDataClick);
            // 
            // btnSaveData
            // 
            this.btnSaveData.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveData.BaseColor = System.Drawing.Color.White;
            this.btnSaveData.ButtonColor = System.Drawing.Color.DarkRed;
            this.btnSaveData.ButtonText = null;
            this.btnSaveData.CornerRadius = 5;
            this.btnSaveData.ForeColor = System.Drawing.Color.Black;
            this.btnSaveData.HighlightColor = System.Drawing.Color.Red;
            this.btnSaveData.Image = global::magazin_client.Properties.Resources.document_save;
            this.btnSaveData.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveData.ImageSize = new System.Drawing.Size(70, 70);
            this.btnSaveData.Location = new System.Drawing.Point(150, _screenHeight - 90);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(65, 65);
            this.btnSaveData.TabIndex = 15;
            this.btnSaveData.Click += new System.EventHandler(this.SaveDataClick);
            // 
            // btnClearData
            // 
            this.btnClearData.BackColor = System.Drawing.Color.Transparent;
            this.btnClearData.BaseColor = System.Drawing.Color.White;
            this.btnClearData.ButtonColor = System.Drawing.Color.DarkRed;
            this.btnClearData.ButtonText = null;
            this.btnClearData.CornerRadius = 5;
            this.btnClearData.ForeColor = System.Drawing.Color.Black;
            this.btnClearData.HighlightColor = System.Drawing.Color.Red;
            this.btnClearData.Image = global::magazin_client.Properties.Resources.deleteAll;
            this.btnClearData.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnClearData.ImageSize = new System.Drawing.Size(70, 70);
            this.btnClearData.Location = new System.Drawing.Point(80, _screenHeight - 90);
            this.btnClearData.Name = "btnClearData";
            this.btnClearData.Size = new System.Drawing.Size(65, 65);
            this.btnClearData.TabIndex = 14;
            this.btnClearData.Click += new System.EventHandler(this.ClearDataClick);
            // 
            // btnConfirmBuy
            // 
            this.btnConfirmBuy.BackColor = System.Drawing.Color.Transparent;
            this.btnConfirmBuy.BaseColor = System.Drawing.Color.White;
            this.btnConfirmBuy.ButtonColor = System.Drawing.Color.DarkRed;
            this.btnConfirmBuy.ButtonText = null;
            this.btnConfirmBuy.CornerRadius = 5;
            this.btnConfirmBuy.ForeColor = System.Drawing.Color.Black;
            this.btnConfirmBuy.HighlightColor = System.Drawing.Color.Red;
            this.btnConfirmBuy.Image = global::magazin_client.Properties.Resources.camera_test;
            this.btnConfirmBuy.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnConfirmBuy.ImageSize = new System.Drawing.Size(70, 70);
            this.btnConfirmBuy.Location = new System.Drawing.Point(10, _screenHeight - 90);
            this.btnConfirmBuy.Name = "btnConfirmBuy";
            this.btnConfirmBuy.Size = new System.Drawing.Size(65, 65);
            this.btnConfirmBuy.TabIndex = 8;
            this.btnConfirmBuy.Click += new System.EventHandler(this.BtnConfirmBuyClick);
            // 
            // MainWorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1036, 745);
            this.Controls.Add(this.bigData);
            this.Controls.Add(this.btnRestoreData);
            this.Controls.Add(this.btnSaveData);
            this.Controls.Add(this.btnClearData);
            this.Controls.Add(this.picProductCost);
            this.Controls.Add(this.picUsrLogo);
            this.Controls.Add(this.picCart);
            this.Controls.Add(this.btnConfirmBuy);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.lblProductDescription);
            this.Controls.Add(this.lblProductCost);
            this.Controls.Add(this.lblProductName);
            this.Controls.Add(this.picProduct);
            this.Controls.Add(this.dgProductCart);
            this.Controls.Add(this.lstHotAccess);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MainWorkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Главное окно формы";
            this.Load += new System.EventHandler(this.MainWorkForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainWorkFormKeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProductCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picUsrLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picProduct)).EndInit();
            this.bigData.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private int xPos = 10;
        private System.Windows.Forms.ListView lstHotAccess;
        private System.Windows.Forms.ImageList imLstPics;
        private System.Windows.Forms.DataGridView dgProductCart;
        private System.Windows.Forms.PictureBox picProduct;
        private System.Windows.Forms.Label lblProductName;
        private System.Windows.Forms.Label lblProductCost;
        private System.Windows.Forms.Label lblProductDescription;
        private System.Windows.Forms.Label lblTotal;
        private RoundButton btnConfirmBuy;
        private System.Windows.Forms.PictureBox picCart;
        private System.Windows.Forms.PictureBox picUsrLogo;
        private System.Windows.Forms.PictureBox picProductCost;
        private RoundButton btnClearData;
        private RoundButton btnSaveData;
        private RoundButton btnRestoreData;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductDescription;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductBatch;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPriceDisc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductTotal;
        private System.Windows.Forms.DataGridViewButtonColumn btnRemove;
        private System.Windows.Forms.Panel bigData;
        private System.Windows.Forms.Label lblCurProductPrice;
        private System.Windows.Forms.Label lblCurProductName;
        private System.Windows.Forms.Label lblCurProductQuantity;
    }
}