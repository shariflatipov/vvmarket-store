﻿namespace magazin_client {
    partial class SettingsForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnPrintDailyReport = new System.Windows.Forms.Button();
            this.bntAddDiscount = new System.Windows.Forms.Button();
            this.btnRegisterReturn = new System.Windows.Forms.Button();
            this.btnOtherOperations = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnPrintDailyReport
            // 
            this.btnPrintDailyReport.Location = new System.Drawing.Point(12, 12);
            this.btnPrintDailyReport.Name = "btnPrintDailyReport";
            this.btnPrintDailyReport.Size = new System.Drawing.Size(151, 71);
            this.btnPrintDailyReport.TabIndex = 0;
            this.btnPrintDailyReport.Text = "Дневной отчет";
            this.btnPrintDailyReport.UseVisualStyleBackColor = true;
            this.btnPrintDailyReport.Click += new System.EventHandler(this.btnPrintDailyReport_Click);
            // 
            // bntAddDiscount
            // 
            this.bntAddDiscount.Location = new System.Drawing.Point(169, 12);
            this.bntAddDiscount.Name = "bntAddDiscount";
            this.bntAddDiscount.Size = new System.Drawing.Size(151, 71);
            this.bntAddDiscount.TabIndex = 1;
            this.bntAddDiscount.Text = "Добавить дисконт";
            this.bntAddDiscount.UseVisualStyleBackColor = true;
            this.bntAddDiscount.Click += new System.EventHandler(this.bntAddDiscount_Click);
            // 
            // btnRegisterReturn
            // 
            this.btnRegisterReturn.Location = new System.Drawing.Point(326, 12);
            this.btnRegisterReturn.Name = "btnRegisterReturn";
            this.btnRegisterReturn.Size = new System.Drawing.Size(151, 71);
            this.btnRegisterReturn.TabIndex = 2;
            this.btnRegisterReturn.Text = "Оформить возврат";
            this.btnRegisterReturn.UseVisualStyleBackColor = true;
            this.btnRegisterReturn.Click += new System.EventHandler(this.btnRegisterReturn_Click);
            // 
            // btnOtherOperations
            // 
            this.btnOtherOperations.Location = new System.Drawing.Point(483, 12);
            this.btnOtherOperations.Name = "btnOtherOperations";
            this.btnOtherOperations.Size = new System.Drawing.Size(151, 71);
            this.btnOtherOperations.TabIndex = 3;
            this.btnOtherOperations.Text = "Другие операции";
            this.btnOtherOperations.UseVisualStyleBackColor = true;
            this.btnOtherOperations.Click += new System.EventHandler(this.btnOtherOperations_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 483);
            this.Controls.Add(this.btnOtherOperations);
            this.Controls.Add(this.btnRegisterReturn);
            this.Controls.Add(this.bntAddDiscount);
            this.Controls.Add(this.btnPrintDailyReport);
            this.Name = "SettingsForm";
            this.Text = "SettingsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrintDailyReport;
        private System.Windows.Forms.Button bntAddDiscount;
        private System.Windows.Forms.Button btnRegisterReturn;
        private System.Windows.Forms.Button btnOtherOperations;
    }
}