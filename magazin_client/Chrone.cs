﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Data.SqlServerCe;
using magazin_client.discount;
using magazin_client.goodsreturn;


namespace magazin_client {
    class Chrone {

        public void updates() {
            try {
                var pu = new ProductUpdate();
                XElement magazin = pu.CreateUpdateTask(UpdateTask.Get);
                magazin = HttpCommunication.HttpCommun(magazin);

                if(!ProductUpdate.HandleRecievedUpdate(magazin)) { //Todo rename HandleRecievedUpdate or revert returned value because it's confusing
                    pu = new ProductUpdate();
                    magazin = pu.CreateUpdateTask(UpdateTask.Ok);
                    HttpCommunication.HttpCommun(magazin);
                }
            } catch(Exception ex) {
                Logger.Log(ex, "Chrone.cs : function => Chrone()");
            }
        }


        public void Discounts() {
            try {
                List<Discount> discounts = Discount.GetUnfinishedStates();

                foreach (var discount in discounts) {
                    XElement result = HttpCommunication.HttpCommun(Discount.Serialize(discount));
                   Discount.FinishState(discount.Id, result);
                }
            } catch(Exception ex) {
                Logger.Log(ex, "Chrone.cs : function => Chrone()");
            }
        }


        public void SendTurn() {
            try {
                using (var conn = new SqlCeConnection()) {
                    conn.ConnectionString = CreateDB.ConnString;
                    using (var cmd = new SqlCeCommand()) {
                        conn.Open();
                        cmd.Connection = conn;
                        cmd.CommandText =
                            "SELECT sp.id_sold, sp.sum, sp.start_date, sl.login, sl.pass, sp.discount, sp.discount_num FROM sold_product sp, seller sl " +
                            "WHERE sp.status > 0 AND sp.status <> 2 AND sp.seller_id = sl.id_seller";

                        using (SqlCeDataReader reader = cmd.ExecuteReader()) {
                            while (reader.Read()) {
                                CreateXml cXml = new CreateXml(reader[0].ToString(),
                                    reader[1].ToString().Replace(",", "."), reader[2].ToString(),
                                    reader[3].ToString(), reader[4].ToString(), reader[5].ToString().Replace(",", "."),
                                    reader[6].ToString().Replace(",", "."));
                                XElement magazin = cXml.FillProducts();
                                HttpCommunication.HandleResponseFromServer(magazin, long.Parse(reader[0].ToString()));
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.Log(ex, "Chrone.cs : function => SendTurn()");
            }
        }


        public void SendReTurn() {
            try {
                XElement unfinished = ReturnGood.GetUnfinished();
                if (unfinished != null) {
                    XElement result = HttpCommunication.HttpCommun(unfinished);
                    ReturnGood.Finish(result);
                }
            } catch (Exception exception) {
                Logger.Log(exception, "SendTurn");
            }
        }
    }
}