﻿using System;

namespace magazin_client {
    class NumsByBarCode :Nums {

        private readonly string _barCode;
        private readonly string _batch;

        public NumsByBarCode(ProductCart pc, string barCode, string caption) : base(pc, 0) {
            _barCode = barCode;
            lblInfo.Text = caption;
            try {
                _batch = caption.Substring(caption.IndexOf("#")+1);

            } catch (Exception ex) {
                Logger.Log(ex, "NumsByBarCode");   
            }
        }

        protected override void ConfirmClick(object sender, EventArgs e) {

            double quantity = 1;
            double.TryParse(lblCount.Text, out quantity);

            if (!(quantity > 0)) return;

            var product = Product.SetProductById(_barCode, _batch, quantity);
            pc.AddItem(product);

            pc.HotRowsAdded();
            Dispose();
        }
    }
}
