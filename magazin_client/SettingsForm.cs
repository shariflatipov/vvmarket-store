﻿using System;
using System.Windows.Forms;
using magazin_client.discount;
using magazin_client.ff;
using magazin_client.goodsreturn;
using magazin_client.peripheral;
using magazin_client.reports;

namespace magazin_client {
    public partial class SettingsForm : Form {

        public SettingsForm() {
            InitializeComponent();
        }

        private void btnPrintDailyReport_Click(object sender, EventArgs e) {
            var drView = new DailyReportView();
            drView.ShowDialog();
        }

        private void btnRegisterReturn_Click(object sender, EventArgs e) {
                var retMain = new ReturnMain();
                retMain.ShowDialog();
        }

        private void bntAddDiscount_Click(object sender, EventArgs e) {
            var df = new DiscountListForm();
            df.ShowDialog();
        }

        private void btnOtherOperations_Click(object sender, EventArgs e) {
            var browserForm = new MainBrowser();
            browserForm.ShowDialog();
        }
    }
}
