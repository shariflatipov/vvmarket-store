﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace magazin_client {
    public partial class MainForm : Form {
        public readonly MainWorkForm MWork = new MainWorkForm();
        public readonly AuthForm Auth = new AuthForm();
        private Thread _thrChrone;

        public static string Id;
        public static string Login;
        public static string Pass;
        public static string Stock;
        public static string Ip;
        public static string PrinterPort;
        
        // Printer receipt fields
        public static string PrinterPackage;
        public static string StoreName;
        public static string StoreAddress;
        public static string StoreINN;
        public static string StoreSlogan;
        public static string StorePhones;

        public MainForm() {
            InitializeComponent();
            StoreName = System.Configuration.ConfigurationManager.AppSettings["chkStoreName"];
            StoreAddress = System.Configuration.ConfigurationManager.AppSettings["chkStoreAddress"];
            StoreINN = System.Configuration.ConfigurationManager.AppSettings["chkStoreINN"];
            StoreSlogan = System.Configuration.ConfigurationManager.AppSettings["chkStoreSlogan"];
            StorePhones = System.Configuration.ConfigurationManager.AppSettings["chkStorePhones"];
            PrinterPort = System.Configuration.ConfigurationManager.AppSettings["printerPort"];
            PrinterPackage = System.Configuration.ConfigurationManager.AppSettings["printerPackage"];
        }

        private void Main_Form_Load(object sender, EventArgs e) {
            
            if (!Directory.Exists("logs")) {
                Directory.CreateDirectory("logs");
            }

            var dbc = new DBConnection();
            
            Ip = dbc.GetDBValues("ip");
            Stock = dbc.GetDBValues("point");

            if (dbc.active == false) {
                dbc.MdiParent = this;
                dbc.Dock = DockStyle.Fill;
                dbc.Show();
            } else {
                _thrChrone = new Thread(ChroneThread) {Name = "thrChrone", IsBackground = true};
                _thrChrone.Start();

                dbc.Dispose();
                Auth.MdiParent = this;
                Auth.Show();
            }
        }

        private void ChroneThread() {
            while (true) {
                try {
                    Chrone cr = new Chrone();
                    cr.Discounts();
                    cr.SendTurn();
                    cr.updates();
                    cr.SendReTurn();
                } catch (Exception ex) {
                    Logger.Log(ex, "MainWorkForm.cs : function => ChroneThread()", Level.Warining);
                }
                Thread.Sleep(30000);
            }
        }
    }
}