﻿using System.IO.Ports;
using System.Text;

namespace magazin_client.peripheral {
    public class VfdDisplay: IVfdDisplays {
        readonly SerialPort _sp = new SerialPort();
        readonly byte[] _cmdCodeTable = { 27, 116, 7 };
        readonly byte[] _cmdCharacteSet = { 27, 82, 12 };
        readonly byte[] _cmdClearDisplay = { 12 };
        readonly byte[] _cmdBlink = new byte[3];

        public VfdDisplay() {
            _sp.BaudRate = 9600;
            _sp.PortName = System.Configuration.ConfigurationManager.AppSettings["vfdport"];
            _sp.Parity = Parity.None;
            _sp.DataBits = 8;
            _sp.StopBits = StopBits.One;
            _sp.Open();

            _sp.Write(_cmdCodeTable, 0, _cmdCodeTable.Length);
            _sp.Write(_cmdCharacteSet, 0, _cmdCharacteSet.Length);
        }

        public void Write(string text) {
            Encoding enc = Encoding.GetEncoding("cp866");
            byte[] data = enc.GetBytes(text);
            _sp.Write(data, 0, data.Length);
        }

        public void ClearDisplay() {
            _sp.Write(_cmdClearDisplay, 0, _cmdClearDisplay.Length);
        }

        public void Blink(byte frequency) {
            if(frequency > 255) {
                frequency = 0;
            }

            _cmdBlink[0] = 31;
            _cmdBlink[1] = 69;
            _cmdBlink[2] = frequency;
            _sp.Write(_cmdBlink, 0, _cmdBlink.Length);

        }

        bool IVfdDisplays.IsNumeric {
            get {
                return false;
            }
        }


        public void Close() {
            _sp.Close();
        }


        public void WriteFull(string line1, string line2) {
        }
    }
}
