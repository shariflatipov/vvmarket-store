﻿namespace magazin_client.peripheral
{
    public interface IVfdDisplays
    {
        bool IsNumeric { get; }
        void Write(string text);
        void WriteFull(string line1, string line2);
        void ClearDisplay();
        void Blink(byte frequency);
        void Close();
    }
}
