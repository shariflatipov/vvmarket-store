﻿using System;
using System.Runtime.InteropServices;

namespace magazin_client.peripheral {
    public class PosiflexPD2600 : IVfdDisplays{
        [DllImport("USBPD.DLL")]
        private static extern long OpenUSBpd();

        [DllImport("USBPD.DLL")]
        private static extern long CloseUSBpd();

        [DllImport("USBPD.DLL")]
        private static extern long WritePD(string data, long length);

        [DllImport("USBPD.DLL")]
        private static extern long PdState();

        public void WriteFull(string line1, string line2) {
            try {
                if (OpenUSBpd() == 0) {
                    try {
                        string a = line1.PadRight(20);
                        string b = line2.PadLeft(20);
                        a += b;
                        WritePD(a, a.Length);
                    } catch (Exception exception) {
                        Logger.Log(exception);
                    }
                    CloseUSBpd();
                }

            } catch (Exception ex) {
                Logger.Log(ex);
            }
        }

        public bool IsNumeric {
            get { return false; }
        }

        public void Write(string text) {
            throw new NotImplementedException();
        }

        public void ClearDisplay() {
            throw new NotImplementedException();
        }

        public void Blink(byte frequency) {
            throw new NotImplementedException();
        }

        public void Close() {
            throw new NotImplementedException();
        }
    }
}
