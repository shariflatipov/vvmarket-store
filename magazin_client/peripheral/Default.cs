﻿using System.Drawing;
using System.Drawing.Printing;

namespace magazin_client.peripheral {
    public class Default : IPrinter {

        private bool _bold = false;
        private bool _printBarCode = false;
        private string _strToPrint;
        private Font _printFont;
        private readonly Font _fontBarcode = new Font("Code EAN13", 20);

        public void Print(string str) {
            _strToPrint += str;    
        }

        public void Cut() {
            _printFont = new Font("Courier New", 8, FontStyle.Bold);

            var pd = new PrintDocument {PrintController = new StandardPrintController()};

            var asx = new PaperSize("npf", 850, 3000);
            pd.DefaultPageSettings.PaperSize = asx;
            pd.PrintPage += (PdPrintPage);
            pd.Print();            
        }

        private void PdPrintPage(object sender, PrintPageEventArgs ev) {
            Font font = (_printBarCode) ? _fontBarcode : _printFont;
            ev.Graphics.DrawString(_strToPrint, font, Brushes.Black, new PointF());
        }

        public void BoldOn() {
            _bold = true;
        }

        public void BoldOff() {
            _bold = false;
        }

        public void PrintEan13(string code, bool start) {
            _printBarCode = start;
        }
    }
}
