﻿using System;

namespace magazin_client.peripheral {
    public interface IPrinter {
        void Print(String str);
        void Cut();
        void BoldOn();
        void BoldOff();
        void PrintEan13(string code, bool start);
    }
}
