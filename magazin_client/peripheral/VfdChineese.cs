﻿using System.IO.Ports;

namespace magazin_client.peripheral
{
    class VfdChineese: IVfdDisplays
    {
        private readonly SerialPort _sp = new SerialPort();
        private readonly byte[] _start = { 0x1B, 0x73, 0x30, 0x1B, 0x51, 0x41 };
        private readonly byte[] _end = { 0x0D };
        

        public VfdChineese()
        {
            _sp.BaudRate = 2400;
            _sp.PortName = System.Configuration.ConfigurationManager.AppSettings["vfdport"];
            _sp.Parity = Parity.None;
            _sp.DataBits = 8;
            _sp.StopBits = StopBits.One;
            _sp.Handshake = Handshake.XOnXOff;
            _sp.Open();
       }

        public void Write(string text)
        {
            _sp.Write(_start, 0, _start.Length);
            _sp.Write(text);
            _sp.Write(_end, 0, _end.Length);
        }

        bool IVfdDisplays.IsNumeric
        {
            get
            {
                return true;
            }
        }


        public void ClearDisplay() {
            _sp.Write(_start, 0, _start.Length);
            _sp.Write(_end, 0, _end.Length);
        }

        public void Blink(byte frequency) {}

        public void Close() {
            _sp.Close();
        }


        public void WriteFull(string line1, string line2) {
            
        }
    }
}
