﻿using System;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Net;

namespace magazin_client {

    public enum UpdateTask {
        Get = 0,
        Ok = 1
    }

    class ProductUpdate {
        
        private readonly string _dateToSend;
        private int _act;
        private XElement _magazin;
        

        public ProductUpdate() {
            DateTime b = DateTime.Now;
            _dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");
            
            Init();
        }
        
        private void Init() {
            _act = 1;
            string password = MyCrypt.GetMd5Hash(MainForm.Pass);
            string hashSum = MyCrypt.GetSha1(_dateToSend + "#" + MainForm.Login + "#" + password);

            XElement seller = new XElement("seller");
            seller.Add(new XAttribute("login", MainForm.Login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", _act));
            seller.Add(new XAttribute("date", _dateToSend));

            _magazin = new XElement("magazin");
            _magazin.Add(new XElement(seller));
        }

        public XElement CreateUpdateTask(UpdateTask type) {
            string operation = (type == UpdateTask.Get) ? "get" : "ok";

            XElement update = new XElement("update");
            update.SetValue(operation);
            _magazin.Add(new XElement(update));

            return _magazin;
        }


        public static bool HandleRecievedUpdate(XElement magazin) {

            WebClient webClient = new WebClient();
            bool isErrorExists = false;
            double discount = 0;

            try {
                var query = from mag in magazin.Elements("product")
                            select mag;

                foreach (var mg in query) {
                    try {
                        try {
                            if(!File.Exists(mg.Element("image_source").Value)) {
                                webClient.DownloadFile(MainForm.Ip + mg.Element("image_source").Value, mg.Element("image_source").Value);
                            }
                        } catch(Exception) {
                            mg.Element("image_source").Value = "img/no_image.jpg";
                        }
                        if(mg.Element("name").Value.Length > 69)
                            mg.Element("name").Value = mg.Element("name").Value.Substring(0, 69);
                        if(mg.Attribute("id").Value.Length > 13)
                            mg.Attribute("id").Value = mg.Attribute("id").Value.Substring(0, 13);
                        if(mg.Element("depend").Value.Length > 13)
                            mg.Element("depend").Value = mg.Element("depend").Value.Substring(0, 13);
                        if (mg.Element("discount_percent").Value != null) {
                            discount = double.Parse(mg.Element("discount_percent").Value.Replace(".", ","));
                        }
                        if(mg.Element("flag").Value.Equals("2")) {
                            Product.DeleteProduct(mg.Attribute("id").Value, mg.Element("batch").Value);
                        } else {
                            if(Product.InsertOrUpdateProduct(
                                    mg.Element("is_list").Value,
                                    mg.Element("depend").Value,
                                    mg.Attribute("id").Value,
                                    mg.Element("name").Value,
                                    mg.Element("image_source").Value,
                                    mg.Element("unit").Value,
                                    mg.Element("price").Value,
                                    mg.Element("price_sell").Value,
                                    mg.Element("pack_cnt").Value,
                                    mg.Element("batch").Value, 
                                    discount) == 0) {
                                isErrorExists = true;
                            }
                        }
                    } catch(Exception genExce) {
                        Logger.Log(genExce, "handleRecievedUpdate() inside the foreach statement: " + mg);
                    }
                }
            } catch (Exception ex) {
                Logger.Log(ex, "Chrone.cs : function => handleRecievedUpdate()");
            }
            return isErrorExists;
        }
    }
}
