﻿using System;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;
using magazin_client.discount;
using magazin_client.peripheral;

namespace magazin_client {
    public partial class ChangeBack : Form {

        private readonly double _total;
        private readonly SoldProduct _sqlComm = new SoldProduct();
        private readonly DataGridView _dg;
        private Discount _discount = null;

        public ChangeBack(double total, DataGridView dgv, IVfdDisplays vdfDisplay) {
            InitializeComponent();
            _total = total;
            lblToPay.Text = total.ToString();
            lblFinal.Text = total.ToString();
            _dg = dgv;
            vdfDisplay.WriteFull("CYMMA", lblToPay.Text);
        }

        private void CancelClick(object sender, EventArgs e) {
            Dispose();
        }

        private void ConfirmClick(object sender, EventArgs e) {
            try {
                try {
                    double discount;
                    double.TryParse(lblDiscountTotal.Text, out discount);
                    if (_discount == null || _discount.DiscountCode == null) {
                        _discount = new Discount {DiscountCode = ""};
                    }
                    _sqlComm.UpdateProduct(_total, ProductCartToDb.Id, discount, _discount.DiscountCode);
                    Discount.ChangeSum((_total - discount), _discount);
                } catch (Exception ex) {
                    _sqlComm.UpdateProduct(_total, ProductCartToDb.Id, 0, "");
                    Logger.Log(ex, "ConfirmClick => ChangeBack.cs");
                }

                var prChk = new PrintChk(lblAcceptedMoney.Text, lblChange.Text,
                                         lblFinal.Text, lblDiscountTotal.Text, _dg, lblToPay.Text);

                _sqlComm.InsertProduct(MainForm.Id);
                ProductCartToDb.Id = _sqlComm.GetMaxId();
                
                MainForm mFrm = MdiParent as MainForm;
                mFrm.MWork.ClearDgProduct();
                
            } catch (Exception ex) {
                Logger.Log(ex, "ChangeBack.cs : function => ConfirmClick() => General");
            } finally {
                Dispose();                
            }
        }

        #region Buttons click event handling region
        /// <summary>
        /// Number button click event.
        /// </summary>
        private void BtnClick(object sender, EventArgs e) {
            Button num = (Button)sender;
            if(_flagFixed || _flagPercent) {
                lblDiscount.Text += num.Name.Substring(4);
            } else {
                lblAcceptedMoney.Text += num.Name.Substring(4);
            }
        }

        /// <summary>
        /// Button Clear click event.
        /// </summary>
        private void ClearClick(object sender, EventArgs e) {
            if(_flagFixed || _flagPercent) {
                lblDiscount.Text = "";
            } else {
                lblAcceptedMoney.Text = "";
            }
        }

        /// <summary>
        /// Button back click event.
        /// </summary>
        private void BackClick(object sender, EventArgs e) {
            if(_flagPercent || _flagFixed) {
                if(lblDiscount.Text.Length >= 1) {
                    lblDiscount.Text = lblDiscount.Text.Substring(0, lblDiscount.Text.Length - 1);
                }
            } else if(lblAcceptedMoney.Text.Length >= 1) {
                lblAcceptedMoney.Text = lblAcceptedMoney.Text.Substring(0, lblAcceptedMoney.Text.Length - 1);
            }
        }

        private void BtnDotClick(object sender, EventArgs e) {
            if (_flagFixed || _flagPercent){
                if(!lblDiscount.Text.Contains(',')) {
                    lblDiscount.Text += ",";
                }
            } else if (!lblAcceptedMoney.Text.Contains(',')) {
                lblAcceptedMoney.Text += ",";
            }
        }

        /// <summary>
        /// Lable count text changed.
        /// </summary>
        private void LblCountTextChanged(object sender, EventArgs e) {
            Btn_Confirm_Validation();
        }

        /// <summary>
        /// Enabling or disabling btnConfirm
        /// </summary>
        private void Btn_Confirm_Validation() {
            try {
                double payed = 0;
                double discount = 0;

                try { discount = Convert.ToDouble(lblDiscountTotal.Text.Replace(".", ",")); } catch(Exception ex) { }

                if (lblAcceptedMoney.Text != "") {
                    payed = Convert.ToDouble(lblAcceptedMoney.Text.Replace(".", ","));
                    lblChange.Text = Math.Round(((payed + discount) - _total), 2).ToString();
                } else {
                    lblChange.Text = "";
                }

                if(Convert.ToDouble(lblAcceptedMoney.Text.Replace(".", ",")) + discount >= _total) {
                    confirm.Visible = true;
                } else {
                    confirm.Visible = false;
                }
            } catch (Exception ex) {
                Logger.Log(ex, "ChangeBack.cs : function => Btn_Confirm_Validation", Level.Warining);
                confirm.Visible = false;
            }
        }
        #endregion

        #region Discount

        private bool _flagPercent = false;
        private bool _flagFixed = false;
        private bool _flagDiscount = false;

        private void btnDiscountPercent_Click(object sender, EventArgs e) {
            if(_flagPercent) {
                _flagPercent = false;
                btnDiscountPercent.BackColor = Color.White;
            } else {
                if(_flagFixed) {
                    _flagFixed = false;
                    btnDiscountFixed.BackColor = Color.White;
                }

                _flagPercent = true;
                btnDiscountPercent.BackColor = Color.Yellow;
            }
        }

        private void btnDiscountFixed_Click(object sender, EventArgs e) {
            if(_flagFixed) {
                _flagFixed = false;
                btnDiscountFixed.BackColor = Color.White;
            } else {
                if(_flagPercent) {
                    _flagPercent = false;
                    btnDiscountPercent.BackColor = Color.White;
                }

                _flagFixed = true;
                btnDiscountFixed.BackColor = Color.Yellow;
            }
        }

        private void lblDiscount_TextChanged(object sender, EventArgs e) {
            double discount = 0;
            try {
                discount = Convert.ToDouble(lblDiscount.Text.Replace(".", ","));
            } catch(Exception ex) {
            }

            try {
                if(_flagFixed) {
                    if(Convert.ToDouble(lblToPay.Text.Replace(".", ",")) > discount) {
                        lblDiscountTotal.Text = discount.ToString();
                    } else {
                        lblDiscount.Text = "";
                        lblDiscountTotal.Text = "";
                        MessageBox.Show(this, "Сумма скидки не может превышать сумму покупки", "Ошибка", 
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                } else if(_flagPercent || _flagDiscount) {
                    if(discount <=100 ) {
                        lblDiscountTotal.Text = (Convert.ToDouble(lblToPay.Text.Replace(".", ",")) * discount / 100).ToString();
                    } else {
                        lblDiscount.Text = "";
                        lblDiscountTotal.Text = "";
                        MessageBox.Show(this, "Скидка не может превышать 100%", "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            } catch(Exception ex) {
                Logger.Log(ex, "ChangeBack.cs=>lblDiscount_TextChanged");
            }
        }

        private void lblDiscountTotal_TextChanged(object sender, EventArgs e) {
            double toPay = 0;
            double accepted = 0;
            double discount = 0;

            try {
                toPay = Convert.ToDouble(lblToPay.Text.Replace(".", ","));
            } catch(Exception ex) {
            }
            try {
                accepted = Convert.ToDouble(lblAcceptedMoney.Text.Replace(".", ","));
            } catch(Exception ex) {
            }
            try {
                discount = Convert.ToDouble(lblDiscountTotal.Text.Replace(".", ","));
            } catch(Exception ex) {
            }

            lblChange.Text = ((accepted + discount) - toPay).ToString();
            lblFinal.Text = (toPay - discount).ToString();
            Btn_Confirm_Validation();

        }

        #endregion

        private void ChangeBack_KeyDown(object sender, KeyEventArgs e) {
            string key = utils.Utils.KeyToChar(e.KeyCode.ToString());

            if (key.Equals("dec")) { // ,
                BtnDotClick(null, null);
            } else if (key.Equals("esc")) {
                CancelClick(null, null);
            } else if (key.Equals("clr")) {
                ClearClick(null, null);
            } else {
                if (_flagFixed || _flagPercent) {
                    lblDiscount.Text += key;
                } else {
                    lblAcceptedMoney.Text += key;
                }
            }
        }

        private void btnDiscount_Click(object sender, EventArgs e) {
            var cd = new CalculateDiscount();
            cd.ShowDialog();
            _flagDiscount = true;
            _discount = cd.discount;
            lblDiscount.Text = ((int)_discount.Percents).ToString();
        }
    }
}