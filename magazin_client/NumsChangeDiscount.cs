﻿using System;

namespace magazin_client {

    public class NumsChangeDiscount : Nums {

        public NumsChangeDiscount(ProductCart pc, int index) : base(pc, index) {
            rowIndex = index;
            this.pc = pc;

            lblInfo.Text = "Введите процент скидки";
        }

        protected override void ConfirmClick(object sender, EventArgs e) {
            double count = 0;
            try {
                count = Convert.ToDouble(lblCount.Text);
            } catch (Exception ex) {
                Logger.Log(ex, "NumsChangeDiscount.cs : function => ConfirmClick()");
            }

            if (count > 0) {
                pc.ChangeDiscount(rowIndex, count);
                pc.HotRowsAdded();
            }
            Dispose();
        }
    }
}
