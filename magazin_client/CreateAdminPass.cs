﻿using System;
using System.Windows.Forms;

namespace magazin_client {
    public partial class CreateAdminPass : Form {
        private int tryCnt = 0;

        public CreateAdminPass() {
            InitializeComponent();
        }

        private void ButtonCreateAdminPassClick(object sender, EventArgs e) {
            int error = 0;
           
                if (Pass.TextLength > 6) {
                    if (Pass.Text == PassConfirm.Text) {



                        CreateDB cdb = new CreateDB();
                        MainForm mForm = new MainForm();
                        mForm.Show();
                        this.Hide();
                    }
                    else {
                        MessageBox.Show("Поля не совпадают");
                        error = 1;
                    }
                }
                else {
                    MessageBox.Show("Неправильный формат поля \"Пароль\"");
                    error = 1;
                }
           

            if (error == 1) {
                tryCnt++;

                Pass.Text = "";
                PassConfirm.Text = "";

                if (tryCnt > 2) {
                    MessageBox.Show("Нажмите кнопку \"Инфо\"");
                }
            }

        }

        private void ButtonHelpClick(object sender, EventArgs e) {
            Help hl = new Help();
            hl.ShowDialog();
        }
    }
}