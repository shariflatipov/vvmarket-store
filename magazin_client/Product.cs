﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Drawing;

namespace magazin_client {
    public class Product {

        private string _id;
        private string _name;
        private string _description;
        private string _unit;
        private double _price;
        private Bitmap _image;
        private double _discount;
        private double _quantity;
        private short _packCount;
        private string _batch;


        public string GetId() {
            return _id;
        }

        private void SetId(string id) {
            _id = id;
        }

        public string GetName() {
            return _name;
        }

        public void SetName(string name) {
            _name = name;
        }

        public string GetDescription() {
            return _description;
        }

        public void SetDescription(string description) {
            _description = description;
        }

        public string GetUnit() {
            return _unit;
        }

        public void SetUnit(string unit) {
            _unit = unit;
        }

        public double GetPrice() {
            return Math.Round(_price, 2);
        }

        public void SetPrice(double price) {
            _price = price;
        }

        public double GetQuantity() {
            return Math.Round(_quantity, 3);
        }

        public void SetQuantity(double quantity) {
            _quantity = quantity;
        }

        public void AddQuantity(double quantity) {
            _quantity += quantity;
        }

        public Bitmap GetImage() {
            return _image;
        }

        private void SetImage(string path) {
            try {
                _image = new Bitmap(path);
            } catch {
                try {
                    _image = new Bitmap("img/no_image.jpg");
                }
                catch (Exception ex) {
                    Logger.Log(ex, "Product.cs => SetImage : " + path);
                }
            }
                    
        }

        public void SetImage(Bitmap image) {
            _image = image;
        }

        public short GetPackCount() {
            return _packCount;
        }

        public void SetPackCount(short packCount) {
            _packCount = packCount;
        }

        public double GetDiscount() {
            return _discount;
        }

        public void SetDiscount(double discount) {
            _discount = discount;
        }

        public void SetBatch(string batch) {
            _batch = batch;
        }

        public string GetBatch() {
            return _batch;
        }

        public double GetTotal() {
            return Math.Round((_price * _quantity)-((_price * _quantity * _discount) / 100), 2);
        }


        public static Product SetProductById(string barcode, string batch, double quantity) {
            var product = new Product();

            try {
                CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_description, unitID, product_price, product_image, pack_count, batch, discount_percent " +
                                   "FROM product where product_id=@prod_id and @batch=batch, @discount_percent=discount_percent";
            
                CreateDB.cmd.Parameters.Add("@prod_id", barcode);
                CreateDB.cmd.Parameters.Add("@batch", batch);

                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();

                if (reader.Read()) {
                    product.SetId(reader[0].ToString());
                    
                    product.SetName(reader[1].ToString());
                    product.SetDescription(reader[2].ToString());
                    product.SetUnit(reader[3].ToString());
                    product.SetPrice(Convert.ToDouble(reader[4]));
                    product.SetQuantity(quantity);
                    product.SetDiscount(Convert.ToInt32(reader[8]));

                    short packCount;

                    product.SetImage(reader[5].ToString());
                    short.TryParse(reader[6].ToString(), out packCount);
                    product.SetPackCount(packCount);
                    product.SetBatch(batch);
                }
            } catch (Exception ex) {
                Logger.Log(ex, String.Format("Product.cs : function => SetProductById( ID : = {0})", barcode));
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return product;
        }

        public void SetProductByFields(string id, string name, string description, string unit, double price,
                                       string image, double quantity, double discount, short packCount, string batch) {
            _id = id;
            _name = name;
            _description = description;
            _unit = unit;
            _price = price;
            SetImage(image);
            _discount = discount;
            _quantity = quantity;
            _packCount = packCount;
            _batch = batch;
        }

        public static int InsertProduct(string isList, string dependent, string prodId, string prodName, string prodImage,
                                      string prodUnit, string prodListPrice, string prodPrice, string packCount) {
            int result = 0;

            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    cmd.Connection = conn;

                    try {
                        double listPrice = Convert.ToDouble(prodListPrice.Replace(".", ","));
                        double price = Convert.ToDouble(prodPrice.Replace(".", ","));

                        cmd.CommandText = "INSERT INTO product (category_id, dependent, is_list, product_id, product_name, product_image, unitID, product_list_price, product_price, pack_count) " +
                                                    "VALUES (1, @dependent, @isList, @product_id, @product_name, @product_image, @unitID, @product_list_price, @product_price, @pack_count);";
                        
                        cmd.Parameters.Add("@isList", isList);
                        cmd.Parameters.Add("@dependent", dependent);
                        cmd.Parameters.Add("@product_id", prodId);
                        cmd.Parameters.Add("@product_name", prodName);
                        cmd.Parameters.Add("@product_image", prodImage);
                        cmd.Parameters.Add("@unitID", prodUnit);
                        cmd.Parameters.Add("@product_list_price", listPrice);
                        cmd.Parameters.Add("@product_price", price);
                        cmd.Parameters.Add("@pack_count", packCount);
                        result = cmd.ExecuteNonQuery();
                    } catch (SqlCeException sqle) {
                        Logger.Log(sqle, isList + dependent + prodId + prodName + prodImage + prodUnit + prodPrice);
                    } finally {
                        cmd.Parameters.Clear();
                    }
                }
            }
            return result;
        }


        public static int UpdateProduct(string isList, string dependent, string prodId, string prodName, string prodImage,
                                      string prodUnit, string prodListPrice, string prodPrice, string packCount) {
            int result = 0;

            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    cmd.Connection = conn;

                    try {
                        double listPrice = Convert.ToDouble(prodListPrice.Replace(".", ","));
                        double price = Convert.ToDouble(prodPrice.Replace(".", ","));

                            cmd.CommandText = "UPDATE product SET dependent = @dependent, is_list = @isList, product_name = @product_name, product_image = @product_image, pack_count = @pack_count, " +
                                        "unitID = @unitID, product_list_price = @product_list_price, product_price = @product_price WHERE product_id = @product_id";
                        
                        cmd.Parameters.Add("@isList", isList);
                        cmd.Parameters.Add("@dependent", dependent);
                        cmd.Parameters.Add("@product_id", prodId);
                        cmd.Parameters.Add("@product_name", prodName);
                        cmd.Parameters.Add("@product_image", prodImage);
                        cmd.Parameters.Add("@unitID", prodUnit);
                        cmd.Parameters.Add("@product_list_price", listPrice);
                        cmd.Parameters.Add("@product_price", price);
                        cmd.Parameters.Add("@pack_count", packCount);
                        result = cmd.ExecuteNonQuery();
                    } catch (SqlCeException sqle) {
                        Logger.Log(sqle, isList + dependent + prodId + prodName + prodImage + prodUnit + prodPrice);
                    } finally {
                        cmd.Parameters.Clear();
                    }
                }
            }
            return result;
        }

        public static int InsertOrUpdateProduct(string isList, string dependent, string prodId, string prodName, string prodImage,
                                      string prodUnit, string prodListPrice, string prodPrice, string packCount, string batch, double discount) {
            int result = 0;

            using (var conn = new SqlCeConnection()) {
                conn.ConnectionString = CreateDB.ConnString;
                using (var cmd = new SqlCeCommand()) {
                    cmd.Connection = conn;

                    try {
                        double listPrice = Convert.ToDouble(prodListPrice.Replace(".", ","));
                        double price = Convert.ToDouble(prodPrice.Replace(".", ","));

                            cmd.CommandText = "INSERT INTO product " +
                                                  "(category_id, dependent, is_list, product_id, product_name, product_image, " +
                                                  "unitID, product_list_price, product_price, pack_count, batch, discount_percent) " +
                                              "VALUES " +
                                                  "(1, @dependent, @isList, @product_id, @product_name, @product_image, @unitID, " +
                                                  "@product_list_price, @product_price, @pack_count, @batch, @discount_percent);";
                            
                        cmd.Parameters.Add("@isList", isList);
                        cmd.Parameters.Add("@dependent", dependent);
                        cmd.Parameters.Add("@product_id", prodId);
                        cmd.Parameters.Add("@product_name", prodName);
                        cmd.Parameters.Add("@product_image", prodImage);
                        cmd.Parameters.Add("@unitID", prodUnit);
                        cmd.Parameters.Add("@product_list_price", listPrice);
                        cmd.Parameters.Add("@product_price", price);
                        cmd.Parameters.Add("@pack_count", packCount);
                        cmd.Parameters.Add("@batch", batch);
                        cmd.Parameters.Add("@discount_percent", discount);
                        try {
                            conn.Open();
                            result = cmd.ExecuteNonQuery();
                        } catch (Exception ex){
                            Logger.Log(ex, isList + dependent + prodId + prodName + prodImage + prodUnit + prodPrice);
                            cmd.CommandText = "UPDATE product SET " +
                                                    "dependent = @dependent, is_list = @isList, product_name = @product_name, " +
                                                    "product_image = @product_image, unitID = @unitID, product_list_price = @product_list_price, " +
                                                    "product_price = @product_price, pack_count = @pack_count, discount_percent=@discount_percent " +
                                              "WHERE product_id = @product_id and batch=@batch";
                            result = cmd.ExecuteNonQuery();
                        }
                    } catch (SqlCeException sqle) {
                        Logger.Log(sqle, isList + dependent + prodId + prodName + prodImage + prodUnit + prodPrice);
                    } finally {
                        cmd.Parameters.Clear();
                    }
                }
            }
            return result;
        }

        public static void DeleteProduct(string productId, string batch) {
            try {
                CreateDB.cmd.CommandText = "delete from product where product_id = @product_id and batch = @batch";
                CreateDB.cmd.Parameters.Add("@product_id", productId);
                CreateDB.cmd.Parameters.Add("@batch", batch);
                CreateDB.cmd.ExecuteNonQuery();
            } catch (SqlCeException ex) {
                Logger.Log(ex, String.Format("DeleteProduct : id={0}, batch={1}", productId, batch));
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }

        }

        public static int GetCount(string productId) {
            Int32 result = 0;
            try {
                CreateDB.cmd.CommandText = "SELECT count(*) FROM product where product_id=@prod_id";
                CreateDB.cmd.Parameters.Add("@prod_id", productId);
                result = (Int32)CreateDB.cmd.ExecuteScalar();
            } catch(Exception ex) {
                Logger.Log(ex, String.Format("Product.cs : function => GetBatchCount( ID : = {0})", productId));
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return result;
        }

        public static List<Product> GetProducts(string productId) {
            var result = new List<Product>();

            try {
                CreateDB.cmd.CommandText = "SELECT product_id, product_name, product_description, unitID, product_price, product_image, pack_count, batch, discount_percent " +
                                   "FROM product where product_id=@prod_id";

                CreateDB.cmd.Parameters.Add("@prod_id", productId);

                SqlCeDataReader reader = CreateDB.cmd.ExecuteReader();
                while (reader.Read()) {

                    var p = new Product();

                    p.SetId(reader[0].ToString());
                    p.SetName(reader[1].ToString());
                    p.SetDescription(reader[2].ToString());
                    p.SetUnit(reader[3].ToString());
                    p.SetPrice(Math.Round(Convert.ToDouble(reader[4]), 2));
                    p.SetQuantity(1);
                    p.SetDiscount(Convert.ToDouble(reader[8]));
                    p.SetImage(reader[5].ToString());
                    short packCount;
                    short.TryParse(reader[6].ToString(), out packCount);
                    p.SetPackCount(packCount);
                    string batch = reader[7].ToString();
                    p.SetBatch(batch);

                    result.Add(p);
                }
            } catch(Exception ex) {
                Logger.Log(ex, String.Format("Product.cs : function => SetProductById( ID : = {0})", productId));
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }

            return result;
        }
    }
}