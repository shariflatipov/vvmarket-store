﻿using System;
using System.Collections.Generic;
using System.Data;

namespace magazin_client.reports {
    static class DailyReport {

        public static List<DailyReportObject> GetDailyReport() {

            var resultList = new List<DailyReportObject>();

            try {

                CreateDB.cmd.CommandText =
                    "SELECT s.id_seller, s.login, sum(sp.[sum]), sum(sp.discount) " +
                    "FROM sold_product sp, seller s WHERE sp.seller_id = s.id_seller and start_date like @sdt group by s.id_seller, s.login";
                CreateDB.cmd.Parameters.Add("@sdt", DateTime.Today.ToShortDateString() + '%');
                CreateDB.cmd.Parameters.Add("@seller", SqlDbType.Int); 
                
                var reader = CreateDB.cmd.ExecuteReader();

                while (reader.Read()) {
                    var obj = new DailyReportObject {
                        id = int.Parse(reader[0].ToString()),
                        Login = reader[1].ToString(),
                        Sum = double.Parse(reader[2].ToString()),
                        DiscountSum = double.Parse(reader[3].ToString())
                    };

                    CreateDB.cmd.CommandText = "SELECT        SUM(price * count * discont / 100) AS Expr1 "+
                                                    "FROM            transactions AS t "+
                                                    "WHERE        (sold_id IN " +
                                            " (SELECT        sp.id_sold " +
                                               "FROM            transactions AS tr INNER JOIN " +
                                                                         "sold_product AS sp ON tr.sold_id = sp.id_sold "+
                                               "WHERE        (sp.start_date LIKE @sdt) AND (sp.seller_id = @seller)))";
                    CreateDB.cmd.Parameters["@seller"].Value = obj.id;
                    double d = 0;
                    
                    Double.TryParse(CreateDB.cmd.ExecuteScalar().ToString(), out d);
                    obj.Sum += d;
                    obj.DiscountSum += d;
                    resultList.Add(obj);
                }
            } catch (Exception exception) {
                Logger.Log(exception, "Daily Report");
            } finally {
                CreateDB.cmd.Parameters.Clear();
            }
            return resultList;
        }
    }
}
