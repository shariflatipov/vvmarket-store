﻿using System;
using System.Windows.Forms;

namespace magazin_client.reports {
    public partial class DailyReportView : Form {
        public DailyReportView() {
            InitializeComponent();
        }

        private void DailyReportView_Load(object sender, EventArgs e) {
            this.DailyReportObjectBindingSource.DataSource = DailyReport.GetDailyReport();
            this.reportViewer1.RefreshReport();
        }
    }
}
