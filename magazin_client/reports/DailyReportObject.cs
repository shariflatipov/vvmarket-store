﻿namespace magazin_client.reports {

    public class DailyReportObject {
        public int id { get; set; }
        public string Login { get; set; }
        public double Sum { get; set; }
        public double DiscountSum { get; set; }

    }
}
