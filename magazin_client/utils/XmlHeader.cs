﻿using System;
using System.Data.SqlServerCe;
using System.Linq;
using System.Xml.Linq;

namespace magazin_client.utils {

    internal class XmlHeader {

        
        public static XElement GetXmlHeader(string pass, string act, string login) {
            DateTime b = DateTime.Now;
            string dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");

            string password = MyCrypt.GetMd5Hash(pass);
            string hashSum = MyCrypt.GetSha1(dateToSend + "#" + login + "#" + password);
            var magazin = new XElement("magazin");
            var seller = new XElement("seller");
            seller.Add(new XAttribute("login", login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", act));
            seller.Add(new XAttribute("date", dateToSend));
            magazin.Add(seller);
            return magazin;
        }

        public static XElement GetXmlHeader(string act) {
            DateTime b = DateTime.Now;
            string dateToSend = b.ToString("yyyy-MM-dd HH:mm:ss");

            string password = MyCrypt.GetMd5Hash(MainForm.Pass);
            string hashSum = MyCrypt.GetSha1(dateToSend + "#" + MainForm.Login + "#" + password);
            var magazin = new XElement("magazin");
            var seller = new XElement("seller");
            seller.Add(new XAttribute("login", MainForm.Login));
            seller.Add(new XAttribute("checksum", hashSum));
            seller.Add(new XAttribute("stock", MainForm.Stock));
            seller.Add(new XAttribute("act", act));
            seller.Add(new XAttribute("date", dateToSend));
            magazin.Add(seller);
            return magazin;
        }


        public static State ParseResponse(XElement response) {
            try {
                if (response.Elements().Any()) {
                    var xElement = response.Element("trn");
                    if (xElement != null) {
                        Int64 reqTrn = Convert.ToInt64(xElement.Value);
                    }
                    var element = response.Element("status");
                    if (element != null && element.Value.Equals("2")) {
                        return State.Ok;
                    } else {
                        return State.Error;
                    }
                }
            } catch (Exception excep) {
                Logger.Log(excep, "ReturnGood=>Finish", Level.Warining);
                return State.Error;
            }
            return State.Ok;
        }
    }
}