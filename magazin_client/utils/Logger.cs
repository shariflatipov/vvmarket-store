﻿using System;
using System.IO;

namespace magazin_client {

    public enum Level {
        Warining = 2,
        Error = 1,
        Info = 3
    }

    public class Logger {

        public static void Log(Exception ex, string function = "", Level level = Level.Error) {
            try {
                string path = "logs/nonlevel.txt";
                string dt = DateTime.Today.ToShortDateString();

                if (level == Level.Error) {
                    path = "logs/ERROR" + dt + ".txt";
                } else if (level == Level.Warining) {
                    path = "logs/WARNING" + dt + ".txt";
                } else if (level == Level.Info) {
                    path = "logs/INFO" + dt + ".txt";
                }

                FileStream fs = File.Open(path, FileMode.Append, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);
                sw.WriteLine();
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine("Exception: " + ex.ToString());
                sw.WriteLine("Target site: " + ex.TargetSite.Name);
                sw.WriteLine("Function and line: " + function);
                sw.WriteLine("Timestamp: " + DateTime.Now);
                sw.WriteLine(
                    "-------------------------------------------------------------------------------------------------------");
                sw.WriteLine();
                sw.Flush();
                fs.Close();
            } catch {
            }
        }
    }
}
