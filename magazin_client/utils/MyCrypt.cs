﻿using System;
using System.Text;
using System.Diagnostics;
using System.Security.Cryptography;
using System.IO;

namespace magazin_client {
    class MyCrypt {
        /// /// Шифрует строку value
        /// 
        /// Строка которую необходимо зашифровать
        /// Ключ шифрования
        public static string Encrypt(string str, string keyCrypt) {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(str), keyCrypt));
        }

        /// /// Расшифроывает данные из строки value
        /// 
        /// Зашифрованая строка
        /// Ключ шифрования
        /// Возвращает null, если прочесть данные не удалось
        [DebuggerNonUserCodeAttribute]
        public static string Decrypt(string str, string keyCrypt) {
            string result;
            try {
                CryptoStream cs = InternalDecrypt(Convert.FromBase64String(str), keyCrypt);
                StreamReader sr = new StreamReader(cs);

                result = sr.ReadToEnd();

                cs.Close();
                cs.Dispose();

                sr.Close();
                sr.Dispose();
            } catch (CryptographicException ex) {
                Logger.Log(ex, "MyCrypt.cs : function => Decrypt()", Level.Warining);
                return null;
            }

            return result;
        }

        private static byte[] Encrypt(byte[] key, string value) {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateEncryptor((new PasswordDeriveBytes(value, null)).GetBytes(16), new byte[16]);

            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);

            cs.Write(key, 0, key.Length);
            cs.FlushFinalBlock();

            byte[] result = ms.ToArray();

            ms.Close();
            ms.Dispose();

            cs.Close();
            cs.Dispose();

            ct.Dispose();

            return result;
        }

        private static CryptoStream InternalDecrypt(byte[] key, string value) {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateDecryptor((new PasswordDeriveBytes(value, null)).GetBytes(16), new byte[16]);

            MemoryStream ms = new MemoryStream(key);
            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        }

        public static string GetMd5Hash(string input) {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++) {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public static string GetSha1(string userPassword) {
            return BitConverter.ToString(SHA1Managed.Create().ComputeHash(Encoding.Default.GetBytes(userPassword))).Replace("-", "").ToLower();
        }
    }
}
