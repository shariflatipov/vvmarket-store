﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace magazin_client.utils {
    public static class Utils {

        public static string KeyToChar(string key) {
            switch (key) {
                case "NumPad0":
                case "D0":
                    return "0";
                case "NumPad1":
                case "D1":
                    return "1";
                case "NumPad2":
                case "D2":
                    return "2";
                case "NumPad3":
                case "D3":
                    return "3";
                case "NumPad4":
                case "D4":
                    return "4";
                case "NumPad5":
                case "D5":
                    return "5";
                case "NumPad6":
                case "D6":
                    return "6";
                case "NumPad7":
                case "D7":
                    return "7";
                case "NumPad8":
                case "D8":
                    return "8";
                case "NumPad9":
                case "D9":
                    return "9";
                case "Escape":
                    return "esc";
                case "Decimal":
                    return "dec"; // ,
                case "Delete":
                    return "clr";
                default:
                    return "";
            }
        }


        public static Image GetImageFromPath(string path) {
            Image image = null;
            try {
                image = Image.FromFile(path);
            }
            catch (Exception ex) {
                image = Image.FromFile("img/no_image.jpg");
                Logger.Log(ex, path);
            }
            return image;
        }


        public static int CalculateCheckSum(string digits) {
            int i;
            int checksum = 0;


            for (i = 1; i < 12; i += 2) {
                checksum += Convert.ToInt32(digits.Substring(i, 1));
            }
            checksum *= 3;
            for (i = 0; i < 12; i += 2) {
                checksum += Convert.ToInt32(digits.Substring(i, 1));
            }

            return (10 - checksum % 10) % 10;
        }
    }
}
