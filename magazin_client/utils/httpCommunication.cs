﻿using System;
using System.Linq;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Text;

namespace magazin_client {
    class HttpCommunication {
        public static XElement HttpCommun(XElement xml) {
            XElement reqResponse;
            string responseFromServer = "";

            try {
                /************** POST Request method ****************/

                // Create a request for the URL. 		

                HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create(MainForm.Ip + "pos_client/api/");

                string postData = xml.ToString();
                byte[] bytes = Encoding.UTF8.GetBytes(postData);


                myHttpWebRequest.Method = "POST";
                myHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
                myHttpWebRequest.ContentLength = bytes.Length;
                Stream newStream = myHttpWebRequest.GetRequestStream();
                newStream.Write(bytes, 0, bytes.Length);
                newStream.Close();
                HttpWebResponse webResp = (HttpWebResponse)myHttpWebRequest.GetResponse();

                if (webResp.StatusCode == HttpStatusCode.OK) {
                    StreamReader loResponseStream = new StreamReader(webResp.GetResponseStream(), Encoding.UTF8);
                    responseFromServer = loResponseStream.ReadToEnd();
                }

                reqResponse = XElement.Parse(@responseFromServer);
            } catch (Exception ex) {
                Logger.Log(ex, "HttpCommunication.cs : function => HttpCommun()");
                reqResponse = null;
            }
            
            return reqResponse;
        }

        public static void HandleResponseFromServer(XElement xmlToSend, long trnID) {
            HttpCommunication ht = new HttpCommunication();
            try {
                XElement response = HttpCommunication.HttpCommun(xmlToSend);

                if (response.Elements().Any()) {
                    Int64 reqTrn = Convert.ToInt64(response.Element("trn").Value);

                    if (response.Element("status").Value.Equals("2")) {
                    //    SoldProduct.FinishProduct(reqTrn);
                    //} else {
                        SoldProduct.FinishProduct(reqTrn, response.Element("description").Value, Convert.ToInt16(response.Element("status").Value));
                    }
                }
            } catch (Exception excep) {
                Logger.Log(excep, "CreateXML.cs : function => HandleResponseFromServer()");
                try {
                    string ex;

                    if (excep.ToString().Length < 50) {
                        ex = excep.ToString();
                    } else {
                        ex = excep.ToString().Substring(0, 49);
                    }
                    SoldProduct.FinishProduct(trnID, ex, 3);
                } catch (Exception ex) {
                    Logger.Log(ex, "CreateXML.cs : function => HandleResponseFromServer() => exception");
                }
            }
        }
    }
}