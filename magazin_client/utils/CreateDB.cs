﻿using System.Data.SqlServerCe;

namespace magazin_client {
    class CreateDB {
        private const string TableProduct = "CREATE TABLE product(" +
             "product_id nvarchar(13) NOT NULL primary key," +
             "category_id INTEGER NOT NULL," +
             "dependent nvarchar(13) DEFAULT 0," +
             "is_list INTEGER DEFAULT 0," +
             "product_name nvarchar(70) NOT NULL," +
             "product_image nvarchar(255) NOT NULL," +
             "unitID nvarchar(13) NOT NULL," +
             "product_list_price REAL NOT NULL," +
             "product_price REAL NOT NULL," +
             "product_description nvarchar(255));";

        private const string TableReturn = "CREATE TABLE retProd(" +
            "id_return integer identity not null primary key," +
            "sold_id INTEGER NOT NULL," +
            "trn_id INTEGER NOT NULL," +
            "seller_id INTEGER NOT NULL," +
            "date DATETIME NOT NULL," +
            "count REAL NOT NULL," +
            "sum REAL NOT NULL," +
            "status INTEGER NOT NULL);";

        private const string TableSeller = "CREATE TABLE seller(" +
            "id_seller INTEGER NOT NULL PRIMARY KEY IDENTITY," +
            "last_name NVARCHAR(255) NOT NULL," +
            "first_name NVARCHAR(255) NOT NULL," +
            "patronymic NVARCHAR(255) DEFAULT NULL," +
            "birthdate DATETIME NOT NULL," +
            "login NVARCHAR(255) NOT NULL," +
            "pass NVARCHAR(255) NOT NULL," +
            "status INTEGER NOT NULL);";

        private const string TableSold = "CREATE TABLE sold_product(" +
            "id_sold INTEGER identity not null primary key," +
            "seller_id INTEGER NOT NULL, sum FLOAT," +
            "start_date NVARCHAR(20) NOT NULL," +
            "end_date NVARCHAR(20)," +
            "status INTEGER NOT NULL DEFAULT 0," +
            "description NVARCHAR(255));";

        private const string TableTransactions = "CREATE TABLE transactions (" +
            "id_trn       integer identity not null primary key," +
            "sold_id      integer NOT NULL," +
            "product_id   nvarchar(13) NOT NULL," +
            "count        float NOT NULL," +
            "price        float NOT NULL," +
            "discont      float NOT NULL," +
            "status       tinyint NOT NULL," +
            "description  nvarchar(255));";


        private const string TableUnit = "CREATE TABLE unit(" +
            "id_unit INTEGER identity not null primary key," +
            "name_unit NVARCHAR(20)," +
            "status NVARCHAR(70));";

        private const string TableTmp = "CREATE TABLE tmpTable(" +
            "id_trn       integer identity not null primary key," +
            "tableNum     integer not null," +
            "product_id   nvarchar(13) NOT NULL," +
            "count        float NOT NULL," +
            "status       tinyint NOT NULL," +
            "description  nvarchar(255));";

        public static readonly string ConnString = string.Format("Data Source=\"{0}\";Password=\"{1}\"", "db.sdf", "lsJKG98ljf&*^!@#");

        public static SqlCeCommand cmd = new SqlCeCommand();
        public static SqlCeConnection conn = new SqlCeConnection();

        public CreateDB() {
            var createDbPayment = new SqlCeEngine(ConnString);
            createDbPayment.CreateDatabase();

            conn.ConnectionString = ConnString;

            conn.Open();

            SqlCeTransaction trn = conn.BeginTransaction();

            cmd.Transaction = trn;

            cmd.Connection = conn;

            cmd.CommandText = TableProduct;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableReturn;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableSeller;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableSold;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableTransactions;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableUnit;
            cmd.ExecuteNonQuery();

            cmd.CommandText = TableTmp;
            cmd.ExecuteNonQuery();

            trn.Commit();

            conn.Close();
        }
    }
}