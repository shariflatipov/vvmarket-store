﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for NumsTest and is intended
    ///to contain all NumsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class NumsTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Nums Constructor
        ///</summary>
        [TestMethod()]
        public void NumsConstructorTest() {
            ProductCart pc = null; // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            Nums target = new Nums(pc, index);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BackClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BackClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BackClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BtnClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BtnClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CancelClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void CancelClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.CancelClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ClearClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ClearClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ConfirmClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ConfirmClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ConfirmClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DeactivateDispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DeactivateDisposeTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.DeactivateDispose(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for NumsKeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void NumsKeyDownTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            Nums_Accessor target = new Nums_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.NumsKeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
