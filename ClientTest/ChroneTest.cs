﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ChroneTest and is intended
    ///to contain all ChroneTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ChroneTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Chrone Constructor
        ///</summary>
        [TestMethod()]
        public void ChroneConstructorTest() {
            Chrone target = new Chrone();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Discounts
        ///</summary>
        [TestMethod()]
        public void DiscountsTest() {
            Chrone target = new Chrone(); // TODO: Initialize to an appropriate value
            target.Discounts();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SendReTurn
        ///</summary>
        [TestMethod()]
        public void SendReTurnTest() {
            Chrone target = new Chrone(); // TODO: Initialize to an appropriate value
            target.SendReTurn();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SendTurn
        ///</summary>
        [TestMethod()]
        public void SendTurnTest() {
            Chrone target = new Chrone(); // TODO: Initialize to an appropriate value
            target.SendTurn();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for updates
        ///</summary>
        [TestMethod()]
        public void updatesTest() {
            Chrone target = new Chrone(); // TODO: Initialize to an appropriate value
            target.updates();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
