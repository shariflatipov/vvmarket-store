﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Linq;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for HttpCommunicationTest and is intended
    ///to contain all HttpCommunicationTest Unit Tests
    ///</summary>
    [TestClass()]
    public class HttpCommunicationTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for HttpCommunication Constructor
        ///</summary>
        [TestMethod()]
        public void HttpCommunicationConstructorTest() {
            HttpCommunication target = new HttpCommunication();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for HandleResponseFromServer
        ///</summary>
        [TestMethod()]
        public void HandleResponseFromServerTest() {
            XElement xmlToSend = null; // TODO: Initialize to an appropriate value
            long trnID = 0; // TODO: Initialize to an appropriate value
            HttpCommunication.HandleResponseFromServer(xmlToSend, trnID);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for HttpCommun
        ///</summary>
        [TestMethod()]
        public void HttpCommunTest() {
            XElement xml = null; // TODO: Initialize to an appropriate value
            XElement expected = null; // TODO: Initialize to an appropriate value
            XElement actual;
            actual = HttpCommunication.HttpCommun(xml);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
