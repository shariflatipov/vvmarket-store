﻿using magazin_client.goodsreturn;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Linq;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ReturnGoodTest and is intended
    ///to contain all ReturnGoodTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReturnGoodTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ReturnGood Constructor
        ///</summary>
        [TestMethod()]
        public void ReturnGoodConstructorTest() {
            ReturnGood target = new ReturnGood();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for GetById
        ///</summary>
        [TestMethod()]
        public void GetByIdTest() {
            string tmpBarcode = string.Empty; // TODO: Initialize to an appropriate value
            ReturnGood expected = null; // TODO: Initialize to an appropriate value
            ReturnGood actual;
            actual = ReturnGood.GetById(tmpBarcode);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetUnfinished
        ///</summary>
        [TestMethod()]
        public void GetUnfinishedTest() {
            XElement expected = null; // TODO: Initialize to an appropriate value
            XElement actual;
            actual = ReturnGood.GetUnfinished();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for RemoveItem
        ///</summary>
        [TestMethod()]
        public void RemoveItemTest() {
            ReturnGood target = new ReturnGood(); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            target.RemoveItem(index);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Save
        ///</summary>
        [TestMethod()]
        public void SaveTest() {
            ReturnGood target = new ReturnGood(); // TODO: Initialize to an appropriate value
            target.Save();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
