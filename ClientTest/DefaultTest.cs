﻿using magazin_client.peripheral;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing.Printing;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for DefaultTest and is intended
    ///to contain all DefaultTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DefaultTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Default Constructor
        ///</summary>
        [TestMethod()]
        public void DefaultConstructorTest() {
            Default target = new Default();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BoldOff
        ///</summary>
        [TestMethod()]
        public void BoldOffTest() {
            Default target = new Default(); // TODO: Initialize to an appropriate value
            target.BoldOff();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BoldOn
        ///</summary>
        [TestMethod()]
        public void BoldOnTest() {
            Default target = new Default(); // TODO: Initialize to an appropriate value
            target.BoldOn();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Cut
        ///</summary>
        [TestMethod()]
        public void CutTest() {
            Default target = new Default(); // TODO: Initialize to an appropriate value
            target.Cut();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PrintEan13
        ///</summary>
        [TestMethod()]
        public void Ean13Test() {
            Default target = new Default(); // TODO: Initialize to an appropriate value
            string code = string.Empty; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PdPrintPage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void PdPrintPageTest() {
            Default_Accessor target = new Default_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PrintPageEventArgs ev = null; // TODO: Initialize to an appropriate value
            target.PdPrintPage(sender, ev);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Print
        ///</summary>
        [TestMethod()]
        public void PrintTest() {
            Default target = new Default(); // TODO: Initialize to an appropriate value
            string str = string.Empty; // TODO: Initialize to an appropriate value
            target.Print(str);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
