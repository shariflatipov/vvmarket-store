﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;
using magazin_client.peripheral;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ChangeBackTest and is intended
    ///to contain all ChangeBackTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ChangeBackTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ChangeBack Constructor
        ///</summary>
        [TestMethod()]
        public void ChangeBackConstructorTest() {
            double total = 0F; // TODO: Initialize to an appropriate value
            string tableNumber = string.Empty; // TODO: Initialize to an appropriate value
            DataGridView dgv = null; // TODO: Initialize to an appropriate value
            IVfdDisplays vdfDisplay = null; // TODO: Initialize to an appropriate value
            ChangeBack target = new ChangeBack(total, dgv, vdfDisplay);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BackClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BackClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BtnClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BtnDotClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnDotClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Btn_Confirm_Validation
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void Btn_Confirm_ValidationTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            target.Btn_Confirm_Validation();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CancelClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void CancelClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ChangeBack_KeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ChangeBack_KeyDownTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ClearClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ConfirmClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ConfirmClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for LblCountTextChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void LblCountTextChangedTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btnDiscountFixed_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btnDiscountFixed_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btnDiscountPercent_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btnDiscountPercent_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btnDiscount_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btnDiscount_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for lblDiscountTotal_TextChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void lblDiscountTotal_TextChangedTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for lblDiscount_TextChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void lblDiscount_TextChangedTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChangeBack_Accessor target = new ChangeBack_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
