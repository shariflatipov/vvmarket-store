﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for RoundButtonTest and is intended
    ///to contain all RoundButtonTest Unit Tests
    ///</summary>
    [TestClass()]
    public class RoundButtonTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for RoundButton Constructor
        ///</summary>
        [TestMethod()]
        public void RoundButtonConstructorTest() {
            RoundButton target = new RoundButton();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawBackground
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawBackgroundTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawBackground(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawGlow
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawGlowTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawGlow(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawHighlight
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawHighlightTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawHighlight(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawImageTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawImage(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawInnerStroke
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawInnerStrokeTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawInnerStroke(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawOuterStroke
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawOuterStrokeTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawOuterStroke(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DrawText
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DrawTextTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.DrawText(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RoundRect
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void RoundRectTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            RectangleF r = new RectangleF(); // TODO: Initialize to an appropriate value
            float r1 = 0F; // TODO: Initialize to an appropriate value
            float r2 = 0F; // TODO: Initialize to an appropriate value
            float r3 = 0F; // TODO: Initialize to an appropriate value
            float r4 = 0F; // TODO: Initialize to an appropriate value
            GraphicsPath expected = null; // TODO: Initialize to an appropriate value
            GraphicsPath actual;
            actual = target.RoundRect(r, r1, r2, r3, r4);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SetClip
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SetClipTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            Graphics g = null; // TODO: Initialize to an appropriate value
            target.SetClip(g);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for StringFormatAlignment
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void StringFormatAlignmentTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            ContentAlignment textalign = new ContentAlignment(); // TODO: Initialize to an appropriate value
            StringFormat expected = null; // TODO: Initialize to an appropriate value
            StringFormat actual;
            actual = target.StringFormatAlignment(textalign);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for VistaButton_KeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_KeyDownTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_KeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_KeyUp
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_KeyUpTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_KeyUp(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_MouseDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_MouseDownTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_MouseDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_MouseEnter
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_MouseEnterTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_MouseEnter(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_MouseLeave
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_MouseLeaveTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_MouseLeave(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_MouseUp
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_MouseUpTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_MouseUp(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_Paint
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_PaintTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            PaintEventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_Paint(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for VistaButton_Resize
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void VistaButton_ResizeTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.VistaButton_Resize(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for mFadeIn_Tick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void mFadeIn_TickTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.mFadeIn_Tick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for mFadeOut_Tick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void mFadeOut_TickTest() {
            RoundButton_Accessor target = new RoundButton_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.mFadeOut_Tick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BackImage
        ///</summary>
        [TestMethod()]
        public void BackImageTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Image expected = null; // TODO: Initialize to an appropriate value
            Image actual;
            target.BackImage = expected;
            actual = target.BackImage;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for BaseColor
        ///</summary>
        [TestMethod()]
        public void BaseColorTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Color expected = new Color(); // TODO: Initialize to an appropriate value
            Color actual;
            target.BaseColor = expected;
            actual = target.BaseColor;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ButtonColor
        ///</summary>
        [TestMethod()]
        public void ButtonColorTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Color expected = new Color(); // TODO: Initialize to an appropriate value
            Color actual;
            target.ButtonColor = expected;
            actual = target.ButtonColor;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ButtonStyle
        ///</summary>
        [TestMethod()]
        public void ButtonStyleTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            RoundButton.Style expected = new RoundButton.Style(); // TODO: Initialize to an appropriate value
            RoundButton.Style actual;
            target.ButtonStyle = expected;
            actual = target.ButtonStyle;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ButtonText
        ///</summary>
        [TestMethod()]
        public void ButtonTextTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            target.ButtonText = expected;
            actual = target.ButtonText;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CornerRadius
        ///</summary>
        [TestMethod()]
        public void CornerRadiusTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            target.CornerRadius = expected;
            actual = target.CornerRadius;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ForeColor
        ///</summary>
        [TestMethod()]
        public void ForeColorTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Color expected = new Color(); // TODO: Initialize to an appropriate value
            Color actual;
            target.ForeColor = expected;
            actual = target.ForeColor;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GlowColor
        ///</summary>
        [TestMethod()]
        public void GlowColorTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Color expected = new Color(); // TODO: Initialize to an appropriate value
            Color actual;
            target.GlowColor = expected;
            actual = target.GlowColor;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HighlightColor
        ///</summary>
        [TestMethod()]
        public void HighlightColorTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Color expected = new Color(); // TODO: Initialize to an appropriate value
            Color actual;
            target.HighlightColor = expected;
            actual = target.HighlightColor;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Image
        ///</summary>
        [TestMethod()]
        public void ImageTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Image expected = null; // TODO: Initialize to an appropriate value
            Image actual;
            target.Image = expected;
            actual = target.Image;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ImageAlign
        ///</summary>
        [TestMethod()]
        public void ImageAlignTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            ContentAlignment expected = new ContentAlignment(); // TODO: Initialize to an appropriate value
            ContentAlignment actual;
            target.ImageAlign = expected;
            actual = target.ImageAlign;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ImageSize
        ///</summary>
        [TestMethod()]
        public void ImageSizeTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            Size expected = new Size(); // TODO: Initialize to an appropriate value
            Size actual;
            target.ImageSize = expected;
            actual = target.ImageSize;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for TextAlign
        ///</summary>
        [TestMethod()]
        public void TextAlignTest() {
            RoundButton target = new RoundButton(); // TODO: Initialize to an appropriate value
            ContentAlignment expected = new ContentAlignment(); // TODO: Initialize to an appropriate value
            ContentAlignment actual;
            target.TextAlign = expected;
            actual = target.TextAlign;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
