﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for PrintChkTest and is intended
    ///to contain all PrintChkTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PrintChkTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for PrintChk Constructor
        ///</summary>
        [TestMethod()]
        public void PrintChkConstructorTest() {
            string accept = string.Empty; // TODO: Initialize to an appropriate value
            string change = string.Empty; // TODO: Initialize to an appropriate value
            string total = string.Empty; // TODO: Initialize to an appropriate value
            string discount = string.Empty; // TODO: Initialize to an appropriate value
            DataGridView dgv = null; // TODO: Initialize to an appropriate value
            string subTotal = string.Empty; // TODO: Initialize to an appropriate value
            PrintChk target = new PrintChk(accept, change, total, discount, dgv, subTotal);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Init
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            PrintChk_Accessor target = new PrintChk_Accessor(param0); // TODO: Initialize to an appropriate value
            target.Init();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SaveToFile
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SaveToFileTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            PrintChk_Accessor target = new PrintChk_Accessor(param0); // TODO: Initialize to an appropriate value
            string str = string.Empty; // TODO: Initialize to an appropriate value
            target.SaveToFile(str);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
