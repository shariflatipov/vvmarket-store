﻿using magazin_client.goodsreturn;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ReturnMainTest and is intended
    ///to contain all ReturnMainTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ReturnMainTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ReturnMain Constructor
        ///</summary>
        [TestMethod()]
        public void ReturnMainConstructorTest() {
            ReturnMain target = new ReturnMain();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BtnConfirmClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnConfirmClickTest() {
            ReturnMain_Accessor target = new ReturnMain_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BtnConfirmClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DgReturnCellClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DgReturnCellClickTest() {
            ReturnMain_Accessor target = new ReturnMain_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DataGridViewCellEventArgs e = null; // TODO: Initialize to an appropriate value
            target.DgReturnCellClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            ReturnMain_Accessor target = new ReturnMain_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            ReturnMain_Accessor target = new ReturnMain_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for TxtBarcodeTextChanged
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void TxtBarcodeTextChangedTest() {
            ReturnMain_Accessor target = new ReturnMain_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.TxtBarcodeTextChanged(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
