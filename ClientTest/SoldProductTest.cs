﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for SoldProductTest and is intended
    ///to contain all SoldProductTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SoldProductTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for SoldProduct Constructor
        ///</summary>
        [TestMethod()]
        public void SoldProductConstructorTest() {
            SoldProduct target = new SoldProduct();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for FinishProduct
        ///</summary>
        [TestMethod()]
        public void FinishProductTest() {
            long trnId = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = SoldProduct.FinishProduct(trnId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for FinishProduct
        ///</summary>
        [TestMethod()]
        public void FinishProductTest1() {
            long trnId = 0; // TODO: Initialize to an appropriate value
            string description = string.Empty; // TODO: Initialize to an appropriate value
            short status = 0; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = SoldProduct.FinishProduct(trnId, description, status);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetMaxId
        ///</summary>
        [TestMethod()]
        public void GetMaxIdTest() {
            SoldProduct target = new SoldProduct(); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            actual = target.GetMaxId();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InsertProduct
        ///</summary>
        [TestMethod()]
        public void InsertProductTest() {
            SoldProduct target = new SoldProduct(); // TODO: Initialize to an appropriate value
            string sellerId = string.Empty; // TODO: Initialize to an appropriate value
            target.InsertProduct(sellerId);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for UpdateProduct
        ///</summary>
        [TestMethod()]
        public void UpdateProductTest() {
            SoldProduct target = new SoldProduct(); // TODO: Initialize to an appropriate value
            double total = 0F; // TODO: Initialize to an appropriate value
            long trnId = 0; // TODO: Initialize to an appropriate value
            double discount = 0F; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
