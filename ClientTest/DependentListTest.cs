﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for DependentListTest and is intended
    ///to contain all DependentListTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DependentListTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DependentList Constructor
        ///</summary>
        [TestMethod()]
        public void DependentListConstructorTest() {
            ProductCart pc = null; // TODO: Initialize to an appropriate value
            string id = string.Empty; // TODO: Initialize to an appropriate value
            DependentList target = new DependentList(pc, id);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for DependentList_Load
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DependentList_LoadTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.DependentList_Load(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetListContent
        ///</summary>
        [TestMethod()]
        public void GetListContentTest() {
            ListView depList = null; // TODO: Initialize to an appropriate value
            ImageList im = null; // TODO: Initialize to an appropriate value
            string commandType = string.Empty; // TODO: Initialize to an appropriate value
            string depId = string.Empty; // TODO: Initialize to an appropriate value
            DependentList.GetListContent(depList, im, commandType, depId);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetListContent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void GetListContentTest1() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            ImageList im = null; // TODO: Initialize to an appropriate value
            string commandType = string.Empty; // TODO: Initialize to an appropriate value
            string depId = string.Empty; // TODO: Initialize to an appropriate value
            target.GetListContent(im, commandType, depId);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button1_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void button1_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button1_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button2_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void button2_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button2_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button3_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void button3_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button3_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button4_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void button4_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button4_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for depList_MouseClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void depList_MouseClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            DependentList_Accessor target = new DependentList_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            MouseEventArgs e = null; // TODO: Initialize to an appropriate value
            target.depList_MouseClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
