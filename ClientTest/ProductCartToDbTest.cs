﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ProductCartToDbTest and is intended
    ///to contain all ProductCartToDbTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProductCartToDbTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProductCartToDb Constructor
        ///</summary>
        [TestMethod()]
        public void ProductCartToDbConstructorTest() {
            ProductCartToDb target = new ProductCartToDb();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for ChangeProductQuantity
        ///</summary>
        [TestMethod()]
        public void ChangeProductQuantityTest() {
            Product product = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = ProductCartToDb.ChangeProductQuantity(product);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for deleteAllProducts
        ///</summary>
        [TestMethod()]
        public void deleteAllProductsTest() {
            ProductCartToDb target = new ProductCartToDb(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.deleteAllProducts();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for deleteProduct
        ///</summary>
        [TestMethod()]
        public void deleteProductTest() {
            ProductCartToDb target = new ProductCartToDb(); // TODO: Initialize to an appropriate value
            string barcode = string.Empty; // TODO: Initialize to an appropriate value
            string price = string.Empty; // TODO: Initialize to an appropriate value
            string quantity = string.Empty; // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.deleteProduct(barcode, price, quantity, batch);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for insertNewProduct
        ///</summary>
        [TestMethod()]
        public void insertNewProductTest() {
            ProductCartToDb target = new ProductCartToDb(); // TODO: Initialize to an appropriate value
            Product product = null; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.insertNewProduct(product);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for restoreBasket
        ///</summary>
        [TestMethod()]
        public void restoreBasketTest() {
            ProductCartToDb target = new ProductCartToDb(); // TODO: Initialize to an appropriate value
            long trnId = 0; // TODO: Initialize to an appropriate value
            IEnumerable<Product> expected = null; // TODO: Initialize to an appropriate value
            IEnumerable<Product> actual;
            actual = target.restoreBasket(trnId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for storeBasket
        ///</summary>
        [TestMethod()]
        public void storeBasketTest() {
            ProductCartToDb target = new ProductCartToDb(); // TODO: Initialize to an appropriate value
            long expected = 0; // TODO: Initialize to an appropriate value
            long actual;
            actual = target.storeBasket();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
