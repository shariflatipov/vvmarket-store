﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ChooseBatchTest and is intended
    ///to contain all ChooseBatchTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ChooseBatchTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ChooseBatch Constructor
        ///</summary>
        [TestMethod()]
        public void ChooseBatchConstructorTest() {
            List<Product> products = null; // TODO: Initialize to an appropriate value
            ChooseBatch target = new ChooseBatch(products);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChooseBatch_Accessor target = new ChooseBatch_Accessor(param0); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChooseBatch_Accessor target = new ChooseBatch_Accessor(param0); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btn_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btn_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChooseBatch_Accessor target = new ChooseBatch_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.btn_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for button1_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void button1_ClickTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ChooseBatch_Accessor target = new ChooseBatch_Accessor(param0); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.button1_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
