﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ProductCartTest and is intended
    ///to contain all ProductCartTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProductCartTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProductCart Constructor
        ///</summary>
        [TestMethod()]
        public void ProductCartConstructorTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc);
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddItem
        ///</summary>
        [TestMethod()]
        public void AddItemTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            Product product = null; // TODO: Initialize to an appropriate value
            target.AddItem(product);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddItemByPrice
        ///</summary>
        [TestMethod()]
        public void AddItemByPriceTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            double price = 0F; // TODO: Initialize to an appropriate value
            target.AddItemByPrice(price);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for AddProduct
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void AddProductTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ProductCart_Accessor target = new ProductCart_Accessor(param0); // TODO: Initialize to an appropriate value
            Product product = null; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CalculateSubTotal
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void CalculateSubTotalTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ProductCart_Accessor target = new ProductCart_Accessor(param0); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CalculateTotal
        ///</summary>
        [TestMethod()]
        public void CalculateTotalTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.CalculateTotal();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ChangeQuantity
        ///</summary>
        [TestMethod()]
        public void ChangeQuantityTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            target.ChangeQuantity(index, quantity);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ChangeQuantityByPack
        ///</summary>
        [TestMethod()]
        public void ChangeQuantityByPackTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            short count = 0; // TODO: Initialize to an appropriate value
            target.ChangeQuantityByPack(index, quantity, count);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CheckProductExistance
        ///</summary>
        [TestMethod()]
        public void CheckProductExistanceTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            string productId = string.Empty; // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.CheckProductExistance(productId, batch);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Clear
        ///</summary>
        [TestMethod()]
        public void ClearTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            target.Clear();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearHighlighted
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ClearHighlightedTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ProductCart_Accessor target = new ProductCart_Accessor(param0); // TODO: Initialize to an appropriate value
            target.ClearHighlighted();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetCurrentProduct
        ///</summary>
        [TestMethod()]
        public void GetCurrentProductTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            Product expected = null; // TODO: Initialize to an appropriate value
            Product actual;
            actual = target.GetCurrentProduct(index);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetCurrentProduct
        ///</summary>
        [TestMethod()]
        public void GetCurrentProductTest1() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            Product expected = null; // TODO: Initialize to an appropriate value
            Product actual;
            actual = target.GetCurrentProduct();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HotRowsAdded
        ///</summary>
        [TestMethod()]
        public void HotRowsAddedTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            target.HotRowsAdded();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Remove
        ///</summary>
        [TestMethod()]
        public void RemoveTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            target.Remove();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RemoveItem
        ///</summary>
        [TestMethod()]
        public void RemoveItemTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            int index = 0; // TODO: Initialize to an appropriate value
            target.RemoveItem(index);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RestoreBasket
        ///</summary>
        [TestMethod()]
        public void RestoreBasketTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.RestoreBasket();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SaveBasket
        ///</summary>
        [TestMethod()]
        public void SaveBasketTest() {
            DataGridView dgvc = null; // TODO: Initialize to an appropriate value
            ProductCart target = new ProductCart(dgvc); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.SaveBasket();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SetSelectedItem
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SetSelectedItemTest() {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            ProductCart_Accessor target = new ProductCart_Accessor(param0); // TODO: Initialize to an appropriate value
            int itemIndex = 0; // TODO: Initialize to an appropriate value
            target.SetSelectedItem(itemIndex);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
