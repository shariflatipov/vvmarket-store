﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Collections.Generic;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ProductTest and is intended
    ///to contain all ProductTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProductTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Product Constructor
        ///</summary>
        [TestMethod()]
        public void ProductConstructorTest() {
            Product target = new Product();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for AddQuantity
        ///</summary>
        [TestMethod()]
        public void AddQuantityTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            target.AddQuantity(quantity);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DeleteProduct
        ///</summary>
        [TestMethod()]
        public void DeleteProductTest() {
            string productId = string.Empty; // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            Product.DeleteProduct(productId, batch);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetBatch
        ///</summary>
        [TestMethod()]
        public void GetBatchTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetBatch();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetCount
        ///</summary>
        [TestMethod()]
        public void GetCountTest() {
            string productId = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = Product.GetCount(productId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetDescription
        ///</summary>
        [TestMethod()]
        public void GetDescriptionTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetDescription();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetDiscount
        ///</summary>
        [TestMethod()]
        public void GetDiscountTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetDiscount();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetId
        ///</summary>
        [TestMethod()]
        public void GetIdTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetId();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetImage
        ///</summary>
        [TestMethod()]
        public void GetImageTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            Bitmap expected = null; // TODO: Initialize to an appropriate value
            Bitmap actual;
            actual = target.GetImage();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetName
        ///</summary>
        [TestMethod()]
        public void GetNameTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetName();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPackCount
        ///</summary>
        [TestMethod()]
        public void GetPackCountTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            short expected = 0; // TODO: Initialize to an appropriate value
            short actual;
            actual = target.GetPackCount();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetPrice
        ///</summary>
        [TestMethod()]
        public void GetPriceTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetPrice();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetProducts
        ///</summary>
        [TestMethod()]
        public void GetProductsTest() {
            string productId = string.Empty; // TODO: Initialize to an appropriate value
            List<Product> expected = null; // TODO: Initialize to an appropriate value
            List<Product> actual;
            actual = Product.GetProducts(productId);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetQuantity
        ///</summary>
        [TestMethod()]
        public void GetQuantityTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetQuantity();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetTotal
        ///</summary>
        [TestMethod()]
        public void GetTotalTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.GetTotal();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetUnit
        ///</summary>
        [TestMethod()]
        public void GetUnitTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetUnit();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InsertOrUpdateProduct
        ///</summary>
        [TestMethod()]
        public void InsertOrUpdateProductTest() {
            //string isList = string.Empty; // TODO: Initialize to an appropriate value
            //string dependent = string.Empty; // TODO: Initialize to an appropriate value
            //string prodId = string.Empty; // TODO: Initialize to an appropriate value
            //string prodName = string.Empty; // TODO: Initialize to an appropriate value
            //string prodImage = string.Empty; // TODO: Initialize to an appropriate value
            //string prodUnit = string.Empty; // TODO: Initialize to an appropriate value
            //string prodListPrice = string.Empty; // TODO: Initialize to an appropriate value
            //string prodPrice = string.Empty; // TODO: Initialize to an appropriate value
            //string packCount = string.Empty; // TODO: Initialize to an appropriate value
            //string batch = string.Empty; // TODO: Initialize to an appropriate value
            //int expected = 0; // TODO: Initialize to an appropriate value
            //int actual;
            //actual = Product.InsertOrUpdateProduct(isList, dependent, prodId, prodName, prodImage, prodUnit, prodListPrice, prodPrice, packCount, batch);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InsertProduct
        ///</summary>
        [TestMethod()]
        public void InsertProductTest() {
            string isList = string.Empty; // TODO: Initialize to an appropriate value
            string dependent = string.Empty; // TODO: Initialize to an appropriate value
            string prodId = string.Empty; // TODO: Initialize to an appropriate value
            string prodName = string.Empty; // TODO: Initialize to an appropriate value
            string prodImage = string.Empty; // TODO: Initialize to an appropriate value
            string prodUnit = string.Empty; // TODO: Initialize to an appropriate value
            string prodListPrice = string.Empty; // TODO: Initialize to an appropriate value
            string prodPrice = string.Empty; // TODO: Initialize to an appropriate value
            string packCount = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = Product.InsertProduct(isList, dependent, prodId, prodName, prodImage, prodUnit, prodListPrice, prodPrice, packCount);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SetBatch
        ///</summary>
        [TestMethod()]
        public void SetBatchTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            target.SetBatch(batch);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetDescription
        ///</summary>
        [TestMethod()]
        public void SetDescriptionTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string description = string.Empty; // TODO: Initialize to an appropriate value
            target.SetDescription(description);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetDiscount
        ///</summary>
        [TestMethod()]
        public void SetDiscountTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double discount = 0F; // TODO: Initialize to an appropriate value
            target.SetDiscount(discount);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetId
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SetIdTest() {
            Product_Accessor target = new Product_Accessor(); // TODO: Initialize to an appropriate value
            string id = string.Empty; // TODO: Initialize to an appropriate value
            target.SetId(id);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetImage
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SetImageTest() {
            Product_Accessor target = new Product_Accessor(); // TODO: Initialize to an appropriate value
            string path = string.Empty; // TODO: Initialize to an appropriate value
            target.SetImage(path);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetImage
        ///</summary>
        [TestMethod()]
        public void SetImageTest1() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            Bitmap image = null; // TODO: Initialize to an appropriate value
            target.SetImage(image);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetName
        ///</summary>
        [TestMethod()]
        public void SetNameTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            target.SetName(name);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetPackCount
        ///</summary>
        [TestMethod()]
        public void SetPackCountTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            short packCount = 0; // TODO: Initialize to an appropriate value
            target.SetPackCount(packCount);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetPrice
        ///</summary>
        [TestMethod()]
        public void SetPriceTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double price = 0F; // TODO: Initialize to an appropriate value
            target.SetPrice(price);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetProductByFields
        ///</summary>
        [TestMethod()]
        public void SetProductByFieldsTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string id = string.Empty; // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            string description = string.Empty; // TODO: Initialize to an appropriate value
            string unit = string.Empty; // TODO: Initialize to an appropriate value
            double price = 0F; // TODO: Initialize to an appropriate value
            string image = string.Empty; // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            double discount = 0F; // TODO: Initialize to an appropriate value
            short packCount = 0; // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            target.SetProductByFields(id, name, description, unit, price, image, quantity, discount, packCount, batch);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetProductById
        ///</summary>
        [TestMethod()]
        public void SetProductByIdTest() {
            string barcode = string.Empty; // TODO: Initialize to an appropriate value
            string batch = string.Empty; // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            Product expected = null; // TODO: Initialize to an appropriate value
            Product actual;
            actual = Product.SetProductById(barcode, batch, quantity);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for SetQuantity
        ///</summary>
        [TestMethod()]
        public void SetQuantityTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            double quantity = 0F; // TODO: Initialize to an appropriate value
            target.SetQuantity(quantity);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetUnit
        ///</summary>
        [TestMethod()]
        public void SetUnitTest() {
            Product target = new Product(); // TODO: Initialize to an appropriate value
            string unit = string.Empty; // TODO: Initialize to an appropriate value
            target.SetUnit(unit);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for UpdateProduct
        ///</summary>
        [TestMethod()]
        public void UpdateProductTest() {
            string isList = string.Empty; // TODO: Initialize to an appropriate value
            string dependent = string.Empty; // TODO: Initialize to an appropriate value
            string prodId = string.Empty; // TODO: Initialize to an appropriate value
            string prodName = string.Empty; // TODO: Initialize to an appropriate value
            string prodImage = string.Empty; // TODO: Initialize to an appropriate value
            string prodUnit = string.Empty; // TODO: Initialize to an appropriate value
            string prodListPrice = string.Empty; // TODO: Initialize to an appropriate value
            string prodPrice = string.Empty; // TODO: Initialize to an appropriate value
            string packCount = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = Product.UpdateProduct(isList, dependent, prodId, prodName, prodImage, prodUnit, prodListPrice, prodPrice, packCount);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
