﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml.Linq;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ProductUpdateTest and is intended
    ///to contain all ProductUpdateTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ProductUpdateTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for ProductUpdate Constructor
        ///</summary>
        [TestMethod()]
        public void ProductUpdateConstructorTest() {
            ProductUpdate target = new ProductUpdate();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CreateUpdateTask
        ///</summary>
        [TestMethod()]
        public void CreateUpdateTaskTest() {
            ProductUpdate target = new ProductUpdate(); // TODO: Initialize to an appropriate value
            UpdateTask type = new UpdateTask(); // TODO: Initialize to an appropriate value
            XElement expected = null; // TODO: Initialize to an appropriate value
            XElement actual;
            actual = target.CreateUpdateTask(type);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for HandleRecievedUpdate
        ///</summary>
        [TestMethod()]
        public void HandleRecievedUpdateTest() {
            XElement magazin = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = ProductUpdate.HandleRecievedUpdate(magazin);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Init
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitTest() {
            ProductUpdate_Accessor target = new ProductUpdate_Accessor(); // TODO: Initialize to an appropriate value
            target.Init();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
