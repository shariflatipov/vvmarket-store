﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for MainWorkFormTest and is intended
    ///to contain all MainWorkFormTest Unit Tests
    ///</summary>
    [TestClass()]
    public class MainWorkFormTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for MainWorkForm Constructor
        ///</summary>
        [TestMethod()]
        public void MainWorkFormConstructorTest() {
            MainWorkForm target = new MainWorkForm();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BtnConfirmBuyClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnConfirmBuyClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BtnConfirmBuyClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for CalculateTotal
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void CalculateTotalTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            target.CalculateTotal();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearDataClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ClearDataClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ClearDataClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearDgProduct
        ///</summary>
        [TestMethod()]
        public void ClearDgProductTest() {
            MainWorkForm target = new MainWorkForm(); // TODO: Initialize to an appropriate value
            target.ClearDgProduct();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearFormLbl
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ClearFormLblTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            target.ClearFormLbl();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for DgProductCartCellClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DgProductCartCellClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DataGridViewCellEventArgs e = null; // TODO: Initialize to an appropriate value
            target.DgProductCartCellClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MainWorkFormKeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void MainWorkFormKeyDownTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = null; // TODO: Initialize to an appropriate value
            target.MainWorkFormKeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for MainWorkForm_Load
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void MainWorkForm_LoadTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.MainWorkForm_Load(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PicCart_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void PicCart_ClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.PicCart_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PicUsrLogoClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void PicUsrLogoClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.PicUsrLogoClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for RestoreDataClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void RestoreDataClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.RestoreDataClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SaveDataClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SaveDataClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.SaveDataClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for SetProductSettings
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void SetProductSettingsTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            target.SetProductSettings();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ShowHotPlug
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ShowHotPlugTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ShowHotPlug(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for picProductCost_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void picProductCost_ClickTest() {
            MainWorkForm_Accessor target = new MainWorkForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.picProductCost_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
