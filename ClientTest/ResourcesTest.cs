﻿using magazin_client.Properties;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Globalization;
using System.Resources;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for ResourcesTest and is intended
    ///to contain all ResourcesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ResourcesTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Resources Constructor
        ///</summary>
        [TestMethod()]
        public void ResourcesConstructorTest() {
            Resources target = new Resources();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Actions_edit_clear_icon
        ///</summary>
        [TestMethod()]
        public void Actions_edit_clear_iconTest() {
            Bitmap actual;
            actual = Resources.deleteAll;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ChangeBackBackground
        ///</summary>
        [TestMethod()]
        public void ChangeBackBackgroundTest() {
            Bitmap actual;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Culture
        ///</summary>
        [TestMethod()]
        public void CultureTest() {
            CultureInfo expected = null; // TODO: Initialize to an appropriate value
            CultureInfo actual;
            Resources.Culture = expected;
            actual = Resources.Culture;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for ResourceManager
        ///</summary>
        [TestMethod()]
        public void ResourceManagerTest() {
            ResourceManager actual;
            actual = Resources.ResourceManager;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Sign_Close_icon
        ///</summary>
        [TestMethod()]
        public void Sign_Close_iconTest() {
            Bitmap actual;
            actual = Resources.Close;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for add
        ///</summary>
        [TestMethod()]
        public void addTest() {
            Bitmap actual;
            actual = Resources.add;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for apple_aluminum_keyboard1
        ///</summary>
        [TestMethod()]
        public void apple_aluminum_keyboard1Test() {
            Bitmap actual;
            actual = Resources.apple_aluminum_keyboard1;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for camera_test
        ///</summary>
        [TestMethod()]
        public void camera_testTest() {
            Bitmap actual;
            actual = Resources.camera_test;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for document_save
        ///</summary>
        [TestMethod()]
        public void document_saveTest() {
            Bitmap actual;
            actual = Resources.document_save;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for images
        ///</summary>
        [TestMethod()]
        public void imagesTest() {
            Bitmap actual;
            actual = Resources.images;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for korzina209082
        ///</summary>
        [TestMethod()]
        public void korzina209082Test() {
            Bitmap actual;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for money
        ///</summary>
        [TestMethod()]
        public void moneyTest() {
            Bitmap actual;
            actual = Resources.money;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for shopping_cart_accept_icon
        ///</summary>
        [TestMethod()]
        public void shopping_cart_accept_iconTest() {
            Bitmap actual;
            actual = Resources.cart;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for skies
        ///</summary>
        [TestMethod()]
        public void skiesTest() {
            Bitmap actual;
            actual = Resources.skies;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for view_refresh
        ///</summary>
        [TestMethod()]
        public void view_refreshTest() {
            Bitmap actual;
            actual = Resources.view_refresh;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
