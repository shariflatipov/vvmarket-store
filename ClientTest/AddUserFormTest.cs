﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for AddUserFormTest and is intended
    ///to contain all AddUserFormTest Unit Tests
    ///</summary>
    [TestClass()]
    public class AddUserFormTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for AddUserForm Constructor
        ///</summary>
        [TestMethod()]
        public void AddUserFormConstructorTest() {
            AddUserForm target = new AddUserForm();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for BtnEnterClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnEnterClickTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BtnEnterClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BtnExitClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void BtnExitClickTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.BtnExitClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ConfirmAddUser
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void ConfirmAddUserTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.ConfirmAddUser(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for addUser
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void addUserTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            string lastName = string.Empty; // TODO: Initialize to an appropriate value
            string firstName = string.Empty; // TODO: Initialize to an appropriate value
            string patronimyc = string.Empty; // TODO: Initialize to an appropriate value
            string pass = string.Empty; // TODO: Initialize to an appropriate value
            short status = 0; // TODO: Initialize to an appropriate value
            DateTime birthDate = new DateTime(); // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.addUser(lastName, firstName, patronimyc, pass, status, birthDate);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for isLoginExists
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void isLoginExistsTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            string login = string.Empty; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.isLoginExists(login);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for resetFields
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void resetFieldsTest() {
            AddUserForm_Accessor target = new AddUserForm_Accessor(); // TODO: Initialize to an appropriate value
            target.resetFields();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
