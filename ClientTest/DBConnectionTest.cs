﻿using magazin_client;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for DBConnectionTest and is intended
    ///to contain all DBConnectionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DBConnectionTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DBConnection Constructor
        ///</summary>
        [TestMethod()]
        public void DBConnectionConstructorTest() {
            DBConnection target = new DBConnection();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for DBConnection_Load
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DBConnection_LoadTest() {
            DBConnection_Accessor target = new DBConnection_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.DBConnection_Load(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            DBConnection_Accessor target = new DBConnection_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetDBValues
        ///</summary>
        [TestMethod()]
        public void GetDBValuesTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            string param = string.Empty; // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetDBValues(param);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetHDDSerial
        ///</summary>
        [TestMethod()]
        public void GetHDDSerialTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetHDDSerial();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetMACAddress
        ///</summary>
        [TestMethod()]
        public void GetMACAddressTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            target.GetMACAddress();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for GetMotherboardSerial
        ///</summary>
        [TestMethod()]
        public void GetMotherboardSerialTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            string expected = string.Empty; // TODO: Initialize to an appropriate value
            string actual;
            actual = target.GetMotherboardSerial();
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            DBConnection_Accessor target = new DBConnection_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for UpdateDBValues
        ///</summary>
        [TestMethod()]
        public void UpdateDBValuesTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            string param = string.Empty; // TODO: Initialize to an appropriate value
            string value = string.Empty; // TODO: Initialize to an appropriate value
            target.UpdateDBValues(param, value);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for insertToDB
        ///</summary>
        [TestMethod()]
        public void insertToDBTest() {
            DBConnection target = new DBConnection(); // TODO: Initialize to an appropriate value
            string name = string.Empty; // TODO: Initialize to an appropriate value
            string status = string.Empty; // TODO: Initialize to an appropriate value
            target.insertToDB(name, status);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
