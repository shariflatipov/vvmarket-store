﻿using magazin_client.peripheral;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for IPrinterTest and is intended
    ///to contain all IPrinterTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IPrinterTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        internal virtual IPrinter CreateIPrinter() {
            // TODO: Instantiate an appropriate concrete class.
            IPrinter target = null;
            return target;
        }

        /// <summary>
        ///A test for BoldOff
        ///</summary>
        [TestMethod()]
        public void BoldOffTest() {
            IPrinter target = CreateIPrinter(); // TODO: Initialize to an appropriate value
            target.BoldOff();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for BoldOn
        ///</summary>
        [TestMethod()]
        public void BoldOnTest() {
            IPrinter target = CreateIPrinter(); // TODO: Initialize to an appropriate value
            target.BoldOn();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Cut
        ///</summary>
        [TestMethod()]
        public void CutTest() {
            IPrinter target = CreateIPrinter(); // TODO: Initialize to an appropriate value
            target.Cut();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for PrintEan13
        ///</summary>
        [TestMethod()]
        public void Ean13Test() {
            IPrinter target = CreateIPrinter(); // TODO: Initialize to an appropriate value
            string code = string.Empty; // TODO: Initialize to an appropriate value
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Print
        ///</summary>
        [TestMethod()]
        public void PrintTest() {
            IPrinter target = CreateIPrinter(); // TODO: Initialize to an appropriate value
            string str = string.Empty; // TODO: Initialize to an appropriate value
            target.Print(str);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
