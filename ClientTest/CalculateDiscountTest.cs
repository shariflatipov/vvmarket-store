﻿using magazin_client.discount;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for CalculateDiscountTest and is intended
    ///to contain all CalculateDiscountTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CalculateDiscountTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculateDiscount Constructor
        ///</summary>
        [TestMethod()]
        public void CalculateDiscountConstructorTest() {
            CalculateDiscount target = new CalculateDiscount();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CalculateDiscount_KeyDown
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void CalculateDiscount_KeyDownTest() {
            CalculateDiscount_Accessor target = new CalculateDiscount_Accessor(); // TODO: Initialize to an appropriate value
            
            object sender = null; // TODO: Initialize to an appropriate value
            KeyEventArgs e = new KeyEventArgs(Keys.D0); // TODO: Initialize to an appropriate value
            target.CalculateDiscount_KeyDown(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            CalculateDiscount_Accessor target = new CalculateDiscount_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            CalculateDiscount_Accessor target = new CalculateDiscount_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for discount
        ///</summary>
        [TestMethod()]
        public void discountTest() {
            CalculateDiscount target = new CalculateDiscount(); // TODO: Initialize to an appropriate value
            
            Discount expected = null; // TODO: Initialize to an appropriate value
            Discount actual;
            target.discount = expected;
            actual = target.discount;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
