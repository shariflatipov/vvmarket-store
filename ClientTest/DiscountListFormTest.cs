﻿using magazin_client.discount;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Windows.Forms;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for DiscountListFormTest and is intended
    ///to contain all DiscountListFormTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DiscountListFormTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for DiscountListForm Constructor
        ///</summary>
        [TestMethod()]
        public void DiscountListFormConstructorTest() {
            DiscountListForm target = new DiscountListForm();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for DiscountListForm_Load
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DiscountListForm_LoadTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.DiscountListForm_Load(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void DisposeTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            bool disposing = false; // TODO: Initialize to an appropriate value
            target.Dispose(disposing);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for InitializeComponent
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void InitializeComponentTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            target.InitializeComponent();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btnCreate_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btnCreate_ClickTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.btnCreate_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for btnExit_Click
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void btnExit_ClickTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            EventArgs e = null; // TODO: Initialize to an appropriate value
            target.btnExit_Click(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for dataGridView1_CellContentDoubleClick
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void dataGridView1_CellContentDoubleClickTest() {
            DiscountListForm_Accessor target = new DiscountListForm_Accessor(); // TODO: Initialize to an appropriate value
            object sender = null; // TODO: Initialize to an appropriate value
            DataGridViewCellEventArgs e = null; // TODO: Initialize to an appropriate value
            target.dataGridView1_CellContentDoubleClick(sender, e);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
