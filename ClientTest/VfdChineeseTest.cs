﻿using magazin_client.peripheral;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ClientTest
{
    
    
    /// <summary>
    ///This is a test class for VfdChineeseTest and is intended
    ///to contain all VfdChineeseTest Unit Tests
    ///</summary>
    [TestClass()]
    public class VfdChineeseTest {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext {
            get {
                return testContextInstance;
            }
            set {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for VfdChineese Constructor
        ///</summary>
        [TestMethod()]
        public void VfdChineeseConstructorTest() {
            VfdChineese target = new VfdChineese();
            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for Blink
        ///</summary>
        [TestMethod()]
        public void BlinkTest() {
            VfdChineese target = new VfdChineese(); // TODO: Initialize to an appropriate value
            byte frequency = 0; // TODO: Initialize to an appropriate value
            target.Blink(frequency);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for ClearDisplay
        ///</summary>
        [TestMethod()]
        public void ClearDisplayTest() {
            VfdChineese target = new VfdChineese(); // TODO: Initialize to an appropriate value
            target.ClearDisplay();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Close
        ///</summary>
        [TestMethod()]
        public void CloseTest() {
            VfdChineese target = new VfdChineese(); // TODO: Initialize to an appropriate value
            target.Close();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for Write
        ///</summary>
        [TestMethod()]
        public void WriteTest() {
            VfdChineese target = new VfdChineese(); // TODO: Initialize to an appropriate value
            string text = string.Empty; // TODO: Initialize to an appropriate value
            target.Write(text);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for magazin_client.peripheral.IVfdDisplays.IsNumeric
        ///</summary>
        [TestMethod()]
        [DeploymentItem("magazin_client.exe")]
        public void IsNumericTest() {
            IVfdDisplays target = new VfdChineese(); // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsNumeric;
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
